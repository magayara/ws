USE [MiPasta]
GO
/****** Object:  Table [dbo].[CommentLikeTe]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommentLikeTe](
	[CommentId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[IsLike] [bit] NOT NULL,
 CONSTRAINT [PK_CommentLikeTe] PRIMARY KEY CLUSTERED 
(
	[CommentId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CommentTe]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommentTe](
	[CommentId] [uniqueidentifier] NOT NULL,
	[RecipeId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Message] [text] NOT NULL,
	[Date] [datetime] NOT NULL,
	[CommenParentId] [uniqueidentifier] NULL,
 CONSTRAINT [PK_CommentTe] PRIMARY KEY CLUSTERED 
(
	[CommentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EstateTe]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EstateTe](
	[EstateId] [int] NOT NULL,
	[Name] [nvarchar](300) NOT NULL,
 CONSTRAINT [PK_EstateTe] PRIMARY KEY CLUSTERED 
(
	[EstateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IngredientTe]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IngredientTe](
	[Description] [nvarchar](300) NOT NULL,
 CONSTRAINT [PK_IngredientTe_1] PRIMARY KEY CLUSTERED 
(
	[Description] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IngredientUnitTe]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IngredientUnitTe](
	[Unit] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_IngredientUnitTe] PRIMARY KEY CLUSTERED 
(
	[Unit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductTe]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductTe](
	[BarCode] [nvarchar](50) NOT NULL,
	[Nombre] [nvarchar](300) NULL,
	[ImageUrl] [nvarchar](300) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RecipeCookStepTe]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecipeCookStepTe](
	[CookStepId] [uniqueidentifier] NOT NULL,
	[RecipeId] [uniqueidentifier] NOT NULL,
	[Order] [int] NOT NULL,
	[Description] [nvarchar](300) NOT NULL,
 CONSTRAINT [PK_CookStepTe] PRIMARY KEY CLUSTERED 
(
	[CookStepId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RecipeFavouriteTe]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecipeFavouriteTe](
	[RecipeId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_RecipeFavouriteTe] PRIMARY KEY CLUSTERED 
(
	[RecipeId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RecipeIngredientTe]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecipeIngredientTe](
	[IngredientId] [uniqueidentifier] NOT NULL,
	[RecipeId] [uniqueidentifier] NOT NULL,
	[Order] [int] NOT NULL,
	[Quantity] [money] NOT NULL,
	[Unit] [nvarchar](300) NOT NULL,
	[Description] [nvarchar](300) NOT NULL,
 CONSTRAINT [PK_IngredientTe] PRIMARY KEY CLUSTERED 
(
	[IngredientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RecipeLikeTe]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecipeLikeTe](
	[RecipeId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[IsLike] [bit] NOT NULL,
 CONSTRAINT [PK_RecipeLikeTe] PRIMARY KEY CLUSTERED 
(
	[RecipeId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RecipeRaitingTe]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecipeRaitingTe](
	[RecipeId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Rating] [money] NOT NULL,
 CONSTRAINT [PK_RecipeRaitingTe] PRIMARY KEY CLUSTERED 
(
	[RecipeId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RecipeTe]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecipeTe](
	[RecipeId] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](300) NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Difficulty] [int] NOT NULL,
	[CookTime] [money] NOT NULL,
	[Rating] [money] NULL,
	[Likes] [bigint] NULL,
	[VideoUrl] [nvarchar](300) NULL,
	[Image1Url] [nvarchar](300) NULL,
	[Image2Url] [nvarchar](300) NULL,
	[Image3Url] [nvarchar](300) NULL,
	[Date] [datetime] NOT NULL,
	[BarCode] [nvarchar](300) NULL,
 CONSTRAINT [PK_RecipeTe] PRIMARY KEY CLUSTERED 
(
	[RecipeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserStatisticTe]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserStatisticTe](
	[UserId] [uniqueidentifier] NOT NULL,
	[Followers] [bigint] NOT NULL,
	[Following] [bigint] NOT NULL,
	[Points] [bigint] NOT NULL,
 CONSTRAINT [PK_UserStatisticTe] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserTe]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTe](
	[UserId] [uniqueidentifier] NOT NULL,
	[FirstName] [nvarchar](300) NULL,
	[LastName] [nvarchar](300) NULL,
	[Email] [nvarchar](300) NOT NULL,
	[Ocupation] [nvarchar](300) NULL,
	[EstateId] [int] NOT NULL,
	[BirthDate] [date] NOT NULL,
	[Gender] [int] NOT NULL,
	[OauthToken] [nvarchar](300) NULL,
	[AuthenticationType] [int] NOT NULL,
	[Password] [nvarchar](300) NULL,
	[AvatarUrl] [nvarchar](300) NULL,
	[FacebookId] [nvarchar](300) NULL,
 CONSTRAINT [PK_UserTe] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CommentLikeTe] ADD  CONSTRAINT [DF_CommentLikeTe_IsLike]  DEFAULT ((0)) FOR [IsLike]
GO
ALTER TABLE [dbo].[RecipeLikeTe] ADD  CONSTRAINT [DF_RecipeLikeTe_Likes]  DEFAULT ((0)) FOR [IsLike]
GO
ALTER TABLE [dbo].[RecipeTe] ADD  CONSTRAINT [DF_RecipeTe_Rating]  DEFAULT ((0)) FOR [Rating]
GO
ALTER TABLE [dbo].[RecipeTe] ADD  CONSTRAINT [DF_RecipeTe_Likes]  DEFAULT ((0)) FOR [Likes]
GO
/****** Object:  StoredProcedure [dbo].[DeleteRecipe_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteRecipe_sp] (
	 @RecipeId uniqueidentifier
)
AS
BEGIN
	SET NOCOUNT OFF;

		DELETE FROM [dbo].[RecipeLikeTe]
		WHERE [RecipeId] = @RecipeId

		DELETE FROM [dbo].[RecipeFavouriteTe]
		WHERE [RecipeId] = @RecipeId

		DELETE FROM [dbo].[RecipeRaitingTe]
		WHERE [RecipeId] = @RecipeId
		
		DELETE FROM [dbo].[CommentTe]
		WHERE [RecipeId] = @RecipeId

		DELETE FROM [dbo].[RecipeCookStepTe]
		WHERE [RecipeId] = @RecipeId

		DELETE FROM [dbo].[RecipeIngredientTe]
		WHERE [RecipeId] = @RecipeId

		DELETE FROM [dbo].[RecipeTe]
		WHERE [RecipeId] = @RecipeId

END
GO
/****** Object:  StoredProcedure [dbo].[GetComments_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetComments_sp] (
	 @RecipeId uniqueidentifier = null
	,@UserId uniqueidentifier = null
	,@PageNumber int
	,@ItemsPerPage int
)
AS
BEGIN
	SET NOCOUNT ON;

	if(@RecipeId IS NOT NULL)
	BEGIN

		SELECT 
				 a.[CommentId]
				,a.[RecipeId]
				,a.[Message]
				,a.[Date]
				,b.[UserId]
				,b.[FirstName]
				,b.[LastName]
				,b.[AvatarUrl]
				,CAST(ISNULL([IsLike], 0) AS BIT) Ilike
				,[CommenParentId]
			FROM [dbo].[CommentTe] a
			INNER JOIN [dbo].[UserTe] b
				ON a.[UserId] = b.[UserId]
			LEFT JOIN [dbo].[CommentLikeTe] c
				ON c.[CommentId] =  a.[CommentId]
				AND a.[UserId] = c.[UserId]
			WHERE a.[RecipeId] = @RecipeId
			ORDER BY a.[Date] DESC
			OFFSET @ItemsPerPage * (@PageNumber - 1) ROWS
		FETCH NEXT @ItemsPerPage ROWS ONLY OPTION (RECOMPILE);

	END
	ELSE 
	BEGIN

			SELECT 
				 a.[CommentId]
				,a.[RecipeId]
				,a.[Message]
				,a.[Date]
				,b.[UserId]
				,b.[FirstName]
				,b.[LastName]
				,b.[AvatarUrl]
				,CAST(ISNULL([IsLike], 0) AS BIT) Ilike
				,[CommenParentId]
			FROM [dbo].[CommentTe] a
			INNER JOIN [dbo].[RecipeTe] d
				ON d.[RecipeId] = a.[RecipeId]
			INNER JOIN [dbo].[UserTe] b
				ON a.[UserId] = b.[UserId]
			LEFT JOIN [dbo].[CommentLikeTe] c
				ON c.[CommentId] =  a.[CommentId]
				AND a.[UserId] = c.[UserId]
			WHERE d.[UserId] = @UserId
			ORDER BY a.[Date] DESC
			OFFSET @ItemsPerPage * (@PageNumber - 1) ROWS
		FETCH NEXT @ItemsPerPage ROWS ONLY OPTION (RECOMPILE);
	END



END
GO
/****** Object:  StoredProcedure [dbo].[GetEstates_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetEstates_sp]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT  [EstateId]
		,[Name]
	FROM [dbo].[EstateTe]
	ORDER BY [Name];
END
GO
/****** Object:  StoredProcedure [dbo].[GetIngredients_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetIngredients_sp] (
	@Query nvarchar(100)
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [Description]
	FROM [dbo].[IngredientTe]
	WHERE [Description] LIKE '%' + @Query + '%'
END
GO
/****** Object:  StoredProcedure [dbo].[GetIngredientUnits_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetIngredientUnits_sp] (
	@Query nvarchar(100) = null
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [Unit]
	FROM [dbo].[IngredientUnitTe]
	WHERE [Unit] LIKE '%' + @Query + '%'
END
GO
/****** Object:  StoredProcedure [dbo].[GetProduct_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetProduct_sp] (
	@BarCode nvarchar(50)
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT  [BarCode]
		,[Nombre]
		,[ImageUrl]
	FROM [dbo].[ProductTe]
	WHERE [BarCode] = @BarCode
END
GO
/****** Object:  StoredProcedure [dbo].[GetRecipe_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetRecipe_sp] (
	  @UserId uniqueidentifier
	 ,@RecipeId uniqueidentifier
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		 a.[RecipeId]
		,a.[Title]
		,a.[Rating]
		,a.[Difficulty]
		,a.[CookTime]
		,a.[Likes]
		,a.[VideoUrl]
		,a.[Image1Url]
		,a.[Image2Url]
		,a.[Image3Url]
		,a.[Barcode]
		,a.[Date]
		,b.[UserId]
		,b.[FirstName]
		,b.[LastName]
		,b.[AvatarUrl]
		,ISNULL(c.[Rating], 0)  MyRating
		,CAST(CASE WHEN d.[UserId] IS NOT NULL THEN 1 ELSE 0 END AS BIT) IsFavourite
		,CAST(CASE WHEN e.[UserId] IS NOT NULL THEN 1 ELSE 0 END AS BIT) IsLike
	FROM [dbo].[RecipeTe] a
	INNER JOIN [dbo].[UserTe] b
		ON a.[UserId] = b.[UserId]
	LEFT JOIN [dbo].[RecipeRaitingTe] c
		ON a.[RecipeId] = c.[RecipeId]
		AND c.[UserId] = @UserId
	LEFT JOIN [dbo].[RecipeFavouriteTe] d
		ON a.[RecipeId] = d.[RecipeId]
		AND d.[UserId] = @UserId
	LEFT JOIN [dbo].RecipeLikeTe e
		ON a.[RecipeId] = e.[RecipeId]
		AND e.[UserId] = @UserId
	WHERE a.[RecipeId] = @RecipeId
	

END
GO
/****** Object:  StoredProcedure [dbo].[GetRecipeCookStepsByRecipeId_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetRecipeCookStepsByRecipeId_sp] (
	 @RecipeId uniqueidentifier
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT  [CookStepId]
		,[RecipeId]
		,[Order]
		,[Description]
	FROM [dbo].[RecipeCookStepTe]
	WHERE [RecipeId] = @RecipeId
	ORDER BY [Order];

END
GO
/****** Object:  StoredProcedure [dbo].[GetRecipeIngredientsByRecipeId_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetRecipeIngredientsByRecipeId_sp] (
	 @RecipeId uniqueidentifier
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT  [IngredientId]
		,[RecipeId]
		,[Order]
		,[Quantity]
		,[Unit]
		,[Description]
	FROM [dbo].[RecipeIngredientTe]
	WHERE [RecipeId] = @RecipeId
	ORDER BY [Order];

END
GO
/****** Object:  StoredProcedure [dbo].[GetRecipesByFilters_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetRecipesByFilters_sp] (
	 @PageNumber int
	,@ItemsPerPage int
	,@Barcode nvarchar(300) = null
	,@Latest bit 
	,@Popular bit
	,@Title nvarchar(300) = null
)
AS
BEGIN
	SET NOCOUNT ON;

	if(@Latest = 1)
	begin
		SELECT 
			 a.[RecipeId]
			,a.[Title]
			,a.[Rating]
			,a.[Difficulty]
			,a.[CookTime]
			,a.[Likes]
			,a.[Image1Url]
			,a.[Image2Url]
			,a.[Image3Url]
			,a.[Date]
			,a.[Barcode]
			,b.[UserId]
			,b.[FirstName]
			,b.[LastName]
			,b.[AvatarUrl]
		FROM [dbo].[RecipeTe] a
		INNER JOIN [dbo].[UserTe] b
			ON a.[UserId] = b.[UserId]
		WHERE a.[BarCode]  = ISNULL(@Barcode, a.[BarCode])
		AND a.[Title] LIKE  '%' + ISNULL(@Title, a.[Title])  + '%'
		ORDER BY a.[Date] DESC
		OFFSET @ItemsPerPage * (@PageNumber - 1) ROWS
		FETCH NEXT @ItemsPerPage ROWS ONLY OPTION (RECOMPILE);
	end
	else if(@Popular = 1)
	begin
		SELECT 
			 a.[RecipeId]
			,a.[Title]
			,a.[Rating]
			,a.[Difficulty]
			,a.[CookTime]
			,a.[Likes]
			,a.[Image1Url]
			,a.[Image2Url]
			,a.[Image3Url]
			,a.[Date]
			,a.[Barcode]
			,b.[UserId]
			,b.[FirstName]
			,b.[LastName]
			,b.[AvatarUrl]
		FROM [dbo].[RecipeTe] a
		INNER JOIN [dbo].[UserTe] b
			ON a.[UserId] = b.[UserId]
		WHERE a.[BarCode]  = ISNULL(@Barcode, a.[BarCode])
		AND a.[Title] LIKE  '%' + ISNULL(@Title, a.[Title])  + '%'
		ORDER BY a.[Likes] DESC
		OFFSET @ItemsPerPage * (@PageNumber - 1) ROWS
		FETCH NEXT @ItemsPerPage ROWS ONLY OPTION (RECOMPILE);
	end

	

END
GO
/****** Object:  StoredProcedure [dbo].[GetRecipesByUserId_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetRecipesByUserId_sp] (
	 @UserId uniqueidentifier
	,@PageNumber int
	,@ItemsPerPage int
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		 a.[RecipeId]
		,a.[Title]
		,a.[Rating]
		,a.[Difficulty]
		,a.[CookTime]
		,a.[Likes]
		,a.[Image1Url]
		,a.[Image2Url]
		,a.[Image3Url]
		,a.[Date]
		,a.[Barcode]
		,b.[UserId]
		,b.[FirstName]
		,b.[LastName]
		,b.[AvatarUrl]
	FROM [dbo].[RecipeTe] a
	INNER JOIN [dbo].[UserTe] b
		ON a.[UserId] = b.[UserId]
	WHERE b.[UserId] = @UserId
	ORDER BY a.[Date] DESC
	OFFSET @ItemsPerPage * (@PageNumber - 1) ROWS
	FETCH NEXT @ItemsPerPage ROWS ONLY OPTION (RECOMPILE);
	

END
GO
/****** Object:  StoredProcedure [dbo].[GetRecipesFavouritesByUserId_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetRecipesFavouritesByUserId_sp] (
	 @UserId uniqueidentifier
	,@PageNumber int
	,@ItemsPerPage int
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		 a.[RecipeId]
		,a.[Title]
		,a.[Rating]
		,a.[Difficulty]
		,a.[CookTime]
		,a.[Likes]
		,a.[Image1Url]
		,a.[Image2Url]
		,a.[Image3Url]
		,a.[Date]
		,a.[Barcode]
		,c.[UserId]
		,c.[FirstName]
		,c.[LastName]
		,c.[AvatarUrl]
	FROM [dbo].[RecipeTe] a
	INNER JOIN [dbo].[RecipeFavouriteTe] b
		ON  a.[RecipeId] = b.[RecipeId]
	INNER JOIN [dbo].[UserTe] c
		ON a.[UserId] = c.[UserId]
	WHERE b.[UserId] = @UserId
	ORDER BY a.[Date] DESC
	OFFSET @ItemsPerPage * (@PageNumber - 1) ROWS
	FETCH NEXT @ItemsPerPage ROWS ONLY OPTION (RECOMPILE);
	

END
GO
/****** Object:  StoredProcedure [dbo].[GetUserByEmail_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUserByEmail_sp] (
	 @Email nvarchar(300)
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT  [UserId]
		,[FirstName]
		,[LastName]
		,[Email]
		,[Ocupation]
		,[EstateId]
		,[BirthDate]
		,[Gender]
		,[OauthToken]
		,[AuthenticationType]
		,[Password]
		,[AvatarUrl]
		,[FacebookId]
	FROM [dbo].[UserTe]
	WHERE [Email] = @Email

END
GO
/****** Object:  StoredProcedure [dbo].[GetUserById_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUserById_sp] (
	 @UserId uniqueidentifier
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT  [UserId]
		,[FirstName]
		,[LastName]
		,[Email]
		,[Ocupation]
		,[EstateId]
		,[BirthDate]
		,[Gender]
		,[OauthToken]
		,[AuthenticationType]
		,[Password]
		,[AvatarUrl]
		,[FacebookId]
	FROM [dbo].[UserTe]
	WHERE [UserId] = @UserId

END
GO
/****** Object:  StoredProcedure [dbo].[GetUserStatistic_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUserStatistic_sp] (
	 @UserId uniqueidentifier
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [UserId]
		,[Followers]
		,[Following]
		,[Points]
	FROM [dbo].[UserStatisticTe]
	WHERE [UserId] = @UserId

END
GO
/****** Object:  StoredProcedure [dbo].[SaveComment_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SaveComment_sp] (
	 @CommentId uniqueidentifier
	,@RecipeId uniqueidentifier
	,@UserId uniqueidentifier
	,@Message text
	,@Date datetime
	,@CommenParentId uniqueidentifier = null
)
AS
BEGIN
	SET NOCOUNT OFF;

	INSERT INTO [dbo].[CommentTe]
		([CommentId]
		,[RecipeId]
		,[UserId]
		,[Message]
		,[Date]
		,[CommenParentId])
	VALUES
		(@CommentId
		,@RecipeId
		,@UserId
		,@Message
		,@Date
		,@CommenParentId)

END
GO
/****** Object:  StoredProcedure [dbo].[SaveRecipe_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SaveRecipe_sp] (
	 @RecipeId uniqueidentifier
	,@Title nvarchar(300)
	,@Rating float  = 0
	,@UserId uniqueidentifier
	,@Difficulty int
	,@CookTime decimal(18,0)
	,@Likes bigint = 0
	,@Image1Url nvarchar(300) = null
	,@Image2Url nvarchar(300) = null
	,@Image3Url nvarchar(300) = null
	,@Barcode nvarchar(300) = null
)
AS
BEGIN
	SET NOCOUNT OFF;


	if(EXISTS(SELECT TOP 1 [RecipeId] FROM [dbo].[RecipeTe] WHERE [RecipeId] = @RecipeId))
	begin
	
		DELETE FROM [dbo].[RecipeCookStepTe]
		WHERE [RecipeId] = @RecipeId

		DELETE FROM [dbo].[RecipeIngredientTe]
		WHERE [RecipeId] = @RecipeId


		UPDATE [dbo].[RecipeTe]
		   SET [Title] = @Title
			  ,[Rating] = @Rating
			  ,[UserId] = @UserId
			  ,[Difficulty] = @Difficulty
			  ,[CookTime] = @CookTime
			  ,[Likes] = @Likes
			  ,[Image1Url] = @Image1Url
			  ,[Image2Url] = @Image2Url
			  ,[Image3Url] = @Image3Url
			  ,[Barcode] = @Barcode
		 WHERE [RecipeId] = @RecipeId

	end
	else
	begin

		INSERT INTO [dbo].[RecipeTe]
			([RecipeId]
			,[Title]
			,[Rating]
			,[UserId]
			,[Difficulty]
			,[CookTime]
			,[Likes]
			,[Image1Url]
			,[Image2Url]
			,[Image3Url]
			,[Date]
			,[Barcode])
		VALUES
			(@RecipeId
			,@Title
			,@Rating
			,@UserId
			,@Difficulty
			,@CookTime
			,@Likes
			,@Image1Url
			,@Image2Url
			,@Image3Url
			,GETDATE()
			,@Barcode)

	end


END
GO
/****** Object:  StoredProcedure [dbo].[SaveRecipeCookStep_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SaveRecipeCookStep_sp] (
	 @CookStepId uniqueidentifier
	,@RecipeId uniqueidentifier
	,@Order int
	,@Description nvarchar(300)
)
AS
BEGIN
	SET NOCOUNT OFF;


	INSERT INTO [dbo].[RecipeCookStepTe]
		([CookStepId]
		,[RecipeId]
		,[Order]
		,[Description])
	VALUES
		(@CookStepId
		,@RecipeId
		,@Order
		,@Description)

END
GO
/****** Object:  StoredProcedure [dbo].[SaveRecipeIngredient_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SaveRecipeIngredient_sp] (
	 @IngredientId uniqueidentifier
	,@RecipeId uniqueidentifier
	,@Order int
	,@Quantity decimal(18,0)
	,@Unit nvarchar(300)
	,@Description nvarchar(300)
)
AS
BEGIN
	SET NOCOUNT OFF;

	INSERT INTO [dbo].[RecipeIngredientTe]
		([IngredientId]
		,[RecipeId]
		,[Order]
		,[Quantity]
		,[Unit]
		,[Description])
	VALUES
		(@IngredientId
		,@RecipeId
		,@Order
		,@Quantity
		,@Unit
		,@Description)

END
GO
/****** Object:  StoredProcedure [dbo].[SaveUser_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SaveUser_sp] (
	 @UserId uniqueidentifier
	,@FirstName nvarchar(300)
	,@LastName nvarchar(300)
	,@Email nvarchar(300)
	,@Ocupation nvarchar(300)
	,@EstateId int
	,@BirthDate date
	,@Gender int
	,@OauthToken nvarchar(300) = null
	,@AuthenticationType int
	,@Password nvarchar(300) = null
	,@AvatarUrl nvarchar(300) = null
	,@FacebookId nvarchar(300) = null
)
AS
BEGIN
	SET NOCOUNT OFF;

	if(EXISTS(SELECT TOP 1 [UserId] FROM [dbo].[UserTe] WHERE [Email] = @Email))
	begin

		UPDATE [dbo].[UserTe]
		   SET [FirstName] = @FirstName
			  ,[LastName] = @LastName
			  ,[Email] = @Email
			  ,[Ocupation] = @Ocupation
			  ,[EstateId] =  @EstateId
			  ,[BirthDate] = @BirthDate
			  ,[Gender] = @Gender
			  ,[OauthToken] = @OauthToken
			  ,[AuthenticationType] = @AuthenticationType
			  ,[Password] = @Password
			  ,[AvatarUrl] = @AvatarUrl
			  ,[FacebookId] = @FacebookId
		 WHERE [UserId] = @UserId

	end
	else
	begin
	
		INSERT INTO [dbo].[UserTe]
			([UserId]
			,[FirstName]
			,[LastName]
			,[Email]
			,[Ocupation]
			,[EstateId]
			,[BirthDate]
			,[Gender]
			,[OauthToken]
			,[AuthenticationType]
			,[Password]
			,[AvatarUrl]
			,[FacebookId])
		VALUES
			(@UserId
			,@FirstName
			,@LastName
			,@Email
			,@Ocupation
			,@EstateId
			,@BirthDate
			,@Gender
			,@OauthToken
			,@AuthenticationType
			,@Password
			,@AvatarUrl
			,@FacebookId)

	end

END
GO
/****** Object:  StoredProcedure [dbo].[SetCommentLike_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SetCommentLike_sp] (
	 @UserId uniqueidentifier
	,@CommentId uniqueidentifier
	,@IsLike bit
)
AS
BEGIN
	SET NOCOUNT OFF;

	DELETE FROM [dbo].[CommentLikeTe]
	WHERE [CommentId] = @CommentId
	AND [UserId] = @UserId

	if(@IsLike = 1) 
	begin

		INSERT INTO [dbo].[CommentLikeTe]
			([CommentId]
			,[UserId]
			,[IsLike])
		VALUES
			(@CommentId
			,@UserId
			,@IsLike)

	end

END
GO
/****** Object:  StoredProcedure [dbo].[SetRecipeAsFavourite_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SetRecipeAsFavourite_sp] (
	 @UserId uniqueidentifier
	,@RecipeId uniqueidentifier
	,@IsFavourite bit
)
AS
BEGIN
	SET NOCOUNT OFF;

	DELETE FROM [dbo].[RecipeFavouriteTe]
	WHERE [RecipeId] = @RecipeId
	AND [UserId] = @UserId

	if(@IsFavourite = 1)
	begin

	INSERT INTO [dbo].[RecipeFavouriteTe]
		([RecipeId]
		,[UserId])
	VALUES
		(@RecipeId
		,@UserId)

	end

END
GO
/****** Object:  StoredProcedure [dbo].[SetRecipeLike_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SetRecipeLike_sp] (
	 @UserId uniqueidentifier
	,@RecipeId uniqueidentifier
	,@IsLike bit
)
AS
BEGIN
	SET NOCOUNT OFF;

	DELETE FROM [dbo].[RecipeLikeTe]
	WHERE [RecipeId] = @RecipeId
	AND [UserId] = @UserId

	if(@IsLike = 1) 
	begin

		INSERT INTO [dbo].[RecipeLikeTe]
			([RecipeId]
			,[UserId]
			,[IsLike])
		VALUES
			(@RecipeId
			,@UserId
			,@IsLike)

	end

	UPDATE [dbo].[RecipeTe] SET	
	[Likes] = (SELECT COUNT([RecipeId]) FROM [dbo].[RecipeLikeTe] WHERE [IsLike] = 1 )
	WHERE [RecipeId] = @RecipeId

END
GO
/****** Object:  StoredProcedure [dbo].[SetRecipeRating_sp]    Script Date: 10/1/2018 9:00:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SetRecipeRating_sp] (
	 @UserId uniqueidentifier
	,@RecipeId uniqueidentifier
	,@Rating decimal
)
AS
BEGIN
	SET NOCOUNT OFF;

	DELETE FROM [dbo].[RecipeRaitingTe]
	WHERE [RecipeId] = @RecipeId
	AND [UserId] = @UserId

	INSERT INTO [dbo].[RecipeRaitingTe]
		([RecipeId]
		,[UserId]
		,[Rating])
	VALUES
		(@RecipeId
		,@UserId
		,@Rating)

	UPDATE [dbo].[RecipeTe] SET	
	[Rating] = (SELECT SUM([Rating]) / COUNT([RecipeId]) FROM [dbo].[RecipeRaitingTe] WHERE [RecipeId] = @RecipeId )
	WHERE [RecipeId] = @RecipeId

END
GO
USE [master]
GO
ALTER DATABASE [MiPasta] SET  READ_WRITE 
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (1, N'Aguascalientes')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (2, N'Baja California')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (3, N'Baja California Sur')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (4, N'Campeche')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (5, N'Chiapas')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (6, N'Chihuahua')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (7, N'Ciudad de México')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (8, N'Coahuila de Zaragoza')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (9, N'Colima')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (10, N'Durango')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (11, N'Estado de México')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (12, N'Guanajuato')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (13, N'Guerrero')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (14, N'Hidalgo')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (15, N'Jalisco')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (16, N'Michoacán de Ocampo')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (17, N'Morelos')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (18, N'Nayarit')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (19, N'Nuevo León')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (20, N'Oaxaca')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (21, N'Puebla')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (22, N'Querétaro')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (23, N'Quintana Roo')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (24, N'San Luis Potosí')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (25, N'Sin Localidad')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (26, N'Sinaloa')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (27, N'Sonora')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (28, N'Tabasco')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (29, N'Tamaulipas')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (30, N'Tlaxcala')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (31, N'Veracruz de Ignacio de la Llave')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (32, N'Yucatán')
GO
INSERT [dbo].[EstateTe] ([EstateId], [Name]) VALUES (33, N'Zacatecas')
GO
INSERT [dbo].[IngredientTe] ([Description]) VALUES (N'Cebolla')
GO
INSERT [dbo].[IngredientTe] ([Description]) VALUES (N'Jamón')
GO
INSERT [dbo].[IngredientTe] ([Description]) VALUES (N'Pasta de moño')
GO
INSERT [dbo].[IngredientTe] ([Description]) VALUES (N'Pimimienta')
GO
INSERT [dbo].[IngredientTe] ([Description]) VALUES (N'Queso')
GO