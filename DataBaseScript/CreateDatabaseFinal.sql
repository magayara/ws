ALTER DATABASE [MiPasta2] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MiPasta2] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MiPasta2] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MiPasta2] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MiPasta2] SET ARITHABORT OFF 
GO
ALTER DATABASE [MiPasta2] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MiPasta2] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MiPasta2] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MiPasta2] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MiPasta2] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MiPasta2] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MiPasta2] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MiPasta2] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MiPasta2] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MiPasta2] SET  DISABLE_BROKER 
GO
ALTER DATABASE [MiPasta2] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MiPasta2] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MiPasta2] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MiPasta2] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MiPasta2] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MiPasta2] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MiPasta2] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MiPasta2] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [MiPasta2] SET  MULTI_USER 
GO
ALTER DATABASE [MiPasta2] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MiPasta2] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MiPasta2] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MiPasta2] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [MiPasta2] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [MiPasta2] SET QUERY_STORE = OFF
GO
USE [MiPasta2]
GO
/****** Object:  UserDefinedFunction [dbo].[Split]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[Split]
(
    @id bigint,
    @Line varchar(255),
    @SplitOn nvarchar(5) = ','
)
RETURNS @RtnValue table
(
    RecipeId BIGINT,
	[Order] INT,
    [Description] varchar(255) NOT NULL
)
AS
BEGIN
    IF @Line IS NULL RETURN

    DECLARE @split_on_len INT = LEN(@SplitOn)
    DECLARE @start_at INT = 1
    DECLARE @end_at INT
    DECLARE @data_len INT

	    DECLARE @count INT = 1
	set @Line = replace(@Line, '\n','')
    WHILE 1=1
    BEGIN
        SET @end_at = CHARINDEX(@SplitOn,@Line,@start_at)
        SET @data_len = CASE @end_at WHEN 0 THEN LEN(@Line) ELSE @end_at-@start_at END
        INSERT INTO @RtnValue (RecipeId, [Order],[Description]) VALUES(@id, @count, RTRIM(LTRIM(SUBSTRING(@Line,@start_at,@data_len))));
        IF @end_at = 0 BREAK;
        SET @start_at = @end_at + @split_on_len
		SET @count = @count + 1
    END

    RETURN
END
GO
/****** Object:  Table [dbo].[AppUser]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppUser](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](300) NULL,
	[LastName] [nvarchar](300) NULL,
	[Email] [nvarchar](300) NOT NULL,
	[Ocupation] [nvarchar](300) NULL,
	[EstateId] [int] NOT NULL,
	[BirthDate] [date] NOT NULL,
	[Gender] [int] NOT NULL,
	[OauthToken] [nvarchar](300) NULL,
	[AuthenticationType] [int] NOT NULL,
	[Password] [nvarchar](300) NULL,
	[AvatarUrl] [nvarchar](300) NULL,
	[FacebookId] [nvarchar](300) NULL,
 CONSTRAINT [PK_AppUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AppUserStatistic]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppUserStatistic](
	[AppUserId] [bigint] NOT NULL,
	[Followers] [bigint] NOT NULL,
	[Following] [bigint] NOT NULL,
	[Points] [bigint] NOT NULL,
 CONSTRAINT [PK_AppUserStatistic] PRIMARY KEY CLUSTERED 
(
	[AppUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](250) NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comment]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comment](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[RecipeId] [bigint] NULL,
	[AppUserId] [bigint] NULL,
	[Message] [text] NOT NULL,
	[Date] [datetime] NOT NULL,
	[CommenParentId] [bigint] NULL,
 CONSTRAINT [PK_Comment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CommentLike]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommentLike](
	[CommentId] [bigint] NOT NULL,
	[AppUserId] [bigint] NOT NULL,
	[IsLike] [bit] NOT NULL,
 CONSTRAINT [PK_CommentLike] PRIMARY KEY CLUSTERED 
(
	[CommentId] ASC,
	[AppUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Estates]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estates](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](300) NOT NULL,
 CONSTRAINT [PK_Estates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ingredient]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ingredient](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](100) NULL,
 CONSTRAINT [PK_Ingredient] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Level]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Level](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Level] [varchar](20) NULL,
 CONSTRAINT [PK_Level] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PermissionRole]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PermissionRole](
	[PermissionId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_PermissionRole] PRIMARY KEY CLUSTERED 
(
	[PermissionId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Permissions]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permissions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[DisplayName] [varchar](255) NULL,
	[Description] [varchar](255) NULL,
	[CreatedAt] [datetime] NOT NULL,
	[UpdatedAt] [datetime] NULL,
 CONSTRAINT [PK_Permissions_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[Description] [varchar](100) NULL,
	[Grammage] [varchar](150) NULL,
	[Ean] [varchar](100) NULL,
	[Enabled] [tinyint] NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[UpdatedAt] [datetime] NULL,
	[oldId] [int] NULL,
	[Image] [varchar](250) NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Recipe]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recipe](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NULL,
	[Image] [varchar](250) NULL,
	[Image2] [varchar](250) NULL,
	[Image3] [varchar](250) NULL,
	[Tumb] [varchar](250) NULL,
	[Introduction] [varchar](250) NULL,
	[ProductId] [int] NULL,
	[Steeps] [varchar](255) NULL,
	[Preparation] [varchar](max) NOT NULL,
	[Time] [varchar](50) NULL,
	[Publish] [tinyint] NOT NULL,
	[Approved] [tinyint] NOT NULL,
	[Score] [numeric](18, 0) NOT NULL,
	[PerfilId] [bigint] NULL,
	[Level] [nvarchar](50) NULL,
	[AdminId] [bigint] NULL,
	[AppUserId] [bigint] NULL,
	[Ip] [varchar](50) NULL,
	[Latitude] [varchar](50) NULL,
	[Longitude] [varchar](50) NULL,
	[Date] [datetime] NULL,
	[LongURL] [varchar](250) NULL,
	[ShortURL] [varchar](250) NULL,
	[Qr] [varchar](250) NULL,
	[oldid] [int] NULL,
	[Video] [varchar](250) NULL,
	[Likes] [bigint] NULL,
 CONSTRAINT [PK_Recipe] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RecipeCookStep]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecipeCookStep](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[RecipeId] [bigint] NOT NULL,
	[Order] [int] NOT NULL,
	[Description] [nvarchar](300) NOT NULL,
 CONSTRAINT [PK_RecipeCookStep] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RecipeFavourite]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecipeFavourite](
	[RecipeId] [bigint] NOT NULL,
	[AppUserId] [bigint] NOT NULL,
 CONSTRAINT [PK_RecipeFavourite] PRIMARY KEY CLUSTERED 
(
	[RecipeId] ASC,
	[AppUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RecipeIngredient]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecipeIngredient](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RecipeId] [int] NULL,
	[Quantity] [varchar](10) NOT NULL,
	[UnitId] [int] NULL,
	[IngredientId] [int] NULL,
	[Order] [int] NULL,
	[Unit] [varchar](300) NULL,
	[Description] [varchar](300) NULL,
 CONSTRAINT [PK_RecipeIngredient] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RecipeLike]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecipeLike](
	[RecipeId] [bigint] NOT NULL,
	[AppUserId] [bigint] NOT NULL,
	[IsLike] [bit] NOT NULL,
 CONSTRAINT [PK_RecipeLike] PRIMARY KEY CLUSTERED 
(
	[RecipeId] ASC,
	[AppUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RecipeScore]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecipeScore](
	[RecipeId] [bigint] NOT NULL,
	[AppUserId] [bigint] NOT NULL,
	[Score] [money] NOT NULL,
 CONSTRAINT [PK_RecipeScore] PRIMARY KEY CLUSTERED 
(
	[RecipeId] ASC,
	[AppUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[DisplayName] [varchar](250) NULL,
	[Description] [varchar](250) NULL,
	[CreatedAt] [datetime] NOT NULL,
	[UpdatedAt] [datetime] NULL,
 CONSTRAINT [PK_Roles_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleUser]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleUser](
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_RoleUser] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Unit]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Unit](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](150) NOT NULL,
 CONSTRAINT [PK_Unit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](250) NOT NULL,
	[Email] [varchar](250) NOT NULL,
	[Password] [varchar](250) NOT NULL,
	[RememberToken] [varchar](100) NULL,
	[CreatedAt] [datetime] NOT NULL,
	[UpdatedAt] [datetime] NULL,
	[Department] [varchar](255) NULL,
	[Manager] [int] NULL,
	[Name] [varchar](255) NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [IX_RecipeCookStep]    Script Date: 10/11/2018 12:58:01 AM ******/
CREATE NONCLUSTERED INDEX [IX_RecipeCookStep] ON [dbo].[RecipeCookStep]
(
	[RecipeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CommentLike] ADD  CONSTRAINT [DF_CommentLike_IsLike]  DEFAULT ((0)) FOR [IsLike]
GO
ALTER TABLE [dbo].[Permissions] ADD  CONSTRAINT [DF_Permissions_CreatedAt]  DEFAULT (getdate()) FOR [CreatedAt]
GO
ALTER TABLE [dbo].[Products] ADD  CONSTRAINT [DF_Products_Enabled]  DEFAULT ((1)) FOR [Enabled]
GO
ALTER TABLE [dbo].[Products] ADD  CONSTRAINT [DF_Products_CreatedAt]  DEFAULT (getdate()) FOR [CreatedAt]
GO
ALTER TABLE [dbo].[Recipe] ADD  CONSTRAINT [DF_Table_1_Publicar]  DEFAULT ((0)) FOR [Publish]
GO
ALTER TABLE [dbo].[Recipe] ADD  CONSTRAINT [DF_Recipe_Score]  DEFAULT ((0)) FOR [Score]
GO
ALTER TABLE [dbo].[RecipeLike] ADD  CONSTRAINT [DF_RecipeLike_IsLike]  DEFAULT ((0)) FOR [IsLike]
GO
ALTER TABLE [dbo].[Roles] ADD  CONSTRAINT [DF_Roles_CreatedAt]  DEFAULT (getdate()) FOR [CreatedAt]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_CreatedAt]  DEFAULT (getdate()) FOR [CreatedAt]
GO
ALTER TABLE [dbo].[PermissionRole]  WITH CHECK ADD  CONSTRAINT [FK_PermissionRole_Permissions] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[Permissions] ([Id])
GO
ALTER TABLE [dbo].[PermissionRole] CHECK CONSTRAINT [FK_PermissionRole_Permissions]
GO
ALTER TABLE [dbo].[PermissionRole]  WITH CHECK ADD  CONSTRAINT [FK_PermissionRole_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])
GO
ALTER TABLE [dbo].[PermissionRole] CHECK CONSTRAINT [FK_PermissionRole_Roles]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([Id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_Category]
GO
ALTER TABLE [dbo].[Recipe]  WITH CHECK ADD  CONSTRAINT [FK_Recipe_Products] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[Recipe] CHECK CONSTRAINT [FK_Recipe_Products]
GO
ALTER TABLE [dbo].[RecipeIngredient]  WITH CHECK ADD  CONSTRAINT [FK_RecipeIngredient_Ingredient] FOREIGN KEY([IngredientId])
REFERENCES [dbo].[Ingredient] ([Id])
GO
ALTER TABLE [dbo].[RecipeIngredient] CHECK CONSTRAINT [FK_RecipeIngredient_Ingredient]
GO
ALTER TABLE [dbo].[RecipeIngredient]  WITH CHECK ADD  CONSTRAINT [FK_RecipeIngredient_Recipe] FOREIGN KEY([RecipeId])
REFERENCES [dbo].[Recipe] ([Id])
GO
ALTER TABLE [dbo].[RecipeIngredient] CHECK CONSTRAINT [FK_RecipeIngredient_Recipe]
GO
ALTER TABLE [dbo].[RecipeIngredient]  WITH CHECK ADD  CONSTRAINT [FK_RecipeIngredient_Unit] FOREIGN KEY([UnitId])
REFERENCES [dbo].[Unit] ([Id])
GO
ALTER TABLE [dbo].[RecipeIngredient] CHECK CONSTRAINT [FK_RecipeIngredient_Unit]
GO
ALTER TABLE [dbo].[RoleUser]  WITH CHECK ADD  CONSTRAINT [FK_RoleUser_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])
GO
ALTER TABLE [dbo].[RoleUser] CHECK CONSTRAINT [FK_RoleUser_Roles]
GO
ALTER TABLE [dbo].[RoleUser]  WITH CHECK ADD  CONSTRAINT [FK_RoleUser_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[RoleUser] CHECK CONSTRAINT [FK_RoleUser_Users]
GO
/****** Object:  StoredProcedure [dbo].[DeleteRecipe_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteRecipe_sp] (
	 @RecipeId bigint
)
AS
BEGIN
	SET NOCOUNT OFF;

		DELETE FROM [dbo].[RecipeLike]
		WHERE [RecipeId] = @RecipeId

		DELETE FROM [dbo].[RecipeFavourite]
		WHERE [RecipeId] = @RecipeId

		DELETE FROM [dbo].[RecipeScore]
		WHERE [RecipeId] = @RecipeId

		DELETE a FROM [dbo].[CommentLike] a
		INNER JOIN [dbo].[Comment] b
			ON a.[CommentId] = b.[Id]
			AND b.[RecipeId] = @RecipeId
		
		DELETE FROM [dbo].[Comment]
		WHERE [RecipeId] = @RecipeId

		DELETE FROM [dbo].[RecipeCookStep]
		WHERE [RecipeId] = @RecipeId

		DELETE FROM [dbo].[RecipeIngredient]
		WHERE [RecipeId] = @RecipeId

		DELETE FROM [dbo].[Recipe]
		WHERE [Id] = @RecipeId

END
GO
/****** Object:  StoredProcedure [dbo].[GetComments_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetComments_sp] (
	 @RecipeId bigint = null
	,@UserId bigint = null
	,@PageNumber int
	,@ItemsPerPage int
)
AS
BEGIN
	SET NOCOUNT ON;

	if(@RecipeId IS NOT NULL)
	BEGIN

			SELECT 
				 a.[Id] CommentId
				,a.[RecipeId]
				,a.[Message]
				,a.[Date]
				,a.[AppUserId] [UserId] 
				,b.[FirstName]
				,b.[LastName]
				,b.[AvatarUrl]
				,CAST(ISNULL([IsLike], 0) AS BIT) Ilike
				,CASE WHEN a.[CommenParentId] = 0 THEN null ELSE a.[CommenParentId] END CommenParentId
			FROM [dbo].[Comment] a
			INNER JOIN [dbo].[AppUser] b
				ON a.[AppUserId] = b.[Id]
			LEFT JOIN [dbo].[CommentLike] c
				ON c.[CommentId] =  a.[Id]
				AND a.[AppUserId] = c.[AppUserId]
			WHERE a.[RecipeId] = @RecipeId
			ORDER BY a.[Date] DESC
			OFFSET @ItemsPerPage * (@PageNumber - 1) ROWS
		FETCH NEXT @ItemsPerPage ROWS ONLY OPTION (RECOMPILE);

	END
	ELSE 
	BEGIN

			SELECT 
				 a.[Id] CommentId
				,a.[RecipeId]
				,a.[Message]
				,a.[Date]
				,a.[AppUserId] [UserId] 
				,b.[FirstName]
				,b.[LastName]
				,b.[AvatarUrl]
				,CAST(ISNULL([IsLike], 0) AS BIT) Ilike
				,CASE WHEN a.[CommenParentId] = 0 THEN null ELSE a.[CommenParentId] END CommenParentId
			FROM [dbo].[Comment] a
			INNER JOIN [dbo].[Recipe] d
				ON d.[Id] = a.[RecipeId]
			INNER JOIN [dbo].[AppUser] b
				ON a.[AppUserId] = b.[Id]
			LEFT JOIN [dbo].[CommentLike] c
				ON c.[CommentId] =  a.[Id]
				AND a.[AppUserId] = c.[AppUserId]
			WHERE d.[AppUserId] = @UserId
			ORDER BY a.[Date] DESC
			OFFSET @ItemsPerPage * (@PageNumber - 1) ROWS
		FETCH NEXT @ItemsPerPage ROWS ONLY OPTION (RECOMPILE);
	END

END
GO
/****** Object:  StoredProcedure [dbo].[GetEstates_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetEstates_sp]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT  [Id]
		,[Name]
	FROM [dbo].[Estates]
	ORDER BY [Name];
END
GO
/****** Object:  StoredProcedure [dbo].[GetIngredients_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetIngredients_sp] (
	@Query nvarchar(100)
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT UPPER([Description]) [Description]
	FROM [dbo].[Ingredient]
	WHERE [Description] LIKE '%' + @Query + '%'

	UNION

	SELECT DISTINCT 'PASTA DE ' + UPPER([Description]) [Description]
	FROM [dbo].[Products]
	WHERE [Description] LIKE '%' + @Query + '%'
	AND [Enabled] = 1

END
GO
/****** Object:  StoredProcedure [dbo].[GetIngredientUnits_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetIngredientUnits_sp] (
	@Query nvarchar(100) = null
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [Description] Unit
	FROM [dbo].[Unit]
	WHERE [Description] LIKE '%' + @Query + '%'
END
GO
/****** Object:  StoredProcedure [dbo].[GetProduct_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetProduct_sp] (
	@BarCode nvarchar(50)
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [Ean] BarCode
		,[Description] Nombre
		,[Image] ImageUrl
	FROM [dbo].[Products]
	WHERE [Ean] = @BarCode
	AND [Enabled] = 1
END
GO
/****** Object:  StoredProcedure [dbo].[GetRecipe_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--[dbo].[GetRecipe_sp] 1, 98
CREATE PROCEDURE [dbo].[GetRecipe_sp] (
	  @UserId bigint
	 ,@RecipeId bigint
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		 a.[Id] RecipeId
		,a.[Name] Title
		,CAST(a.[Score] AS money) Rating
		,f.[Id] Difficulty
		,CAST(a.[Time] AS money) CookTime
		,a.[Likes]
		,a.[Video] VideoUrl
		,a.[Image] Image1Url
		,a.[Image2] Image2Url
		,a.[Image3] Image3Url
		,a.[Qr] Barcode
		,a.[Date]
		,b.[Id] UserId
		,b.[FirstName]
		,b.[LastName]
		,b.[AvatarUrl]
		,ISNULL(c.[Score], 0)  MyRating
		,CAST(CASE WHEN d.[AppUserId] IS NOT NULL THEN 1 ELSE 0 END AS BIT) IsFavourite
		,CAST(CASE WHEN e.[AppUserId] IS NOT NULL THEN 1 ELSE 0 END AS BIT) IsLike
	FROM [dbo].[Recipe] a
	INNER JOIN [dbo].[AppUser] b
		ON a.[AppUserId] = b.[Id]
	LEFT JOIN [dbo].[RecipeScore] c
		ON a.[Id] = c.[RecipeId]
		AND c.[AppUserId] = @UserId
	LEFT JOIN [dbo].[RecipeFavourite] d
		ON a.[Id] = d.[RecipeId]
		AND d.[AppUserId] = @UserId
	LEFT JOIN [dbo].[RecipeLike] e
		ON a.[Id] = e.[RecipeId]
		AND e.[AppUserId] = @UserId
	INNER JOIN [dbo].[Level] f
		ON a.[Level] = f.[Level]
	WHERE a.[Id] = @RecipeId
	--AND a.[Approved] = 1
	

END
GO
/****** Object:  StoredProcedure [dbo].[GetRecipeCookStepsByRecipeId_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetRecipeCookStepsByRecipeId_sp] (
	 @RecipeId bigint
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [Id] CookStepId
		,[RecipeId]
		,[Order]
		,[Description]
	FROM [dbo].[RecipeCookStep]
	WHERE [RecipeId] = @RecipeId
	ORDER BY [Order];

END
GO
/****** Object:  StoredProcedure [dbo].[GetRecipeIngredientsByRecipeId_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetRecipeIngredientsByRecipeId_sp] (
	 @RecipeId bigint
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [Id] IngredientId
		,[RecipeId]
		,[Order]
		,[Quantity]
		,[Unit]
		,[Description]
	FROM [dbo].[RecipeIngredient]
	WHERE [RecipeId] = @RecipeId
	ORDER BY [Order];

END
GO
/****** Object:  StoredProcedure [dbo].[GetRecipesByFilters_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetRecipesByFilters_sp] (
	 @PageNumber int
	,@ItemsPerPage int
	,@Barcode nvarchar(300) = null
	,@Latest bit 
	,@Popular bit
	,@Title nvarchar(300) = null
)
AS
BEGIN
	SET NOCOUNT ON;

	if(@Latest = 1)
	begin
		SELECT 
			 a.[Id] RecipeId
			,a.[Name] Title
			,CAST(a.[Score] AS money) Rating
			,c.[Id] Difficulty
			,CAST(a.[Time] AS money) CookTime
			,a.[Likes]
			,a.[Video] VideoUrl
			,a.[Image] Image1Url
			,a.[Image2] Image2Url
			,a.[Image3] Image3Url
			,a.[Date]
			,a.[Qr] Barcode
			,b.[Id] AppUserId
			,b.[FirstName]
			,b.[LastName]
			,b.[AvatarUrl]
		FROM [dbo].[Recipe] a
		INNER JOIN [dbo].[AppUser] b
			ON a.[AppUserId] = b.[Id]
		INNER JOIN [dbo].[Level] c
			ON a.[Level] = c.[Level]
		WHERE a.[Qr]  = ISNULL(@Barcode, a.[Qr])
		AND a.[Name] LIKE  '%' + ISNULL(@Title, a.[Name])  + '%'
		AND a.[Approved] = 1
		ORDER BY a.[Date] DESC
		OFFSET @ItemsPerPage * (@PageNumber - 1) ROWS
		FETCH NEXT @ItemsPerPage ROWS ONLY OPTION (RECOMPILE);
	end
	else if(@Popular = 1)
	begin
		SELECT 
			 a.[Id] RecipeId
			,a.[Name] Title
			,CAST(a.[Score] AS money) Rating
			,c.[Id] Difficulty
			,CAST(a.[Time] AS money) CookTime
			,a.[Likes]
			,a.[Video] VideoUrl
			,a.[Image] Image1Url
			,a.[Image2] Image2Url
			,a.[Image3] Image3Url
			,a.[Date]
			,a.[Qr] Barcode
			,b.[Id] AppUserId
			,b.[FirstName]
			,b.[LastName]
			,b.[AvatarUrl]
		FROM [dbo].[Recipe] a
		INNER JOIN [dbo].[AppUser] b
			ON a.[AppUserId] = b.[Id]
		INNER JOIN [dbo].[Level] c
			ON a.[Level] = c.[Level]
		WHERE a.[Qr]  = ISNULL(@Barcode, a.[Qr])
		AND a.[Name] LIKE  '%' + ISNULL(@Title, a.[Name])  + '%'
		AND a.[Approved] = 1
		ORDER BY a.[Likes] DESC
		OFFSET @ItemsPerPage * (@PageNumber - 1) ROWS
		FETCH NEXT @ItemsPerPage ROWS ONLY OPTION (RECOMPILE);
	end

	

END
GO
/****** Object:  StoredProcedure [dbo].[GetRecipesByUserId_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetRecipesByUserId_sp] (
	 @UserId bigint
	,@PageNumber int
	,@ItemsPerPage int
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		 a.[Id] RecipeId
		,a.[Name] Title
		,CAST(a.[Score] AS money) Rating
		,c.[Id] Difficulty
		,CAST(a.[Time] AS money) CookTime
		,a.[Likes]
		,a.[Video] VideoUrl
		,a.[Image] Image1Url
		,a.[Image2] Image2Url
		,a.[Image3] Image3Url
		,a.[Qr] Barcode
		,a.[Date]
		,b.[Id] UserId
		,b.[FirstName]
		,b.[LastName]
		,b.[AvatarUrl]
	FROM [dbo].[Recipe] a
	INNER JOIN [dbo].[AppUser] b
		ON a.[AppUserId] = b.[Id]
	INNER JOIN [dbo].[Level] c
		ON a.[Level] = c.[Level]
	WHERE a.[AppUserId] = @UserId

	ORDER BY a.[Date] DESC
	OFFSET @ItemsPerPage * (@PageNumber - 1) ROWS
	FETCH NEXT @ItemsPerPage ROWS ONLY OPTION (RECOMPILE);
	

END
GO
/****** Object:  StoredProcedure [dbo].[GetRecipesFavouritesByUserId_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetRecipesFavouritesByUserId_sp] (
	 @UserId bigint
	,@PageNumber int
	,@ItemsPerPage int
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		 a.[Id] RecipeId
		,a.[Name] Title
		,CAST(a.[Score] AS money) Rating
		,d.[Id] Difficulty
		,CAST(a.[Time] AS money) CookTime
		,a.[Likes]
		,a.[Video] VideoUrl
		,a.[Image] Image1Url
		,a.[Image2] Image2Url
		,a.[Image3] Image3Url
		,a.[Qr] Barcode
		,a.[Date]
		,b.[Id] UserId
		,b.[FirstName]
		,b.[LastName]
		,b.[AvatarUrl]
	FROM [dbo].[Recipe] a
	INNER JOIN [dbo].[AppUser] b
		ON b.[Id] = a.[AppUserId]
	INNER JOIN [dbo].[RecipeFavourite] c
		ON  a.[Id] = c.[RecipeId]
		AND c.[AppUserId] = @UserId
	INNER JOIN [dbo].[Level] d
		ON a.[Level] = d.[Level]
	WHERE a.[Approved] = 1
	ORDER BY a.[Date] DESC
	OFFSET @ItemsPerPage * (@PageNumber - 1) ROWS
	FETCH NEXT @ItemsPerPage ROWS ONLY OPTION (RECOMPILE);
	

END
GO
/****** Object:  StoredProcedure [dbo].[GetUserByEmail_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUserByEmail_sp] (
	 @Email nvarchar(300)
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [Id]
		,[FirstName]
		,[LastName]
		,[Email]
		,[Ocupation]
		,[EstateId]
		,[BirthDate]
		,[Gender]
		,[OauthToken]
		,[AuthenticationType]
		,[Password]
		,[AvatarUrl]
		,[FacebookId]
	FROM [dbo].[AppUser]
	WHERE [Email] = @Email

END
GO
/****** Object:  StoredProcedure [dbo].[GetUserById_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUserById_sp] (
	 @UserId bigint
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [Id]
		,[FirstName]
		,[LastName]
		,[Email]
		,[Ocupation]
		,[EstateId]
		,[BirthDate]
		,[Gender]
		,[OauthToken]
		,[AuthenticationType]
		,[Password]
		,[AvatarUrl]
		,[FacebookId]
	FROM [dbo].[AppUser]
	WHERE [Id] = @UserId

END
GO
/****** Object:  StoredProcedure [dbo].[GetUserStatistic_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUserStatistic_sp] (
	 @UserId bigint
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [AppUserId] UserId
		,[Followers]
		,[Following]
		,[Points]
	FROM [dbo].[AppUserStatistic]
	WHERE [AppUserId] = @UserId

END
GO
/****** Object:  StoredProcedure [dbo].[SaveComment_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SaveComment_sp] (
	 @RecipeId bigint
	,@UserId bigint
	,@Message text
	,@Date datetime
	,@CommenParentId bigint = null
)
AS
BEGIN
	SET NOCOUNT OFF;

	INSERT INTO [dbo].[Comment]
			   ([RecipeId]
			   ,[AppUserId]
			   ,[Message]
			   ,[Date]
			   ,[CommenParentId])
	VALUES
		(@RecipeId
		,@UserId
		,@Message
		,@Date
		,@CommenParentId)

END
GO
/****** Object:  StoredProcedure [dbo].[SaveRecipe_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SaveRecipe_sp] (
	 @RecipeId bigint
	,@Title nvarchar(300)
	,@UserId bigint
	,@Difficulty int
	,@CookTime decimal(18,0)
	,@Image1Url nvarchar(300) = null
	,@Image2Url nvarchar(300) = null
	,@Image3Url nvarchar(300) = null
	,@Barcode nvarchar(300) = null,
	 @NewRecipeId bigint OUTPUT
)
AS
BEGIN
	SET NOCOUNT OFF;


	if (EXISTS(SELECT TOP 1 [Id] FROM [dbo].[Recipe] WHERE [Id] = @RecipeId))
	begin
	
		DELETE FROM [dbo].[RecipeCookStep]
		WHERE [RecipeId] = @RecipeId

		DELETE FROM [dbo].[RecipeIngredient]
		WHERE [RecipeId] = @RecipeId

		UPDATE [dbo].[Recipe]
		   SET [Name] = @Title
			  ,[AppUserId] = @UserId
			  ,[Level] = (SELECT [Level] FROM [dbo].[Level] WHERE Id = @Difficulty)
			  ,[Time] = CAST(@CookTime AS varchar(50))
			  ,[Image] = @Image1Url
			  ,[Image2] = @Image2Url
			  ,[Image3] = @Image3Url
			  ,[Qr] = @Barcode
		 WHERE [Id] = @RecipeId

		 SELECT @NewRecipeId = @RecipeId;

	end
	else
	begin

		SELECT @NewRecipeId = IDENT_CURRENT('Recipe')
		
		INSERT INTO [dbo].[Recipe]
			([Name]
			,[Score]
			,[AppUserId]
			,[Level]
			,[Time]
			,[Likes]
			,[Image]
			,[Image2]
			,[Image3]
			,[Date]
			,[Qr]
			,[Approved])
		VALUES
			(@Title
			,0
			,@UserId
			,(SELECT [Level] FROM [dbo].[Level] WHERE Id = @Difficulty)
			,CAST(@CookTime AS varchar(50))
			,0
			,@Image1Url
			,@Image2Url
			,@Image3Url
			,GETDATE()
			,@Barcode
			,1)

	end


END
GO
/****** Object:  StoredProcedure [dbo].[SaveRecipeCookStep_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SaveRecipeCookStep_sp] (
	 @RecipeId bigint
	,@Order int
	,@Description nvarchar(300)
)
AS
BEGIN
	SET NOCOUNT OFF;


	INSERT INTO [dbo].[RecipeCookStep]
           ([RecipeId]
           ,[Order]
           ,[Description])
	VALUES
		(@RecipeId
		,@Order
		,@Description)

END
GO
/****** Object:  StoredProcedure [dbo].[SaveRecipeIngredient_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SaveRecipeIngredient_sp] (
	 @RecipeId bigint
	,@Order int
	,@Quantity decimal(18,0)
	,@Unit nvarchar(300)
	,@Description nvarchar(300)
)
AS
BEGIN
	SET NOCOUNT OFF;

	INSERT INTO [dbo].[RecipeIngredient]
           ([RecipeId]
           ,[Quantity]
           ,[Order]
           ,[Unit]
           ,[Description])
	VALUES
		(@RecipeId
		,@Quantity
		,@Order
		,@Unit
		,@Description)

END
GO
/****** Object:  StoredProcedure [dbo].[SaveUser_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SaveUser_sp] (
	 @FirstName nvarchar(300)
	,@LastName nvarchar(300)
	,@Email nvarchar(300)
	,@Ocupation nvarchar(300)
	,@EstateId int
	,@BirthDate date
	,@Gender int
	,@OauthToken nvarchar(300) = null
	,@AuthenticationType int
	,@Password nvarchar(300) = null
	,@AvatarUrl nvarchar(300) = null
	,@FacebookId nvarchar(300) = null
)
AS
BEGIN
	SET NOCOUNT OFF;

	if(EXISTS(SELECT TOP 1 [Id] FROM [dbo].[AppUser] WHERE LTRIM(RTRIM(LOWER([Email]))) = LTRIM(RTRIM(LOWER(@Email)))))
	begin

		UPDATE [dbo].[AppUser]
		   SET [FirstName] = @FirstName
			  ,[LastName] = @LastName
			  ,[Email] = @Email
			  ,[Ocupation] = @Ocupation
			  ,[EstateId] =  @EstateId
			  ,[BirthDate] = @BirthDate
			  ,[Gender] = @Gender
			  ,[OauthToken] = @OauthToken
			  ,[AuthenticationType] = @AuthenticationType
			  ,[Password] = @Password
			  ,[AvatarUrl] = @AvatarUrl
			  ,[FacebookId] = @FacebookId
		 WHERE [Email] = @Email

	end
	else
	begin
	
		INSERT INTO [dbo].[AppUser]
			([FirstName]
			,[LastName]
			,[Email]
			,[Ocupation]
			,[EstateId]
			,[BirthDate]
			,[Gender]
			,[OauthToken]
			,[AuthenticationType]
			,[Password]
			,[AvatarUrl]
			,[FacebookId])
		VALUES
			(@FirstName
			,@LastName
			,@Email
			,@Ocupation
			,@EstateId
			,@BirthDate
			,@Gender
			,@OauthToken
			,@AuthenticationType
			,@Password
			,@AvatarUrl
			,@FacebookId)

	end

END
GO
/****** Object:  StoredProcedure [dbo].[SetCommentLike_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SetCommentLike_sp] (
	 @UserId bigint
	,@CommentId bigint
	,@IsLike bit
)
AS
BEGIN
	SET NOCOUNT OFF;

	DELETE FROM [dbo].[CommentLike]
	WHERE [CommentId] = @CommentId
	AND [AppUserId] = @UserId

	if(@IsLike = 1) 
	begin

		INSERT INTO [dbo].[CommentLike]
			([CommentId]
			,[AppUserId]
			,[IsLike])
		VALUES
			(@CommentId
			,@UserId
			,@IsLike)

	end

END
GO
/****** Object:  StoredProcedure [dbo].[SetRecipeAsFavourite_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SetRecipeAsFavourite_sp] (
	 @UserId bigint
	,@RecipeId bigint
	,@IsFavourite bit
)
AS
BEGIN
	SET NOCOUNT OFF;

	DELETE FROM [dbo].[RecipeFavourite]
	WHERE [RecipeId] = @RecipeId
	AND [AppUserId] = @UserId

	if(@IsFavourite = 1)
	begin

	INSERT INTO [dbo].[RecipeFavourite]
		([RecipeId]
		,[AppUserId])
	VALUES
		(@RecipeId
		,@UserId)

	end

END
GO
/****** Object:  StoredProcedure [dbo].[SetRecipeLike_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SetRecipeLike_sp] (
	 @UserId bigint
	,@RecipeId bigint
	,@IsLike bit
)
AS
BEGIN
	SET NOCOUNT OFF;

	DELETE FROM [dbo].[RecipeLike]
	WHERE [RecipeId] = @RecipeId
	AND [AppUserId] = @UserId

	if(@IsLike = 1) 
	begin

		INSERT INTO [dbo].[RecipeLike]
			([RecipeId]
			,[AppUserId]
			,[IsLike])
		VALUES
			(@RecipeId
			,@UserId
			,@IsLike)

	end

	UPDATE [dbo].[Recipe] SET	
		[Likes] = (SELECT COUNT([RecipeId]) FROM [dbo].[RecipeLike] WHERE [IsLike] = 1 )
	WHERE [Id] = @RecipeId

END
GO
/****** Object:  StoredProcedure [dbo].[SetRecipeRating_sp]    Script Date: 10/11/2018 12:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SetRecipeRating_sp] (
	 @UserId bigint
	,@RecipeId bigint
	,@Rating decimal
)
AS
BEGIN
	SET NOCOUNT OFF;

	DELETE FROM [dbo].[RecipeScore]
	WHERE [RecipeId] = @RecipeId
	AND [AppUserId] = @UserId

	INSERT INTO [dbo].[RecipeScore]
		([RecipeId]
		,[AppUserId]
		,[Score])
	VALUES
		(@RecipeId
		,@UserId
		,@Rating)

	UPDATE [dbo].[Recipe] SET	
		[Score] = (SELECT SUM([Score]) / COUNT([RecipeId]) FROM [dbo].[RecipeScore] WHERE [RecipeId] = @RecipeId )
	WHERE [Id] = @RecipeId

END
GO
USE [master]
GO
ALTER DATABASE [MiPasta2] SET  READ_WRITE 
GO
