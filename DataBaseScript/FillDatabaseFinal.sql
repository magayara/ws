USE [MiPasta2]
GO
SET IDENTITY_INSERT [dbo].[Category] ON 
GO
INSERT [dbo].[Category] ([Id], [Description]) VALUES (1, N'AGUADAS')
GO
INSERT [dbo].[Category] ([Id], [Description]) VALUES (2, N'FIDEOS')
GO
INSERT [dbo].[Category] ([Id], [Description]) VALUES (3, N'PASTA INTEGRAL')
GO
INSERT [dbo].[Category] ([Id], [Description]) VALUES (4, N'PASTA KIDS')
GO
INSERT [dbo].[Category] ([Id], [Description]) VALUES (5, N'PASTAS 500g LA MODERNA')
GO
INSERT [dbo].[Category] ([Id], [Description]) VALUES (6, N'PASTAS CON VEGETALES LA MODERNA')
GO
INSERT [dbo].[Category] ([Id], [Description]) VALUES (7, N'PASTAS SABORIZADAS LA MODERNA')
GO
INSERT [dbo].[Category] ([Id], [Description]) VALUES (8, N'PASTAS SECAS')
GO
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
SET IDENTITY_INSERT [dbo].[Products] ON 
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (1, 1, N'ALMEJA', N'20/200g', N'7501018310745', 1, CAST(N'2018-09-17T13:33:49.747' AS DateTime), NULL, 4189, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (2, 1, N'ALMEJA', N'20/220g', N'7501018314583', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4190, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (3, 1, N'CARACOL 1', N'20/200g', N'7501018310554', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4191, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (4, 1, N'CARACOL 2', N'20/200g', N'7501018310561', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4192, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (5, 1, N'CARACOL 2', N'20/220g', N'7501018314606', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4193, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (6, 1, N'CARACOL 2', N'20/450g', N'7501018310967', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4194, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (7, 1, N'CODO 1', N'20/200g', N'7501018310516', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4195, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (8, 1, N'CORBATA', N'20/200g', N'7501018310738', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4196, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (9, 1, N'CORBATA', N'20/220g', N'7501018314682', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4197, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (10, 1, N'CORONA', N'20/200g', N'7501018310219', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4198, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (11, 1, N'CRINOLINA', N'20/200g', N'7501018310226', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4199, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (12, 1, N'ENGRANE', N'20/200g', N'7501018310585', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4200, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (13, 1, N'ENGRANE', N'20/220g', N'7501018314675', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4201, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (14, 1, N'ESTRELLA', N'20/200g', N'7501018310318', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4202, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (15, 1, N'ESTRELLA', N'20/220g', N'7501018314651', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4203, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (16, 1, N'HONGO', N'20/200g', N'7501018310769', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4204, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (17, 1, N'LENGUA', N'20/200g', N'7501018310233', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4205, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (18, 1, N'LENGUA', N'20/220g', N'7501018314613', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4206, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (19, 1, N'LENTEJA', N'20/200g', N'7501018310295', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4207, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (20, 1, N'LETRA', N'20/200g', N'7501018310257', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4208, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (21, 1, N'LETRA', N'20/220g', N'7501018314552', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4209, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (22, 1, N'LETRA', N'20/450g', N'7501018310998', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4210, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (23, 1, N'MACARRONCITO', N'20/200g', N'7501018310080', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4211, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (24, 1, N'MOÑITO', N'20/200g', N'7501018310752', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4212, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (25, 1, N'MOÑITO', N'20/220g', N'7501018314576', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4213, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (26, 1, N'MOÑO MEDIANO', N'20/200g', N'7501018310790', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4214, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (27, 1, N'MUNICION', N'20/200g', N'7501018310264', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4215, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (28, 1, N'MUNICION', N'20/220g', N'7501018314590', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4216, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (29, 1, N'OJITO', N'20/200g', N'7501018310240', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4217, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (30, 1, N'OJITO', N'20/220g', N'7501018314699', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4218, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (31, 1, N'SOMBRERO', N'20/200g', N'7501018310776', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4219, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (32, 1, N'TALLARIN 1', N'20/200g', N'7501018310042', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4220, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (33, 1, N'TALLARIN 1', N'20/220g', N'7501018314637', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4221, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (67, 2, N'CADEJO CAMBRAY', N'20/180g', N'7501018310073', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4222, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (68, 2, N'CADEJO MEDIANO', N'20/180g', N'7501018310066', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4223, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (69, 2, N'FIDEO 0', N'20/220g', N'7501018314514', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4224, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (70, 2, N'FIDEO 0', N'20/200g', N'7501018310011', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4225, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (71, 2, N'FIDEO 0', N'20/450g', N'7501018310981', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4226, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (72, 2, N'FIDEO 1', N'20/200g', N'7501018310028', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4227, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (73, 2, N'FIDEO 1', N'20/220g', N'7501018314545', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4228, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (74, 2, N'FIDEO2', N'20/200g', N'7501018310035', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4229, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (75, 2, N'FIDEO2', N'20/220g', N'7501018314538', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4230, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (85, 3, N'FIDEO 0', N'20/200g', N'7501018319069', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4262, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (86, 3, N'CODO', N'20/200g', N'7501018319014', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4263, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (87, 3, N'TORNILLO', N'20/200g', N'7501018319038', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4264, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (88, 3, N'PLUMA ', N'20/200g', N'7501018319021', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4265, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (89, 3, N'SPAGHETTI', N'20/200g', N'7501018319007', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4266, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (90, 3, N'Letra Integral La Moderna', N' 20/200g', N'7501018319083', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4267, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (97, 4, N'ANIMALES DE LA SELVA', N'20/200g', N'7501018310622', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4268, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (98, 4, N'DINOSAURIOS', N'20/200g', N'7501018310639', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4269, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (99, 4, N'FIGURAS DEL OESTE', N'20/200g', N'7501018310646', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4270, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (100, 4, N'PLUMA TRICOLOR ', N'20/200g', N'7501018319113', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4271, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (101, 4, N'TORNILLO TRICOLOR ', N'20/200g', N'7501018319106', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4272, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (102, 4, N'DEPORTES ', N'20/200g', N'7501018310653', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4273, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (103, 4, N'Letra kids', N'20/200g', N'7501018311346', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4274, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (111, 5, N'Spaghetti LM 20/500gr', N'20/500g', N'7501018311353', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4287, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (112, 5, N'Farfalle LM 10/500gr', N'10/500g', N'7501018311360', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4288, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (113, 5, N'Conchiglioni LM 10/500gr', N'10/500g', N'7501018311377', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4289, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (114, 5, N'Rotini LM 10/500gr ', N'10/500g', N'7501018311384', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4290, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (115, 5, N'Codo 4 LM 10/500gr', N'10/500g', N'7501018311391', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4291, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (116, 5, N'Spaghetti LM Ajo-Albahaca 20/500gr', N'20/500g', N'7501018312404', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4292, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (117, 5, N'Fettuccine Chipotle LM 20/500gr', N'20/500g', N'7501018312411', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4293, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (118, 5, N'LASAGNA LM 12/500g ', N'12/500g', N'7501018314026', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4294, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (127, 6, N'FIDEO 0 VEGETALES  ', N'20/200g', N'7501018319175', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4275, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (128, 6, N'FIDEO 2 VEGETALES ', N'20/200g', N'7501018319168', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4276, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (129, 6, N'CODO 2 VEGETALES ', N'20/200g', N'7501018319144', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4277, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (130, 6, N'LETRA VEGETALES ', N'20/200g', N'7501018319151', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4278, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (131, 6, N'CODO 2  VEGETALES LM 220G', N'20/220g', N'7501018314712', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4279, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (132, 6, N'LETRA  VEGETALES LM 220G', N'20/220g', N'7501018314729', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4280, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (133, 6, N'FIDEO 2  VEGETALES LM 220G', N'20/220g', N'7501018314736', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4281, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (134, 6, N'FIDEO 0 VEGETALES LM 220G', N'20/220g', N'7501018314743', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4282, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (135, 6, N'Rotini Vegetales LM 10/450 G', N'10/450g', N'7501018311049', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4283, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (136, 6, N'Spaghetti Vegetales LM 20/200G', N'20/200g', N'7501018310202', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4284, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (147, 7, N'SPAGHETTI MANTEQ LM 20/200G', N'20/200g', N'7501018312381', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4285, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (148, 7, N'SPAGHETTI CHIPOTLE LM 20/200G', N'20/200g', N'7501018312398', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4286, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (151, 8, N'CARACOL 3', N'20/200g', N'7501018310578', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4231, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (152, 8, N'CARACOL 4', N'20/200g', N'7501018310615', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4232, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (153, 8, N'CODO 2', N'20/200g', N'7501018310523', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4233, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (154, 8, N'CODO 2', N'20/220g', N'7501018314521', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4234, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (155, 8, N'CODO 2', N'20/450g', N'7501018310974', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4235, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (156, 8, N'CODO 3', N'20/200g', N'7501018310530', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4236, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (157, 8, N'CODO 3', N'20/220g', N'7501018314569', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4237, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (158, 8, N'CODO 4', N'20/200g', N'7501018310547', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4238, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (159, 8, N'CODO 4', N'20/220g', N'7501018314668', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4239, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (160, 8, N'MOÑO ', N'20/200g', N'7501018310721', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4240, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (161, 8, N'MACARRON', N'20/200g', N'7501018310097', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4241, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (162, 8, N'ESPIRAL', N'20/200g', N'7501018310714', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4242, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (163, 8, N'PENNE RIGATE', N'20/450g', N'7501018311001', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4243, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (164, 8, N'PLUMA ', N'20/200g', N'7501018310608', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4244, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (165, 8, N'PLUMA ', N'20/220g', N'7501018314620', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4245, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (166, 8, N'ROTINI (Tornillo) ', N'20/450g', N'7501018311018', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4246, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (167, 8, N'TALLARIN 2', N'20/200g', N'7501018310059', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4247, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (168, 8, N'TORNILLO', N'20/200g', N'7501018310592', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4248, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (169, 8, N'FETTUCCINE', N'20/200g', N'7501018310141', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4249, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (170, 8, N'FIDEO CABELLO DE ANGEL', N'20/200g', N'7501018310158', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4250, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (171, 8, N'LINGUINE', N'20/200g', N'7501018310134', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4251, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (172, 8, N'MACARRON LARGO', N'20/200g', N'7501018310110', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4252, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (173, 8, N'MACARRON LARGO', N'20/220g', N'7501018314644', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4253, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (174, 8, N'SPAGHETTI', N'20/200g', N'7501018310103', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4254, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (175, 8, N'SPAGHETTI', N'20/220g', N'7501018314507', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4255, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (176, 8, N'SPAGHETTI', N'20/450g', N'7501018310943', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4256, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (177, 8, N'SPAGHETTI ITALIANO', N'20/200g', N'7501018310127', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4257, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (178, 8, N'SPAGHETTI LA MODERNA', N'4/1kg', N'7501018321000', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4258, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (179, 8, N'LONCHERA LA MODERNA', N'20/200g', N'7501018310509', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4259, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (180, 8, N'LONCHERA SPAGHETTI', N'20/200g', N'7501018310820', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4260, NULL)
GO
INSERT [dbo].[Products] ([Id], [CategoryId], [Description], [Grammage], [Ean], [Enabled], [CreatedAt], [UpdatedAt], [oldId], [Image]) VALUES (181, 8, N'LONCHERA FIDEO ', N'20/200g', N'7501018310837', 1, CAST(N'2018-09-17T13:34:14.540' AS DateTime), NULL, 4261, NULL)
GO
SET IDENTITY_INSERT [dbo].[Products] OFF
GO
SET IDENTITY_INSERT [dbo].[Ingredient] ON 
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (1, N'Aceite')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (2, N'Aceite de ajonjolí')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (3, N'Aceite de canola')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (4, N'Aceite de oliva')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (5, N'Aceite en aerosol')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (6, N'Aceitunas negras sin hueso')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (7, N'Aceitunas verdes sin hueso')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (8, N'Acelgas en trozos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (9, N'Aderezo italiano')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (10, N'Agua')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (11, N'Agua o caldo de pollo sin sal')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (12, N'Aguacate, rebanado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (13, N'Ajo')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (14, N'Ajonjolí blanco, tostado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (15, N'Ajonjolí negro, tostado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (16, N'Albahaca')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (17, N'Almejas blancas, muy fescas y enjuagadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (18, N'Almendras')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (19, N'Almendras en mitades (con piel) y tostadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (20, N'Almendras tostadas y ligeramente picadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (21, N'Almendras, tostadas y en mitades')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (22, N'Animales de la selva')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (23, N'Apio en cubos chicos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (24, N'Arándanos deshidratados')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (25, N'Arúgula baby, lavada y desinfectada')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (26, N'Atún ')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (27, N'Avellanas, tostadas y en mitades')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (28, N'Avena molida')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (29, N'Azúcar')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (30, N'Azúcar mascabado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (31, N'Brocoli')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (32, N'Brócoli en ramitos y blanqueado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (33, N'Calabacita(s), en julianas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (34, N'Calabacitas en rodajas delgadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (35, N'Calabacitas, en láminas delgadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (36, N'Calabacitas, en medias lunas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (37, N'Caldillo de tomate, frio')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (38, N'Caldo de frijol')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (39, N'Caldo de pescado o agua')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (40, N'Caldo de pollo o vegetales sin grasa ni sal')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (41, N'Caldo de pollo sin sal')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (42, N'Caldo de polo sin sal')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (43, N'Caldo de res sin sal')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (44, N'Caldo de vegetales, sin sal')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (45, N'Camarón(es)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (46, N'Canela (chica)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (47, N'Caracol 1')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (48, N'Carne molida')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (49, N'Carne molida de res')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (50, N'Cebolla')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (51, N'Cebolla morada, en julianas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (52, N'Cebolla morada, fileteada')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (53, N'Cebolla morada, finamente picada')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (54, N'Cebolla, fileteada')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (55, N'Cebolla, finamente picada')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (56, N'Champiñon(es)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (57, N'Champiñon(es), en cuartos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (58, N'Champiñon(es), rebanados')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (59, N'Chícharos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (60, N'Chícharos, blanqueados')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (61, N'Chicharrón de cerdo, troceado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (62, N'Chile ancho, limpio y picado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (63, N'Chile chipotles adobados, sin semillas y picados')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (64, N'Chile de árbol fresco, sin semillas ni venas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (65, N'Chile de árbol seco')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (66, N'Chile de árbol seco, limpio y picado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (67, N'Chile de árbol, finamente picado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (68, N'Chile guajillo asado y en julianas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (69, N'Chile guajillo sin semillas y picado (opcional)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (70, N'Chile guajillo, limpio, asado e hidratado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (71, N'Chile morita, asado e hidratado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (72, N'Chile morita, sin semillas y finamente picado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (73, N'Chile piquin molido')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (74, N'Chile piquín molido')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (75, N'Chiles de árbol, frescos en julianas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (76, N'Chiles de árbol, frescos, sin semillas y finamente picados')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (77, N'Chiles poblanos chicos, limpios, escalfados y en rajas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (78, N'Chiles poblanos desvenados y pelados')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (79, N'Chiles serranos, sin semillas y picados')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (80, N'Chorizo español (en rodajas gruesas)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (81, N'Cilantro lavado, desinfectado y picado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (82, N'Ciruelas pasa sin hueso, hidratadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (83, N'Claras de huevo')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (84, N'Clavos de olor')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (85, N'Codo 4')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (86, N'Codo con fibra la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (87, N'Codo no. 4 la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (88, N'Codo no.1 la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (89, N'Codo no.3 la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (90, N'Codo no.4 la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (91, N'Col blanca, lavada, desinfectada y fileteada')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (92, N'Coles de bruselas, en mitades y blanqueadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (93, N'Coliflor mediana, en ramitos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (94, N'Corazones de alcachofa (drenada)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (95, N'Corona')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (96, N'Crema ')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (97, N'Crema baja en grasa')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (98, N'Crema para batir')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (99, N'De ajo finalmente picado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (100, N'De albaca fresca')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (101, N'De champiñones rebanados')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (102, N'De chiles chipotles adobados de lata o al gusto')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (103, N'De crema agria ')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (104, N'De queso parmesano')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (105, N'De sal')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (106, N'De trozos de piña en cubitos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (107, N'Diente de ajo')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (108, N'Diente de ajo (finamente picado)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (109, N'Diente de ajo (picado)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (110, N'Diente de ajo en láminas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (111, N'Diente de ajo finamente picado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (112, N'Diente de ajo picado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (113, N'Diente de ajo, en cuartos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (114, N'Diente de ajo, fileteados')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (115, N'Diente de ajo, finamente picado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (116, N'Diente de ajo, picado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (117, N'Dientes de ajo')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (118, N'Dientes de ajo (finamente picados)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (119, N'Dientes de ajo (picados)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (120, N'Dientes de ajo finamente picados')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (121, N'Dientes de ajo, en julianas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (122, N'Dientes de ajo, finamente picados')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (123, N'Dientes de ajo, picados')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (124, N'Dinosaurios la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (125, N'Duraznos grandes en mitades y sin hueso')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (126, N'Echalote, en mitades')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (127, N'Echalote, finamente picado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (128, N'Echalotes (finamente picados)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (129, N'Ejotes (blanqueados y en mitades)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (130, N'Ejotes, limpios, en mitades y blanqueados')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (131, N'Elotes amarillos congelados, descongelados')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (132, N'Eneldo fresco (picado)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (133, N'Engrane la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (134, N'Epazote lavado y desinfectado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (135, N'Espárragos, blanqueados y cortados en cuatro')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (136, N'Espinacas baby')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (137, N'Espinacas baby, lavadas y desinfectadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (138, N'Espinacas lavadas, desinfectadas y picadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (139, N'Espinacas, lavadas y desinfectadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (140, N'Espiral la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (141, N'Estragón fresco (picado)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (142, N'Fécula de maiz')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (143, N'Fettuccine la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (144, N'Fetuccine la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (145, N'Fideo 0')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (146, N'Fideo 2')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (147, N'Fideo cadejo cambray')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (148, N'Fideo con fobra la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (149, N'Fideo no.1 la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (150, N'Fideo no.2 la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (151, N'Figuras del oeste la moderna kids')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (152, N'Flada o chambarete de res, cocida y deshebrada')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (153, N'Flores de jamainca hidratadas y ligeramente picadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (154, N'Florez de calabaza limpias, lavadas y desinfectadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (155, N'Frijol negro cocido y molido')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (156, N'Frijoles bayos enteros, cocinados en agua con sal')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (157, N'Frijoles negros, cocinados en agua sin sal (sin caldo)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (158, N'Frijoles negros, cocinados en agua sin sal y escurridos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (159, N'Garbanzos, remojados la noche anterior')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (160, N'Granos de elote')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (161, N'Granos de elote amarillo')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (162, N'Granos de elote blanco')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (163, N'Granos de granada')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (164, N'Harina')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (165, N'Harina de trigo')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (166, N'Hierbas italianas secas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (167, N'Hoja de epazote, lavada, desinfectada y picada')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (168, N'Hoja de laurel')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (169, N'Hojas chicas de cilantro, lavadas y desinfectadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (170, N'Hojas de albahaca (lavadas y deinfectadas)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (171, N'Hojas de albahaca (lavadas y desinfectadas)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (172, N'Hojas de albahaca (lavadas, desinfectadas y picadas)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (173, N'Hojas de albahaca lavadas y desinfectadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (174, N'Hojas de albahaca, lavadas y desinfectadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (175, N'Hojas de cilantro')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (176, N'Hojas de cilantro (lavadas y desinfecatadas)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (177, N'Hojas de cilantro, lavadas y desinfectadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (178, N'Hojas de cilantro, lavadas, desinfectadas y picadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (179, N'Hojas de epazote, lavadas y desinfectadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (180, N'Hojas de laurel')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (181, N'Hojas de perejil lavadas y desinfectadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (182, N'Hojas de perejil lavadas, desinfectadas y finamente picadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (183, N'Hojas de perejil lavadas\ndesinfectadas y picadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (184, N'Hojas de perejil, lavadas y desinfectadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (185, N'Hojas de perejil, lavadas, desinfectadas y ligeramente picadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (186, N'Hojas de perejil, lavadas, desinfectadas y picadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (187, N'Hojuelas de maíz (molidas)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (188, N'Hongo portobello grande, en láminas delgadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (189, N'Hongo portobello grande, rebanado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (190, N'Hongos crimini, rebanados')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (191, N'Huevo (ligeramente batido)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (192, N'Huevos batidos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (193, N'Huevos duros, rebanados')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (194, N'Huitlacoche fresco,limpio')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (195, N'Jamón')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (196, N'Jamon de pechuga de pavo')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (197, N'Jamon de pechuga de pavo, en cubos grandes')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (198, N'Jamón de pierna sin sal en cubos chicos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (199, N'Jamón serrano, en rollos chicos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (200, N'Jengibre (finamente picado)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (201, N'Jerez')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (202, N'Jitomate (asados)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (203, N'Jitomate guaje')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (204, N'Jitomates cherry (en mitades)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (205, N'Jitomates en cuartos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (206, N'Jitomates en cuartos y sin semillas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (207, N'Jitomates sin semillas (en cubos medianos)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (208, N'Jotomate')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (209, N'Jugo de limon')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (210, N'Jugo de limón')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (211, N'Jugo de naranja')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (212, N'Laurel')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (213, N'Leche baja en garsa')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (214, N'Leche descremada')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (215, N'Lengua')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (216, N'Letra')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (217, N'Limón sin semillas (el jugo)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (218, N'Limones (el jugo)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (219, N'Limones amarillos (jugo y ralladura)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (220, N'Lomo de huachinango, en cubos grandes')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (221, N'Lonjas de salmón (140g c/u)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (222, N'Macarrón corto la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (223, N'Macarrón la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (224, N'Macarrón largo la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (225, N'Mango manila en cubos medianos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (226, N'Mantequilla sin sal')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (227, N'Mantequilla sin sal, fundida')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (228, N'Manzanas rojas en cubos chicos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (229, N'Matequilla sin sal')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (230, N'Medias pechugas de pollo (limpias y semiaplanadas)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (231, N'Medias pechugas de pollo, limpias y semi aplanadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (232, N'Mejorana fresca, picada')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (233, N'Menta lavada, desinfectada y finamente picada')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (234, N'Miel')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (235, N'Miel de abeja')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (236, N'Milanesa de pollo')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (237, N'Moño la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (238, N'Mortadela, en cuadros chicos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (239, N'Mostaza dijon')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (240, N'Mostaza dijón')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (241, N'Munición')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (242, N'Munición la moderna kids')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (243, N'Nbmn')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (244, N'Nueces')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (245, N'Nueces caramelizadas (lijeramente picadas)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (246, N'Nueces en mitades, tostadas y ligeramente picadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (247, N'Nueces, tostadas y picadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (248, N'Nuez de castilla, limpia')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (249, N'Nuez en mitades')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (250, N'Nuez moscada')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (251, N'Nuez moscada molida')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (252, N'Ojito')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (253, N'Orégano fresco (picado)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (254, N'Orégano fresco, picado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (255, N'Orégano seco, molido')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (256, N'Orégano seco, sal y pimienta negra molida')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (257, N'Orégano, sal y pimienta negra molida')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (258, N'Otra')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (259, N'Pan molido integral')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (260, N'Papa chica, en cubos chicos (con piel)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (261, N'Páprika')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (262, N'Pechuga de pavo horneada\nen rebanadas delgadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (263, N'Pechuga de pollo')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (264, N'Pechuga de pollo (limpia y semiaplanada)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (265, N'Pechuga de pollo cocinada y deshebrada')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (266, N'Pechuga de pollo en cubos medianos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (267, N'Pechuga de pollo, limpia y en cubos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (268, N'Peperoni rebanado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (269, N'Pepitas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (270, N'Perejil')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (271, N'Perejil fresco, finamente picado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (272, N'Perejil fresco, picado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (273, N'Perejil lavado, desinfectado y picado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (274, N'Perejil, finamente picado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (275, N'Perejil, lavado, desinfectado y picado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (276, N'Pimienta negra molida')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (277, N'Pimiento amarillo (en julianas)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (278, N'Pimiento amarillo, limpio y escalfado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (279, N'Pimiento morron')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (280, N'Pimiento naranja (en julianas)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (281, N'Pimiento rojo')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (282, N'Pimiento rojo en cubos chicos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (283, N'Pimiento verde')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (284, N'Pimiento verde (en julianas)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (285, N'Pimiento verde en cubos chicos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (286, N'Pimientos rojos, en cubos chicos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (287, N'Piña miel')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (288, N'Plátano macho en láminas delgadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (289, N'Pluma con fibra la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (290, N'Pluma la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (291, N'Puré de tomate')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (292, N'Queso azul')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (293, N'Queso chedar')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (294, N'Queso cotija')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (295, N'Queso cottage')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (296, N'Queso crema')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (297, N'Queso de cabra')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (298, N'Queso de cabra, desmoronado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (299, N'Queso emmental o gouda (rallado)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (300, N'Queso fresco en cubos chicos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (301, N'Queso fresco rallado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (302, N'Queso fresco, en cubos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (303, N'Queso fresco, en cubos chicos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (304, N'Queso gorgonzola')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (305, N'Queso gruyère')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (306, N'Queso manchego bajo en grasa, rallado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (307, N'Queso manchego, rallado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (308, N'Queso mozzarella fresco y en cubos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (309, N'Queso mozzarella, rallado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (310, N'Queso panela (en bastones)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (311, N'Queso parmesano')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (312, N'Queso parmesano (rallado)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (313, N'Queso parmesano, rallado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (314, N'Ralladura de cáscara de limón')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (315, N'Ralladura de cáscara de naranja')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (316, N'Rayadura de cascara de limon')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (317, N'Requezón sin sal o jocoque')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (318, N'Romero fresco, picado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (319, N'Sal')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (320, N'Sal y pimienta')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (321, N'Sal y pimienta negra molida')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (322, N'Sal y pimiental')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (323, N'Sal, orégano seco molido y pimienta de caneya molida')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (324, N'Salchichas de cerdo')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (325, N'Salchichas de pavo en rodajas y asadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (326, N'Salsa de soya')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (327, N'Salsa de tomate')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (328, N'Salsa de tomate para pizza')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (329, N'Salvia')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (330, N'Setas rebanadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (331, N'Spaghetti')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (332, N'Spaghetti 450')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (333, N'Spaghetti grueso no. 5')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (334, N'Spaghetti grueso no. 7')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (335, N'Spaghetti la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (336, N'Tallo de té de limón, finamente picadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (337, N'Tallo de té limón')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (338, N'Tallos de cilantro lavados, desinfectados y picados')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (339, N'Tallos de cilantro, lavados, desinfectados y picados')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (340, N'Tocino de cerdo ahumado (60g) en cubos chicos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (341, N'Tocino de cerdo ahumado, en cubos chicos (40g)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (342, N'Tocino de pavo, picadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (343, N'Tocino picado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (344, N'Tomate')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (345, N'Tomate (jitomate), asado y en cuartos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (346, N'Tomate (jitomate), en cuartos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (347, N'Tomate (jitomate), en trozos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (348, N'Tomates (jitomates) cherry')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (349, N'Tomates (jitomates) cherry, en mitades')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (350, N'Tomates (jitomates), en octavos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (351, N'Tomates cherry (en mitades)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (352, N'Tomates deshidratados, en mitades')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (353, N'Tomillo')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (354, N'Tomillo fresco')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (355, N'Tomillo fresco (picado)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (356, N'Tomillo fresco picado')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (357, N'Tomillo fresco, limpio')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (358, N'Tornillo')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (359, N'Tornillo con fibra la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (360, N'Tornillo la moderna')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (361, N'Tortillas en julianas y horneadas')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (362, N'Uvas rojas sin semillas, en mitades')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (363, N'Uvas verdes sin semillas,en mitades')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (364, N'Vegetales')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (365, N'Vinagre balsámico')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (366, N'Vinagre balsámico o vinagre manzana')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (367, N'Vinagre blanco')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (368, N'Vinagre de jerez')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (369, N'Vinagre de manzana')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (370, N'Vinagreta de tu preferencia')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (371, N'Vino blanco')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (372, N'Vino tinto')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (373, N'Yogurt natural bajo en grasa')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (374, N'Yogurt natural bajo en grasa sin azúcar')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (375, N'Zanahoria')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (376, N'Zanahoria (en julianas)')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (377, N'Zanahoria rayada')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (378, N'Zanahoria, en cubos chicos y blanqueados')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (379, N'Zanahorias en cubos chicos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (380, N'Zanahorias, en cubos chicos')
GO
INSERT [dbo].[Ingredient] ([Id], [Description]) VALUES (381, N'Zanahorias, en rodajas')
GO
SET IDENTITY_INSERT [dbo].[Ingredient] OFF
GO
SET IDENTITY_INSERT [dbo].[Unit] ON 
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (1, N'Bote')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (2, N'Chorro')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (3, N'Cucharada')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (4, N'Cucharadas')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (5, N'Diente')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (6, N'Dientes')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (7, N'Frasco')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (8, N'Gr')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (9, N'Hoja')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (10, N'Hojas')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (11, N'Kg')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (12, N'Lata')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (13, N'Latas')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (14, N'Lt')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (15, N'Lts')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (16, N'Ml')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (17, N'Paquete')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (18, N'Paquetes')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (19, N'Pieza')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (20, N'Piezas')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (21, N'Pisca')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (22, N'Pizca')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (23, N'Puñito')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (24, N'Raja')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (25, N'Ramas')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (26, N'Rebanadas')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (27, N'Tallo')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (28, N'Tallos')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (29, N'Taza')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (30, N'Tazas')
GO
INSERT [dbo].[Unit] ([Id], [Description]) VALUES (31, N'Trozos')
GO
SET IDENTITY_INSERT [dbo].[Unit] OFF
GO
SET IDENTITY_INSERT [dbo].[Recipe] ON 
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (94, N'Tornillos con crema y atún', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta con 25g de atún', 168, N'9', N'Calienta el agua junto con la sal en una olla alta hasta que hierva. Inmediatamente agrega el Tornillo La Moderna y cocínalo hasta que esté en su punto (suave por fuera y firme por dentro); revisa los tiempos de cocción marcados en el empaque. Escurre y reserva.\n \nCalienta el aceite en un sartén y saltea ajo, cebolla y pimiento por 2 minutos, o hasta que suavicen ligeramente. Incorpora la pasta, mezcla con los ingredientes y continua cocinando por 2 minutos más. Agrega las hojas de perejil, apaga el fuego y mezcla para que las hojas suavicen con el calor remanente en el sartén.\n\nCombina en un tazón el jugo y la ralladura de limón, la crema, sal y pimienta. Agrega la pasta del sartén y el atún previamente desmenuzado. Revuelve para integrar los sabores y disfruta de inmediato.', N'25', 1, 1, CAST(3 AS Numeric(18, 0)), 7, N'Fácil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310592', 1, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (95, N'Ensalada de pastas con arúgula, atún y vinagreta de chile piquín', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'1 taza de ensalada con ½ taza de pasta +50g de atún', 86, N'8', N'Para la vin', N'30', 1, 1, CAST(5 AS Numeric(18, 0)), 23, N'Difícil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-19T00:00:00.000' AS DateTime), N'', N'', N'7501018319014', 2, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (96, N'Sartén de pasta con salchichas y vegetales', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta', 164, N'7', N'Para la salsa Calienta el aceite en una olla a fuego medio y agrega ajo y chile de árbol. Mueve constantemente y permite que el ajo dore ligeramente. Incorpora el orégano junto con la cebolla y mueve regularmente hasta que la cebolla se torne traslúcida. ', N'25', 1, 1, CAST(5 AS Numeric(18, 0)), 6, N'Fácil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-18T00:00:00.000' AS DateTime), N'', N'', N'7501018310608', 3, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (97, N'Pasta hawaiana con piñas a la parrilla y aderezo de yogurt', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta + 30g de jamón con 3 cucharadas de aderezo', 156, N'6', N'Para La Sopa Calienta en una olla el agua con la sal hasta que hierva. Inmediatamente agrega la pasta Codo No.3 La Moderna® y cocina hasta que esté en su punto; revisa los tiempos de cocción marcados en el empaque. Escurre y reserva. Combina en un tazón a', N'15', 1, 1, CAST(5 AS Numeric(18, 0)), 7, N'Difícil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), N'', N'', N'7501018310530', 4, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (98, N'Pluma con hongos mixtos', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta con 1 taza de verduras', 164, N'8', N'Pon a hervir el agua con sal en una olla alta. Inmediatamente agrega la Pluma La Moderna® y cocina hasta que llegue casi a su punto. Escurre y reserva. Vierte el aceite en un sartén caliente y dora ligeramente el echalote. Añade los hongos, aumenta el sabor con sal, pimienta y mejorana; saltea por tres minutos o hasta suavizar los hongos. Agrega el vino e integra bien. Incorpora la pasta con las espinacas y retira del fuego después de un minuto. Sirve con queso de cabra y disfruta.', N'20', 1, 0, CAST(4 AS Numeric(18, 0)), 23, N'Fácil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), N'', N'', N'7501018310608', 5, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (99, N'Tornillos con jamón serrano y chícharos', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta + 30g de jamón serrano', 87, N'8', N'Calienta en el agua junto con la sal en una olla alta hasta que hierva. Inmediatamente agrega la pasta Tornillo La Moderna® y cocina hasta que esté en su punto. Escurre y reserva. Calienta la crema junto con la leche en un sartén semiprofundo hasta que hierva. Agrega la pasta al sartén, revuelve bien, y cocina hasta que la pasta absorba parte de la salsa. Agrega el queso, la pimienta y los chícharos, y mezcla permitiendo que el queso se funda. Retira del fuego e incorpora el jamón serrano y las hojas de perejil. Disfruta de inmediato.', N'20', 1, 1, CAST(5 AS Numeric(18, 0)), 9, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), N'', N'', N'7501018319038', 6, N'https://www.youtube.com/embed/bVdfj7HXuXE', 1)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (100, N'Pasta con nuez y hongos salteados', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta', 168, N'9', N'Calienta el agua junto con la sal en una olla alta hasta que hierva. Inmediatamente agrega la pasta Tornillo con Fibra La Moderna® y cocina hasta que esté en su punto (suave por fuera y firme por dentro). Escurre y reserva. Calienta el aceite en un sartén y cocina el tocino junto con el ajo, hasta que ambos doren. Integra la cole y saltea por un minuto o hasta que comience a suavizar. Agrega los champiñones y continúa salteando hasta que suavicen. Incorpora la pasta al sartén y revuelve con los vegetales. Añade nueces, sal, pimienta y chile ancho; integra perfectamente y cocina por un minuto más. Sirve caliente y disfruta.', N'20', 1, 1, CAST(5 AS Numeric(18, 0)), 8, N'Difícil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), N'', N'', N'7501018310592', 7, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (101, N'Fideos con salsa de coliflor y perejil', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta', 85, N'13', N'Para La Salsa Precalienta el horno a 180ºC. Coloca la coliflor, los echalotes y el ajo en un tazón. Agrega el aceite de canola, sal y pimienta, y revuelve bien para combinar todos los sabores. Pasa los vegetales a una charola para horno, dejándolos en una sola capa uniforme y hornea de 30 a 40 minutos o hasta que la coliflor suavice. Mezcla de manera ocasional para obtener una cocción uniforme. Toma los dientes de ajo con cuidado de no quemarte y retira la cáscara. Coloca todos los ingredientes que horneaste junto con la leche y la mostaza, dentro de un procesador de alimentos o licuadora, y procesa de 2 a 3 minutos hasta obtener una textura suave, ligeramente pesada y bien incorporada. Transfiere la salsa a un sartén y cocina por dos minutos más. Rectifica el sabor con sal y pimienta. Reserva. Para La Pasta Calienta el aceite en una olla a fuego medio, agrega el Fideo con fibra La Moderna® y cocina moviendo ocasionalmente, sin dejar que cambie de color. Incorpora el agua y hierve a fuego bajo de 10 a 12 minutos o hasta que el agua reduzca casi por completo y la pasta suavice. Agrega la salsa de coliflor una vez transcurrido el tiempo y continúa cocinando hasta que la pasta absorba la salsa y tenga una consistencia pesada. Retira del fuego, agrega el perejil y revuelve ligeramente. Sirve de inmediato y disfruta.', N'50', 1, 1, CAST(1 AS Numeric(18, 0)), 22, N'Difícil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), N'', N'', N'7501018319069', 8, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (102, N'Sopita de pasta con caldo de res', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'1 taza', 98, N'5', N'Calienta en una olla el caldo, agrega los vegetales y los garbanzos y hierve por 15 minutos a fuego bajo, para que los garbanzos suavicen. Añade los Dinosaurios La Moderna a la sopa y cocina por 8 minutos a fuego medio. Incorpora la carne junto con el queso y deja cocinar por 3 minutos más, o hasta que el queso se funda por completo. Rectifica el sabor con sal y pimienta. Sirve la sopa caliente y decora con cilantro. Disfruta.', N'30', 1, 1, CAST(5 AS Numeric(18, 0)), 7, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), N'', N'', N'7501018310639', 9, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (103, N'Ensalada de Pasta con elotitos, calabaza y cubos de pollo', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'1 taza de ensalada + ½ taza de pasta + 70g de pollo con 1 ½ cucharada de vinagreta', 99, N'10', N'Para la salsa Calienta en una olla el agua con la sal hasta que hierva. Inmediatamente agrega la pasta de Inmediatamente agrega la pasta de revisa los tiempos de cocción marcados en el empaque. Escurre y reserva. Combina en un tazón aceite de canola, chile piquín, romero, ajo, sal y pimienta. Añade los cubos de pollo y revuelve para que se impregnen con todo el sabor. Cocínalos en un sartén bien caliente hasta que doren y estén bien cocinados al centro. Mezcla en un tazón la pasta. Coloca todos los ingredientes dentro de un frasco con tapa y agita vigorosamente hasta formar una emulsión. Baña la ensalada con la vinagreta y revuelve para incorporar los sabores. Sirve en tazones individuales y disfruta.', N'14', 1, 1, CAST(5 AS Numeric(18, 0)), 7, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), N'', N'', N'7501018310646', 10, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (104, N'Croquetas de atún y municiones con dip de mango', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'5 croquetas de 30g c/u con 4 cucharadas de dip', 27, N'11', N'Para las croquetas. Precalienta tu horno a 180ºC. Calienta en una olla el agua con la sal hasta que hierva. Inmediatamente agrega la pasta Munición La Moderna® revisa los tiempos de cocción marcados en el empaque. Escurre y reserva. Mezcla en un tazón la pasta con el atún, la zanahoria y las claras de huevo hasta integrar y formar una mezcla compacta. Toma una porción de la mezcla con una cuchara y con tus manos dale forma de croqueta; repite con el resto de las porciones. Cubre las croquetas con harina, después pásalas por huevo batido y, por último, empanízala con la avena. Pásalas a una charola para horno con papel encerado, rocíalas con aceite en aerosol y hornea de 10 a 12 minutos o hasta que las croquetas tomen un tono dorado. Para el dip de mango. Licúa la pulpa de mango con la miel, el jugo de limón y el agua hasta obtener una consistencia suave y homogénea. Vierte el dip en un recipiente y mezcla con las hojas de menta. Sirve las croquetas y disfruta con el dip de mango.', N'20', 1, 1, CAST(5 AS Numeric(18, 0)), 7, N'Fácil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), N'', N'', N'7501018310264', 11, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (105, N'Frascos de sopa de dinosaurios con vegetales y pollo', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'1 taza con ½ taza de pasta + 35g de pollo', 98, N'6', N'Calienta una olla, vierte el aceite y cocina la pasta de Dinosaurios La Moderna® Kids!, moviendo constantemente, hasta que esté ligeramente dorada. Licúa los jitomates con un poco del caldo de pollo, cuela, vierte en la olla y deja hervir. Agrega el caldo restante y las zanahorias. Cocina a fuego bajo por 10 minutos e incorpora el resto de los vegetales y el pollo; rectifica el sabor con sal y pimienta. Sube el fuego y deja hasta hervir. Sirve en frascos de vidrio, agrega cubos de queso y disfruta.\n', N'20', 1, 1, CAST(4 AS Numeric(18, 0)), 7, N'Fácil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), N'', N'', N'7501018310639', 12, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (106, N'Sopa de pasta con hongos, elote y calabaza', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'1 taza de sopa con ½ taza de pasta', 7, N'7', N'Calienta el agua junto con la sal en una olla alta hasta que hierva. Inmediatamente agrega el Codo No.1 La Moderna® y cocina hasta que esté en su punto (suave por fuera y firme por dentro); revisa los tiempos de cocción marcados en el empaque. Escurre y reserva. Calienta el caldo de pollo en una olla junto con los vegetales y las hojas de epazote. Cocina hasta que los vegetales comiencen a suavizar, aproximadamente de 8 a 10 minutos. Agrega la pasta y cocina por dos minutos más. Sirve caliente y disfruta.', N'20', 1, 1, CAST(4 AS Numeric(18, 0)), 22, N'Fácil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), N'', N'', N'7501018310516', 13, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (107, N'Sopa de pasta y vegetales con cubos de huachinango', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'Porción recomendada: 1 taza de sopa con ½ taza de pasta', 12, N'10', N'Calienta el aceite por un minuto en una olla a fuego alto. Agrega el ajo y la cebolla y mueve constantemente hasta que la cebolla tome un tono traslúcido. Incorpora los granos de elote y permite que se tornen de color dorado. Añade la zanahoria y la papa, mueve ocasionalmente por dos minutos para que comiencen a suavizar. Incorpora la calabacita y el apio y cocina por un minuto más. Vierte el caldo, baja el fuego y aumenta el sabor con sal, pimienta, tomillo y laurel. Tapa la olla y hierve por 10 minutos. Agrega la pasta y cocina por ocho minutos más a fuego alto. Sumerge los cubos de pescado a la mitad del tiempo de cocción de la pasta y deja hasta  que cambien de color y estén firmes. Sirve la sopa caliente y disfruta de inmediato.', N'25', 1, 1, CAST(3 AS Numeric(18, 0)), 10, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), N'', N'', N'7501018310585', 14, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (108, N'Rollitos de jamón rellenos con pasta y manzana', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'3 rollitos con ½ taza de pasta', 7, N'4', N'Calienta en una olla el agua con la sal hasta que hierva. Inmediatamente agrega el Codo No.1 La Moderna® y cocina hasta que esté en su punto; revisa los tiempos de cocción marcados en el empaque. Escurre y reserva. Integra en un tazón la mitad de la pasta que cocinaste (reserva el resto para futuras preparaciones), el yogurt, la miel, la manzana y la pimienta. Arma rollitos de jamón y rellénalos con la preparación anterior. Disfruta de inmediato.', N'20', 1, 1, CAST(5 AS Numeric(18, 0)), 7, N'Fácil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), N'', N'', N'7501018310516', 15, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (109, N'Ensalada de pasta con tomates, albahaca y queso', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'Porción recomendada: 1 taza', 3, N'4', N'Calienta el agua junto con la sal en una olla alta hasta que hierva. Inmediatamente agrega la Pasta. Caracol La Moderna® y cocínala hasta que quede en su punto (suave por fuera y firme por dentro). Revisa los tiempos de cocción marcados en el empaque; escurre y reserva. Combina en un tazón la Pasta Caracol La Moderna® con los tomates, la albahaca y el queso. ¡Disfruta!', N'15', 1, 1, CAST(5 AS Numeric(18, 0)), 7, N'Fácil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), N'', N'', N'7501018310554', 16, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (110, N'Duraznos rellenos con ensalada de pasta con jamón', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'1/2 durazno relleno', 97, N'4', N'Calienta en una olla el agua con la sal hasta que hierva. Inmediatamente agrega la pasta Animales de la Selva La Moderna® Kids! y cocina hasta que estén en su punto. Escurre la pasta, pásala a un tazón y combínala junto con el jamón, el cilantro, la cebolla, el pimiento y el chile. Incorpora por separado la crema con el jugo de limón, la sal y la pimienta. Sirve la ensalada sobre las mitades de durazno y agrega encima de cada uno un poco de la mezcla de crema. Disfruta de inmediato.', N'20', 1, 1, CAST(4 AS Numeric(18, 0)), 7, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), N'', N'', N'7501018310622', 17, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (111, N'Tazones de fideo seco con salsa de ciruela y chips de platano', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de fideo seco + ¼ de taza de chips', 74, N'5', N'Para los chips. Precalienta tu horno a 160°C. Coloca las rebanadas de plátano sobre una charola, rocía con el aceite en aerosol y espolvorea con tomillo. Hornea por 15 minutos a 160°C o hasta que estén ligeramente dorados y crujientes. Para el fideo seco. Calienta una olla, vierte una cucharada de aceite y saltea ajo, cebolla y jitomates, hasta que los vegetales cambien ligeramente de color y suavicen. Pásalos a tu licuadora e integra con el caldo de pollo, las ciruelas, sal y pimienta; reserva Dora el Fideo No.2 La Moderna® en una olla con el resto del aceite caliente, vierte la salsa y cocina a fuego medio de 12 a 15 minutos, hasta que se haya evaporado el líquido. Sirve caliente, decora con queso y cilantro y acompaña con los chips de plátano.', N'25', 1, 1, CAST(5 AS Numeric(18, 0)), 7, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), N'', N'', N'7501018310035', 18, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (112, N'Sopa de fideos con calabaza y tortilla', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', N'', N'½ taza de pasta', 72, N'7', N'Para la sopa. Funde la mantequilla en una olla y dora y acitrona ligeramente el ajo y la cebolla. Incorpora las calabazas y cocínalas hasta que tomen un ligero tono dorado. Retira de la olla y reserva. Agrega el Fideo No.1 La Moderna® a la olla y tuesta ligeramente con la mantequilla que quedó en ésta. Agrega el caldo, el tomillo, el perejil, la sal y la pimienta. Cocina a fuego medio de 8 a 12 minutos o hasta que la pasta esté suave. Para servir. Sirve la sopa caliente y coloca encima una porción de calabacitas asadas; decora con perejil y las julianas de tortilla horneadas. Disfruta.', N'20', 1, 1, CAST(2 AS Numeric(18, 0)), 15, N'Medio', NULL, 1, N'', N'', N'', CAST(N'2016-07-22T00:00:00.000' AS DateTime), N'', N'', N'7501018310028', 19, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (113, N'Fideo seco con salsa de frijol y morita', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', N'', N'½ taza de fideo + 4 cucharadas de salsa', 72, N'5', N'Para La Salsa Coloca todos los ingredientes dentro del vaso de tu licuadora y procesa hasta lograr una salsa homogénea y suave; reserva. Para El Fideo Calienta el aceite en una olla a fuego medio, agrega el Fideo No.1 La Moderna® y cocina moviendo ocasionalmente, sin dejar que cambie de color. Incorpora el agua y hierve a fuego bajo de 10 a 12 minutos o hasta que el agua reduzca casi por completo y el fideo suavice. Incorpora la salsa de frijol una vez transcurrido el tiempo y continúa cocinando hasta que la pasta absorba la salsa y tenga una consistencia pesada. Sirve caliente y decora con el resto de los ingredientes. Disfruta.', N'30', 1, 1, CAST(5 AS Numeric(18, 0)), 7, N'Fácil', NULL, 1, N'', N'', N'', CAST(N'2016-07-22T00:00:00.000' AS DateTime), N'', N'', N'7501018310028', 20, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (114, N'Moño a la mexicana con mortadela', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'1 taza con 25g de mortadela', 160, N'5', N'Calienta el agua junto con la sal en una olla alta hasta que hierva. Inmediatamente agrega el Moño La Moderna y cocínalo hasta que esté en su punto (suave por fuera y firme por dentro); revisa los tiempos de cocción marcados en el empaque. Escurre, reserva y deja enfriar completamente.\n\nCalienta el aceite en un sartén y saltea la mortadela con el chile morita y el cilantro por 2 minutos o hasta que dore.\n\nCombina en un tazón la pasta cocinada con el caldillo de tomate, ;a mortadela, los vegetales y los frijoles. Revuelve para integrar los sabores y disfruta de inmediato.', N'15', 1, 1, CAST(1 AS Numeric(18, 0)), 7, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310721', 21, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (115, N'Pizza Pasta', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'1 rebanada', 162, N'7', N'Precalienta tu horno a 180 °C. Calienta el agua junto con la sal en una olla alta hasta que hierva. Inmediatamente agrega el Espiral La Moderna® y cocinalo hasta que esté en su punto (suave por fuera y firme por dentro); revisa los tiempos de cocción marcados en el empaque. Escurre la pasta una vez lista y colócala dentro de un refractario circular para horno, ligeramente engrasado con mantequilla. Baña la pasta con la salsa de tomate como si se tratara de una pizza y coloca encima el queso y, finalmente, las rebanadas de peperoni. Cubre el refractario con papel aluminio y hornea por 15 minutos. Retira el aluminio y hornea por 5 minutos más o hasta que el queso gratine. Sirve saliendo del horno y disfruta con la ensalada de tu preferencia.', N'30', 1, 1, CAST(2 AS Numeric(18, 0)), 15, N'Fácil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310714', 22, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (116, N'Ensalada de huevo con atún y pasta', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'1 taza de ensalada con ½ taza de pasta', 161, N'9', N'Para La Pasta Calienta el agua junto con la sal en una olla alta hasta que hierva. Inmediatamente agrega el Macarrón Corto La Moderna® y cocínalo hasta que esté en su punto (suave por fuera y firme por dentro); revisa los tiempos de cocción marcados en el empaque. Escurre y reserva. Para La Ensalada Funde la mantequilla en un sartén y dora ligeramente el ajo y la cebolla. Agrega la pasta cocinada y saltea por dos minutos. Aumenta el sabor con pimienta. Retira del fuego y reparte la pasta en 4 tazones individuales. Acomoda sobre la pasta chile serrano, tomate, atún desmenuzado, huevo y hojas de cilantro. Baña la ensalada con la vinagreta de tu preferencia y disfruta de inmediato.', N'25', 1, 1, CAST(5 AS Numeric(18, 0)), 7, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310097', 23, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (117, N'Refractario de pluma al horno con pollo y tomates deshidratados', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza', 164, N'7', N'Calienta el agua con la sal hasta que hierva. Inmediatamente agrega la pasta Moño La Moderna®, revisa los tiempos de cocción marcados en el empaque. Escurre y reserva. Calienta el aceite en un sartén y saltea la cebolla y el ajo hasta que suavicen. Integra la arúgula y continúa salteando hasta que suavice. Incorpora el resto de los ingredientes junto con la pasta y cocina por dos minutos. Sirve caliente y disfruta.', N'30', 1, 1, CAST(1 AS Numeric(18, 0)), 13, N'Fácil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310608', 24, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (118, N'Spaghetti con pechuga de pavo horneada, arándanos y salsa de nuez', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta con 50g de pavo', 174, N'9', N'Para la salsa\nAsa los vegetales y tuesta las nueces en un comal. Pasa al vaso de tu licuadora e integra junto con el resto de los ingredientes. Vacía la salsa en el sartén en donde sellaste las pechugas y cocina por 5 minutos a fuego bajo. Reserva.\n\nPara la pasta\nCalienta el agua junto con la sal en una olla alta hasta que hierva. Inmediatamente agrega el Spaghetti La Moderna® y cocina hasta que esté en su punto. Revisa los tiempos de cocción marcados en el empaque. Escurre la pasta y pásala al sartén con la salsa, revuelve bien y deja cocinar por un minuto a fuego medio. Sirve la pasta con una porción de pechuga de pavo, arándanos y decora con perejil picado. Disfruta caliente.', N'20', 1, 1, CAST(3 AS Numeric(18, 0)), 7, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310103', 25, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (119, N'Moño con arúgula, nuez y frijol negro', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta con 1 taza de verduras', 160, N'6', N'Calienta el agua con la sal hasta que hierva. Inmediatamente agrega la pasta Moño La Moderna® suave por fuera y firme por dentro.\nEscurre y reserva.\n\nCalienta el aceite en un sartén y saltea la cebolla y el ajo hasta que suavicen. Integra la arúgula y continúa salteando hasta que suavice.\n\nIncorpora el resto de los ingredientes junto con la pasta y cocina por dos minutos.\n\nSirve caliente y disfruta.', N'20', 1, 1, CAST(1 AS Numeric(18, 0)), 7, N'Fácil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310721', 26, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (120, N'Fettuccine cremoso con 2 quesos', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta + 30g de jamón serrano', 169, N'9', N'Calienta en una olla el agua con la sal hasta que hierva. Agrega el Fettuccine La Moderna® y cocina hasta que esté en su punto. Escurre y reserva.\n\nLicúa la crema con los quesos y lleva a un sartén a fuego medio. Agrega los ajos, la cebolla y la ralladura de limón. Rectifica su sabor con sal y pimienta.\n\nCocina por 6 minutos o hasta que espese ligeramente. Agrega la pasta a la salsa y cocina por dos minutos más. Sirve caliente y espolvorea con almendras y perejil fresco al momento.\n\n¡Disfruta!', N'30', 1, 1, CAST(1 AS Numeric(18, 0)), 13, N'Fácil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310141', 27, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (121, N'Spaghetti con camarones albahaca y tomate', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta con 4 camarones', 89, N'8', N'Calienta en una olla el agua con la sal hasta que hierva. Inmediatamente agrega el Spaghetti La Moderna® y cocina hasta que esté en su punto. Escurre y reserva.\nCalienta el aceite en un sartén y suaviza los ajos sin dejar que cambien de color. \nIncorpora los camarones y cocínalos hasta que tomen un tono vivo.\nEspolvoréalos con sal, pimienta, perejil y ralladura de limón.\nCocina por un minuto más. Añade los líquidos al sartén, baja el fuego y cocina por tres minutos. Integra la pasta al sartén y calienta por un minuto.\nAgrega el resto de los ingredientes, retira del fuego.\nMezcla y sirve de inmediato.', N'25', 1, 1, CAST(5 AS Numeric(18, 0)), 22, N'Fácil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018319007', 28, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (122, N'Moño con espárragos y camarones en salsa de queso', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta con 4 camarones + 4 cucharadas de salsa', 160, N'7', N'Para la salsa\nLleva al fuego todos los ingredientes, cocina por 5 minutos sin dejar de mover o hasta obtener una salsa tersa; reserva.\n\nPara la pasta\nCalienta en una olla el agua con la sal hasta que hierva. Inmediatamente agrega los Moños La Moderna® y cocina hasta que esté en su punto.\nEscurre la pasta y pásala al sartén con la salsa; revuelve bien y reserva caliente.\nCalienta el aceite en un sartén y suaviza el ajo. Incorpora los espárragos y saltea por un minuto. Incorpora los camarones y espolvorea con páprika, sal y pimienta; cocínalos hasta que estén firmes y tomen un tono naranja vivo.\nIncorpora la mezcla de camarones y espárragos al sartén con la pasta, tira las avellanas y mezcla bien. Sirve de inmediato y disfruta.', N'20', 1, 1, CAST(5 AS Numeric(18, 0)), 23, N'Fácil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310721', 29, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (123, N'Refractario de fettuccine con salsa de pimiento', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta', 169, N'9', N'Para la salsa\nLicúa todos los ingredientes de la salsa hasta integrar y obtener una salsa lisa.\nVacía la salsa en un sartén y calienta a fuego lento hasta espesar ligeramente.\n \nPara la pasta\nPrecalienta tu horno a 180ºC.\nCalienta el agua junto con la sal en una olla alta hasta que hierva.\nInmediatamente agrega el Fettucine La Moderna®, revisa los tiempos de cocción marcados en el empaque.\nEscurre la pasta y pásala a un refractario para horno, báñala con la salsa, agrega las espinacas y revuelve bien. Coloca en la superficie del refractario, el tocino, las espinacas y las julianas de ajo.\nHornea el refractario de 6 a 8 minutos o hasta que el tocino y el ajo tomen un tono dorado.\nSirve saliendo del horno y disfruta.', N'30', 1, 1, CAST(1 AS Numeric(18, 0)), 8, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310141', 30, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (124, N'Platón de pasta con coles de Bruselas y tocino', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta con ¾ taza de coles de bruselas', 86, N'7', N'Calienta en una olla el agua con la sal hasta que hierva. Inmediatamente agrega el Codo No.4 La Moderna® y cocina hasta que esté en su punto. Escurre y reserva. Fríe el tocino en un sartén caliente, añade las coles y saltea por un par de minutos o hasta que estén ligeramente doradas. Incorpora la pasta, perejil, páprika y pimienta. Mezcla perfectamente y sirve en un platón. Disfruta.', N'20', 0, 0, CAST(0 AS Numeric(18, 0)), 17, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018319014', 31, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (125, N'Fettuccine con salsa de nogada, poblano y granada', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta con ½ taza de salsa', 169, N'11', N'Para La Salsa Coloca todos los ingredientes en tu licuadora o procesador de alimentos y mezcla hasta lograr una salsa de textura suave y homogénea; reserva. Para La Pasta Calienta el agua junto con la sal en una olla alta hasta que hierva. Inmediatamente agrega el Fettuccine La Moderna® y cocina hasta que esté en su punto (suave por fuera y firme por dentro; revisa los tiempos de cocción marcados en el empaque. Escurre y reserva. Calienta el aceite en un sartén y suaviza el ajo y la cebolla hasta que suelten todo su sabor y la cebolla quede transparente. Incorpora las rajas de poblano y cocina por un par de minutos, moviendo ocasionalmente hasta que el chile suavice. Incorpora la pasta al sartén y revuelve para incorporar con los demás ingredientes. El resto de la preparación será al gusto y al más puro estilo de los tradicionales chiles en nogada. Puedes agregar la salsa al sartén y calentarla junto con la pasta durante un minuto para una versión caliente o pasar la pasta a un tazón, esperar a que enfríe ligeramente y mezclarla con la salsa a temperatura ambiente para una versión fría. Termina decorando la pasta con granada y perejil picado y disfruta de inmediato.', N'20', 0, 0, CAST(5 AS Numeric(18, 0)), 8, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310141', 32, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (126, N'Ensalada de pasta con uvas y aceitunas', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de ensalada + ½ taza de pasta + 1 cucharada de vinagreta', 161, N'7', N'Para la vinagreta Mezcla todos los ingredientes dentro de un frasco de vidrio con tapa y agita vigorosamente hasta formar una emulsión. Reserva. Para la pasta Calienta en una olla el agua con la sal hasta que hierva. Inmediatamente agrega los Macarrones La Moderna® y cocina hasta que estén en su punto. Escurre y deja enfriar por completo. Combina en un tazón la pasta junto con el resto de los ingredientes de la ensalada, añade la vinagreta y revuelve para incorporar los sabores. Sirve y disfruta.', N'20', 0, 0, CAST(0 AS Numeric(18, 0)), 7, N'Fácil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310097', 33, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (127, N'Spaghetti con calabacitas rostizadas y tomates', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta preparada con 70g de pollo', 174, N'10', N'Calienta en una olla el agua con la sal hasta que hierva. Inmediatamente agrega el Spaghetti La Moderna® y cocina hasta que quede suave por fuera y firme por dentro. Escurre y reserva. Calienta el aceite en un sartén. Coloca algunas láminas de calabaza dentro del sartén y cocínalas por ambos lados hasta que tomen un tono dorado y una textura ligeramente cru- jiente; aumenta su sabor con sal y pimienta. Retíralas conforme estén listas y reserva. Funde la mantequilla en el mismo sartén donde cocinaste las calabazas y saltea los ajos y los jitomates por un par de minutos, sin permitir que estos suavicen. Integra las hojas de albahaca, la pasta y las calabazas cocinadas. Revuelve bien y calienta por un minuto. Sirve caliente y disfruta con queso parmesano rallado.', N'25', 0, 0, CAST(0 AS Numeric(18, 0)), 17, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310103', 34, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (128, N'Spaghetti con jamaica chile guajillo y perejil', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta', 174, N'9', N'Calienta el agua junto con la sal en una olla alta hasta que hierva. Inmediatamente agrega Spaghetti La Moderna® y cocina hasta que esté en su punto (suave por fuera y firme por dentro); revisa los tiempos de cocción marcados en el empaque. Escurre y reserva. Funde la mantequilla junto con el aceite en un sartén y saltea ajo, cebolla, flores de jamaica y chile hasta que la cebolla y las flores suavicen. Aumenta el sabor con orégano, sal y pimienta, e incorpora el caldo de pollo. Cocina a fuego medio hasta que el líquido reduzca y espese ligeramente; por cuatro minutos, aproximadamente. Integra al sartén la pasta, revuelve para integrar sabores y agrega las hojas de perejil y la ralladura. Cocina por un minuto más y sirve de inmediato. Disfruta.', N'20', 0, 0, CAST(5 AS Numeric(18, 0)), 6, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310103', 35, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (129, N'Sartén de pasta con carne al gratín', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza', 172, N'10', N'Precalienta tu horno a 200°C. Hierve el agua con la sal en una olla alta, agrega el Macarrón Largo La Moderna® y cocina un minuto antes de lo que marca el empaque. Escurre y reserva. Calienta el aceite en un sartén grande para horno y suda el ajo junto con la cebolla hasta que ambos comiencen a cambiar de color. Agrega la carne y cocina a fuego medio hasta que cambie completamente de color. Licúa los jitomates con el puré, chile, pimienta, cilantro y el caldo de pollo hasta obtener una salsa homogénea. Vierte la salsa al sartén y cocina por un minuto. Incorpora la pasta y revuelve bien; aumenta el sabor con sal, pimienta y orégano. Cubre la superficie con los quesos, lleva el sartén al horno hasta gratinar. Sirve una vez listo y disfruta.', N'30', 0, 0, CAST(0 AS Numeric(18, 0)), 17, N'Difícil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310110', 36, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (130, N'Spaghetti con hongos y elotes al limón', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta preparada', 174, N'9', N'Calienta el agua junto con la sal en una olla alta hasta que hierva. Inmediatamente agrega el Spaghetti La Moderna® y revisa los tiempos de cocción marcados en el empaque. Escurre y reserva. Calienta el aceite en un sartén y funde la mantequilla, agrega los ajos junto con las láminas de hongos y los ejotes. Saltea constantemente hasta que los hongos suavicen. Aumenta el sabor con epazote, sal y pimienta. Añade el jugo de limón y el azúcar. Revuelve bien y cocina hasta que el líquido reduzca. Integra la pasta, revuelve bien y cocina por un minuto para incorporar los sabores. Sirve caliente y disfruta.', N'25', 0, 0, CAST(5 AS Numeric(18, 0)), 6, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310103', 37, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (131, N'Pasta picante con tomate y camarones', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta con 4 camarones', 168, N'13', N'Para la salsa Calienta el aceite en un sartén y suda el ajo con la cebolla hasta que empiecen a cambiar de color. Agrega el jitomate y cocina a fuego alto hasta suavizar; mueve constantemente para que no se peguen. Integra puré de tomate, chile, sal, orégano, pimienta y agua Cocina por un par de minutos o hasta que el agua reduzca. Retira del fuego e integra las hojas de albahaca. Reserva. Para la pasta En una olla alta hierve el agua con la sal. Inmediatamente agrega la pasta Tornillo La Moderna® y cocina hasta un minuto antes de lo indicado en el empaque. Escurre y reserva. Calienta el aceite en un sartén, agrega el ajo y el jengibre; mueve constantemente hasta que suavicen. Incorpora los camarones y cocínalos hasta que tomen un tono naranja vivo y estén firmes. Incorpora la pasta al sartén con la salsa y calienta por dos minutos. Sirve de inmediato y disfruta con queso parmesano.', N'25', 0, 0, CAST(3 AS Numeric(18, 0)), 7, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310592', 38, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (132, N'Spaghetti con almejas y salsa de cilantro', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'Porción recomendada: ½ taza de pasta preparada', 174, N'11', N'Para la salsa Licúa todos los ingredientes hasta incorporar y obtener una salsa suave y homogénea; reserva. Para la pasta Calienta el agua junto con la sal en una olla alta hasta que hierva. Inmediatamente agrega el Spaghetti La Moderna® y cocínalo hasta que quede en su punto (suave por fuera y firme por dentro). Revisa los tiempos de cocción marcados en el empaque; escurre y reserva. Funde la mantequilla en un sartén y cocina el ajo, la cebolla y el té limón, sin dejar de mover y hasta suavizar. Incorpora las almejas y el caldo. Tapa el sartén y cocina a fuego medio hasta que las almejas abran. Añade la pasta al sartén, incorpora la salsa y revuelve para integrar todos los sabores. Calienta por 2 minutos más a fuego medio. Sirve de inmediato y decora con cilantro picado. ¡Disfruta!', N'20', 0, 0, CAST(0 AS Numeric(18, 0)), 9, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310103', 39, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (133, N'Tornillos con atún, chícharos y limón', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'Porción recomendada: ½ taza de pasta + ½ taza de chícharos + 50g de atún', 168, N'10', N'Calienta el agua junto con la sal en una olla alta hasta que hierva. Inmediatamente agrega Tornillos La Moderna® y cocínalos hasta que queden en su punto (suaves por fuera y firmes por dentro). Revisa los tiempos de cocción marcados en el empaque. Agrega los chícharos a un minuto de terminar la cocción de la pasta para que se descongelen y mantengan un tono verde vivo. Escurre y reserva. Combina en un tazón la crema con el jugo, la ralladura, el chile y la pimienta. Regresa la pasta a la olla en donde la cocinaste, e incorpora la mezcla de crema. Revuelve y permite que se integre con el calor de la pasta. Incorpora el atún y revuelve una vez más. Sirve de inmediato y disfruta.', N'15', 0, 0, CAST(5 AS Numeric(18, 0)), 7, N'Fácil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310592', 40, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (134, N'Spaghetti con láminas de calabaza y chile de árbol', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta', 174, N'10', N'Pon a hervir el agua con sal y agrega el Spaghetti La Moderna® cocina casi hasta que esté en su punto según el tiempo de cocción que diga el empaque. Escurre y reserva. Calienta el aceite en un sartén y dora ligeramente el ajo. Incorpora los chiles y la calabaza. Saltea por un par de minutos hasta que doren ligeramente y aumenta su sabor con sal y pimienta. Retira del sartén y reserva. Funde la mantequilla en el mismo sartén y agrega la crema, agua y queso. Cocina a fuego bajo y mueve constantemente para integrar la salsa. Agrega la pasta con las calabazas al sartén, revuelve bien y calientapor un par de minutos. Sirve de inmediato y decora con orégano.', N'25', 0, 0, CAST(0 AS Numeric(18, 0)), 22, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310103', 41, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (135, N'Spaghetti con aceitunas con nueces caramelizadas y queso panela', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza', 174, N'7', N'Hierve el agua con sal en una olla alta. Inmediatamente agrega el Spaghetti La Moderna® y cocina hasta que esté suave por fuera y firme por dentro. Escurre y reserva. Vierte el aceite en un sartén caliente y dora el ajo. Agrega la pasta y las aceitunas; mezcla y aumenta el sabor con pimienta. Sirve la pasta con las nueces caramelizadas y el queso parmesano rallado. Disfruta.', N'14', 0, 0, CAST(4 AS Numeric(18, 0)), 10, N'Fácil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310103', 42, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (136, N'Pluma con cubos de pavo y salsa de espinacas', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'Porción recomendada: ½ taza de pasta con 50g de pavo', 164, N'10', N'Para la salsa Coloca la mitad de las espinacas y el resto de los ingredientes dentro de tu procesador de alimentos y procesa hasta lograr una salsa suave y homogénea. De ser necesario agrega un par de cucharadas de agua. Para la pasta Calienta el agua junto con la sal en una olla alta hasta que hierva. Inmediatamente agrega la Pluma La oderna® y cocínala hasta que quede en su punto (suave por fuera y firme por dentro). Revisa los tiempos de cocción marcados en el empaque. Escurre la pasta una vez lista y regrésala a la misma olla. Incorpora la salsa junto con las espinacas restantes y mezcla hasta que las hojas suavicen por el calor remanente. Integra los cubos de pavo, las almendras, la sal y la pimienta. Vuelve a mezclar para incorporar los sabores. Sirve de inmediato y espolvorea con queso parmesano. ¡Disfruta!', N'35', 0, 0, CAST(5 AS Numeric(18, 0)), 7, N'Difícil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310608', 43, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (137, N'Fettuccine con pollo y nueces', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta preparada con 70g de pollo', 169, N'12', N'Para la salsa Calienta el aceite de canola en un sartén y suda el ajo y la cebolla hasta que esta última tome un tono traslúcido. Incorpora los tallos de cilantro junto con los chiles y saltea por un minuto. Añade el agua y la crema al sartén, revuelve bien para incorporar con el resto de los ingredientes y cocina hasta hervir. Pasa esta preparación al vaso de tu licuadora y procesa hasta logar una salsa suave y tersa. Regresa la salsa al sartén y rectifica el sabor con sal, pimienta y nuez moscada. Reserva. Para el pollo Combina en un tazón el aceite junto con el romero y barniza las pechugas de pollo con esta preparación. Cocina el pollo en tu parrilla bien caliente y deja hasta que las líneas se marquen y las pechugas estén bien cocinadas al centro; aproximadamente por cinco minutos por lado. Retira de la parrilla una vez listas, deja reposar un minuto y corta en rebanadas sesgadas. Para la pasta Calienta el agua junto con la sal en una olla alta hasta que hierva. Inmediatamente agrega el Fettuccine La Moderna® y cocina hasta que esté en su punto (suave por fuera y firme por dentro); revisa los tiempos de cocción marcados en el empaque. Escurre la pasta y pásala al sartén de la salsa junto con las nueces. Revuelve y calienta a fuego bajo por 30 segundos. Sirve la pasta caliente junto con el pollo a la parrilla.', N'25', 0, 0, CAST(5 AS Numeric(18, 0)), 13, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310141', 44, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (138, N'Pasta primavera con pollo', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta con 70g de pollo', 160, N'9', N'Para el pollo Combina en un tazón el aceite junto con té limón, los echalotes y pimienta; barniza las pechugas de pollo con esta preparación y cocínalo en un sartén bien caliente hasta que tome un tono dorado y quede bien cocinado al centro; unos por cinco minutos de cada lado. Retira del sartén una vez listas, deja reposar un minuto y corta en rebanadas sesgadas; reserva caliente. Para la pasta Pon a hervir el agua y la sal, inmediatamente agrega el Moño La Moderna® y cocina justo hasta que esté casi en su punto. Escurre y reserva. Funde la mantequilla en un sartén y saltea la cebolla, pimientos, calabacita y zanahoria por un par de minutos hasta suavizar ligeramente. Aumenta el sabor con sal, pimienta, orégano y albahaca. Incorpora la pasta y revuelve para integrar todos los sabores; deja por un minuto más. Sirve la pasta caliente y disfruta con una porción de pollo.', N'20', 0, 0, CAST(4 AS Numeric(18, 0)), 10, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310721', 45, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (139, N'Ensalada de pluma con queso y espinaca', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta preparada con 70g de pollo', 164, N'7', N'Para la ensalada Calienta el agua junto con la sal en una olla alta hasta que hierva. Inmediatamente agrega la Pluma La Moderna® y cocina hasta que esté en su punto (suave por fuera y firme por dentro). Escurre y reserva. Tuesta en un sartén las semillas y pásalas a un tazón, agrega el resto de los ingredientes para la ensalada junto con la pasta y mezcla para incorporar. Para la vinagreta Coloca todos los ingredientes de la vinagreta dentro de un frasco de vidrio con tapa y agita vigorosamente hasta formar una emulsión. Baña la ensalada con la vinagreta y revuelve bien. Sirve de inmediato y disfruta.', N'25', 0, 0, CAST(4 AS Numeric(18, 0)), 7, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310608', 46, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (140, N'Pluma en salsa de chile poblano', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta + 4 cucharadas de salsa', 164, N'9', N'Para la salsa Calienta el aceite de canola en un sartén, dora ligeramente las láminas de ajo y acitrona la cebolla. Añade el chile y mezcla con todos los ingredientes; deja por 2 minutos y aumenta el sabor con sal y pimienta. Pasa a tu licuadora e integra con la crema y el caldo. Regresa al sartén y mantén la salsa caliente. Para la pasta Calienta el agua junto con la sal en una olla alta hasta que hierva. Inmediatamente agrega la pasta Pluma La Moderna® y cocina hasta que esté en su punto (suave por fuera y firme por dentro); revisa los tiempos de cocción marcados en el empaque. Escurre y reserva. Calienta el aceite en un sartén y saltea la cebolla junto con los granos de elote por un minuto. Incorpora la pasta y la salsa. Revuelve bien y deja calentar por un minuto más. Sirve de inmediato y disfruta.', N'20', 0, 0, CAST(5 AS Numeric(18, 0)), 7, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310608', 47, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (141, N'Tornillos con champiñones y pollo empanizado', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta con 70g de pollo + ½ taza de salsa', 168, N'13', N'Para la salsa Funde la mantequilla en un sartén y cocina cebolla, ajo y chile de árbol, hasta que la cebolla tome un tono traslúcido. Agrega los champiñones y el tomillo; aumenta el sabor con sal y pimienta y cocina hasta evaporar el líquido de los hongos. Vierte la crema y el agua, mezcla perfectamente y hierve a fuego bajo por cinco minutos. Para la pasta Precalienta tu horno a 180°C. Calienta el agua con la sal. Al hervir, agrega el Tornillo La Moderna® y cocina hasta un minuto antes del tiempo marcado en el empaque. Escurre y Reserva. Espolvorea la pechuga de pollo con sal y pimienta, pásala por harina para cubrirla ligeramente y empanízala pasándola primero por huevo y finalmente por las hojuelas de maíz. Calienta el aceite en un sartén y dora la pechuga por ambos lados. Pasa el pollo a una charola y termina la cocción dentro del horno, de 4 a 6 minutos aproximadamente. Retira del horno y corta en fajitas. Pasa la pasta al sartén de la salsa y calienta por un minuto a fuego alto. Sirve de inmediato y agrega una porción de las fajitas; disfruta con eneldo picado.', N'25', 0, 0, CAST(5 AS Numeric(18, 0)), 7, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310592', 48, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (142, N'Moño con flor de calabaza y huitlacoche', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta con ½ taza de verduras', 160, N'11', N'Calienta en una olla el agua con la sal hasta que hierva. Inmediatamente agrega el Moño La Moderna® y cocina hasta que esté en su punto (suave por fuera y firme por dentro); revisa los tiempos de cocción marcados en el empaque. Escurre y reserva. Calienta el aceite en un sartén y saltea los ajos, ½ cebolla fileteada y el huitlacoche, hasta que la cebolla tome un tono transparente y el hongo haya suavizado. Aumenta el sabor con epazote, sal y pimienta, y cocina por un minuto más. Pasa esta preparación al vaso de tu licuadora, agrega el agua y la crema para batir y licúa hasta obtener una salsa suave y homogénea. Calienta el aceite de oliva en el mismo sartén donde cocinaste el huitlacoche y saltea el resto de la cebolla con las flores de calabaza únicamente hasta suavizar. Retira y reserva. Vacía la salsa de huitlacoche en el mismo sartén, calienta e integra la pasta y cocina por un minuto más. Retira del fuego e integra el salteado de flor de calabaza. Sirve de inmediato y decora con perejil picado. Disfruta.', N'20', 0, 0, CAST(5 AS Numeric(18, 0)), 15, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310721', 49, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (143, N'Fettuccine con brócoli y grano de elote amarillo', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta + ½ taza de elote', 169, N'11', N'Calienta en una olla el agua con la sal hasta que hierva. Inmediatamente agrega el Fettuccine La Moderna® revisa los tiempos de cocción marcados en el empaque. Escurre y reserva. Calienta un sartén, vierte el aceite de ajonjolí y saltea los vegetales por un par de minutos hasta que suavicen ligeramente y tomen un color vivo. Incorpora la pasta y el jugo de naranja. Cocina hasta que el líquido se evapore; añade el ajonjolí y mezcla. Sirve caliente y disfruta.', N'15', 0, 0, CAST(5 AS Numeric(18, 0)), 8, N'Fácil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310141', 50, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (144, N'Fettuccine con salsa blanca y salmón', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta con 140g de salmón + 1 taza de verduras', 169, N'11', N'Para la salsa Funde la mantequilla en una olla, agrega y cocina la cebolla hasta que esté traslúcida. Añade la harina y mueve constantemente hasta que se dore ligeramente; incorpora la leche y mezcla con un batidor de globo para evitar grumos. Por último, agrega las especias y cocina sin dejar de mover por 10 minutos o hasta que espese ligeramente. Para la pasta Calienta el agua con la sal. Cuando hierva, agrega el Fettuccine La Moderna® y cocina hasta un minuto antes del tiempo marcado en el empaque. Escurre y reserva. Aumenta el sabor del salmón con una cucharada de aceite de oliva, eneldo y pimienta. Funde mantequilla en un sartén y cocina el salmón, primero por el lado de la piel hasta que esté crujiente; enseguida dale vuelta y cocina por un par de minutos más o hasta obtener el término deseado. Retira del sartén y reserva caliente.', N'25', 0, 0, CAST(5 AS Numeric(18, 0)), 7, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310141', 51, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (145, N'Codos con queso, brócoli y salchicha', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'1 rebanada (½ taza de pasta + ½ taza de brócoli + ½ salchicha)', 152, N'8', N'Para las croquetas Incorpora y licúa todos los ingredientes hasta obtener una salsa tersa y homogénea; reserva. Para el dip de mango Precalienta tu horno a 180°C. Calienta en una olla el agua con sal hasta que hierva. Inmediatamente agrega la pasta Codo No.4 La Moderna® , revisa los tiempos de cocción marcados en el empaque. Escurre y colócala en un refractario, previamente engrasado. Agrega el brócoli, las salchichas y mezcla para integrar. Baña la pasta con la salsa que reservaste y revuelve ligeramente. Hornea por 15 minutos o hasta que los líquidos reduzcan. Retira del horno, espolvorea con perejil y sirve de inmediato, o bien, deja enfriar y refrigera para consumir en otro momento.', N'30', 0, 0, CAST(4 AS Numeric(18, 0)), 22, N'Difícil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310615', 52, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (146, N'Fettuccine con chorizo y espinacas', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'½ taza de pasta con 50g de chorizo', 169, N'7', N'Calienta en una olla el agua con la sal hasta que hierva. Inmediatamente agrega el Fettuccine La Moderna® y cocina según el tiempo de cocción marcado en el empaque. Escurre y reserva. Calienta el aceite en un sartén y saltea la cebolla y el chile. Incorpora el chorizo, cocina hasta que esté ligeramente dorado; añade el vino y espera a que el líquido se evapore. Agrega la pasta, las espinacas y la pimienta; mezcla y retira del fuego. Sirve de inmediato y disfruta.', N'20', 0, 0, CAST(5 AS Numeric(18, 0)), 7, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310141', 53, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (147, N'Pluma con tomate cherry, alcachofa, pimientos y queso panela', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'1 taza de ensalada con ½ taza de pasta + 1 cda. de vinagreta', 164, N'7', N'Para la ensalada Calienta agua junto con la sal en una olla hasta que hierva. Agrega la pasta Pluma La Moderna® y cocina hasta que esté suave por fuera y firme por dentro. Escurre y reserva. Corta los corazones de alcachofa en cuartos y colócalos en un tazón con el resto de los ingredientes y la pasta ya fría. Para la vinagreta Licúa sus ingredientes hasta obtener una emulsión. Baña la ensalada con la vinagreta, revuelve bien y sirve de inmediato. Disfruta.', N'20', 0, 0, CAST(1 AS Numeric(18, 0)), 7, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310608', 54, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (148, N'Tornillo con atún ', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', N'thumb_400x290621472147410.jpg', N'La puedes usar como entrada o como guarnición para acompañar una milanesa de res o pollo.', 168, NULL, N'1.- Hervir el Rotini DeLuigi según instrucciones del empaque, con 1 cucharada de sal.\n\n2.- Ya hervida la pasta, poner a escurrir \n\n3.- En una cacerola poner 1 cucharada de aceite de olivo y acitronar 1/2 cebolla finamente picada. \n\n4.- Agregar 1 Lata de atún, previamente escurrido y mezclar con la cebolla.\n\n5.- Agregar 200 ml. de crema acida y mezclar junto con el atún y la cebolla. \n\n6.- Agregar el Rotini DeLuigi y mezclar con los ingredientes anteriores. \n\n7.-Servir al momento. Buen provecho! ', N'25', 0, 0, CAST(5 AS Numeric(18, 0)), 93, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310592', 62, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (149, N'Spaghetti piña chipotle ', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', N'thumb_400x290631472251135.jpg', N'Es una receta que tiene sabores frescos con la piña y los champiñones pero que como es caliente te da esa sensación de calor de hogar, yo la hago y es muy rica hasta para cenar. y toma máximo 30 minutos. ', 176, NULL, N'1. Cuece la pasta en abundante agua hirviendo con sal y un trocito de cebolla hasta que esté al dente.\n2. En una olla calienta el aceite y fríe la cebolla hasta que esté transparente.\n3. Agrega el ajo y fríe por unos minutos, añade los champiñones y cocina por 3 minutos más. Agrega, crema y los chiles chipotles y revuelve todo bien. Sazona con sal. \n4. Baja el fuego, agrega la piña y espolvorea el cuarto de taza del queso parmesano. Revuelve hasta que se derrita. \n6. Sirve la pasta con la salsa de chipotle bien caliente.\nDisfrutenla tanto como yo!.', N'30', 0, 0, CAST(5 AS Numeric(18, 0)), 57, N'Dificil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310943', 63, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (150, N'Fideo Seco ', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', N'thumb_400x290951490471234.jpg', N'Para recibir a los nietos', 128, NULL, N'Se viven los jitomates en agua hirviendo ya que reventaron se apagan y se muelen con la cebolla y el ajo.\nSe muelen los frijoles \nEn un sartén se pone el aceite se fríe el fideo ya que está transparente se le vacía el jitomate molido y se sazona con sal y pimienta y se deja cocer tapado y se mueve de vez en vez ya que está a medio cocer se le agregan los frijoles y se deja hasta este cocido \nSe vacía en un refractario se le agrega queso y crema y se mete al horno a qué se derrita el queso\nY listo para comer', N'30', 0, 0, CAST(0 AS Numeric(18, 0)), 356, N'Difícil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), N'http://app.nutriendotuvida.com/images/recetas/feed/thumb_240x230', N'http://app.nutriendotuvida.com/images/recetas/feed/thumb_240x230', N'7501018319168', 95, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (151, N'Putonesca al tomate \n', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', N'thumb_400x290961490910400.jpg', N'pasta de muchos ingredientes ', 86, NULL, N'primero se pone a hervir el agua con las hojas de laurel y una cuch de aceite de oliva \nen otro sarten se pone los champiñones con las acelgas el broccoli \ndespues el pollo \nse mezclan todos los ingredientes y se vierte la salsa de tomate.\n\nse sirve en un plato con queso parmesano rayado y hojas de hierbabuena', N'30', 0, 0, CAST(1 AS Numeric(18, 0)), 700, N'Dificil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018319014', 96, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (152, N'spaghetti a la boloñesa', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', N'thumb_400x290971491843107.jpg', N'primero pon la pasta y luego la carne', 176, NULL, N'Poner tu pasta a cocer en lo que esta coce tu carne y poner sal, vaciarle la salsa de tomate la albacar cocinar.\nponer en un plato la pasta cocida y encima poner la carne . acompañar con pan 30', N'30', 0, 0, CAST(4 AS Numeric(18, 0)), 1400, N'Dificil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310943', 97, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (153, N'Espagueti con camarones al ajo y brócoli\n\n', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', N'thumb_400x290981491964181.jpg', N'fácil y deliciosa', 174, NULL, N'Cuece el espagueti en una cacerola grande según se indica en el paquete, pero sin sal. Agrega el brócoli al agua 2 min. antes de terminada la cocción.\n\nEntretanto, calienta el aderezo en una sartén grande a fuego medio-alto. Agrega los camarones con el ajo; cocínalos y revuélvelos de 3 a 4 min. o hasta que adquieran un tono rosado.\n\nEscurre la mezcla de espagueti; ponlos de vuelta en la cacerola. Agrega la mezcla de camarones y el queso; revuelve ligeramente.\n\n', N'20', 0, 0, CAST(0 AS Numeric(18, 0)), 1400, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310103', 98, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (154, N'Spaghetti sencillo', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', N'thumb_400x2901001520018446.jpg', N'hola\n', 174, NULL, N'hola\n\n\n\n\n\nz\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n', N'8', 0, 0, CAST(0 AS Numeric(18, 0)), 22, N'Fácil', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), N'https://app.nutriendotuvida.com/images/recetas/feed/thumb_240x230', N'https://app.nutriendotuvida.com/images/recetas/feed/thumb_240x230', N'7501018310103', 100, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
INSERT [dbo].[Recipe] ([Id], [Name], [Image], [Image2], [Image3], [Tumb], [Introduction], [ProductId], [Steeps], [Preparation], [Time], [Publish], [Approved], [Score], [PerfilId], [Level], [AdminId], [AppUserId], [Ip], [Latitude], [Longitude], [Date], [LongURL], [ShortURL], [Qr], [oldid], [Video], [Likes]) VALUES (155, N'Pasta Butroni', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image1.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image2.png', N'http://10.0.0.8:45455/Public/Recipe/e71613da-a774-4ca5-9e13-e4e37cda1f24/Image3.png', NULL, N'de requiere de 25minutos', 10, NULL, N'se prepara muy fácil', N'25', 0, 0, CAST(0 AS Numeric(18, 0)), 1622, N'Medio', NULL, 1, NULL, NULL, NULL, CAST(N'2016-07-22T00:00:00.000' AS DateTime), NULL, NULL, N'7501018310219', 101, N'https://www.youtube.com/embed/bVdfj7HXuXE', NULL)
GO
SET IDENTITY_INSERT [dbo].[Recipe] OFF
GO
SET IDENTITY_INSERT [dbo].[RecipeIngredient] ON 
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (1, 95, N'5', 4, 3, 1, N'Cucharadas', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (2, 95, N'1', 3, 367, 2, N'Cucharada', N'Vinagre blanco')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (3, 95, N'1', 3, 209, 3, N'Cucharada', N'Jugo de limon')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (4, 95, N'1', 3, 316, 4, N'Cucharada', N'Rayadura de cascara de limon')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (5, 95, N'1', 3, 73, 5, N'Cucharada', N'Chile piquin molido')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (6, 95, N'1', 22, 321, 6, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (7, 95, N'2', 15, 10, 7, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (8, 95, N'1/2', 3, 319, 8, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (9, 95, N'200', 17, 86, 9, N'Paquete', N'Codo con fibra la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (10, 95, N'2', 13, 26, 10, N'Latas', N'Atún ')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (11, 95, N'4', 30, 25, 11, N'Tazas', N'Arúgula baby, lavada y desinfectada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (12, 96, N'2', 4, 4, 12, N'Cucharadas', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (13, 96, N'2', 20, 122, 13, N'Piezas', N'Dientes de ajo, finamente picados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (14, 96, N'1', 19, 65, 14, N'Pieza', N'Chile de árbol seco')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (15, 96, N'2', 4, 254, 15, N'Cucharadas', N'Orégano fresco, picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (16, 96, N'1/2', 19, 55, 16, N'Pieza', N'Cebolla, finamente picada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (17, 96, N'1', 12, 347, 17, N'Lata', N'Tomate (jitomate), en trozos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (18, 96, N'1', 22, 321, 18, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (19, 96, N'1/2', 3, 30, 19, N'Cucharada', N'Azúcar mascabado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (20, 96, N'2', 15, 10, 20, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (21, 96, N'1/2', 3, 319, 21, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (22, 96, N'200', 8, 289, 22, N'Gr', N'Pluma con fibra la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (23, 96, N'2', 20, 324, 23, N'Piezas', N'Salchichas de cerdo')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (24, 96, N'2', 4, 313, 24, N'Cucharadas', N'Queso parmesano, rallado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (25, 96, N'4', 4, 275, 25, N'Cucharadas', N'Perejil, lavado, desinfectado y picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (26, 97, N'2', 15, 10, 26, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (27, 97, N'1/2', 3, 319, 27, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (28, 97, N'200', 17, 89, 28, N'Paquete', N'Codo no.3 la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (29, 97, N'2', 4, 338, 29, N'Cucharadas', N'Tallos de cilantro lavados, desinfectados y picados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (30, 97, N'1', 22, 276, 30, N'Pizca', N'Pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (31, 97, N'2', 26, 287, 31, N'Rebanadas', N'Piña miel')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (32, 97, N'2', 26, 196, 32, N'Rebanadas', N'Jamon de pechuga de pavo')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (33, 97, N'4', 28, 23, 33, N'Tallos', N'Apio en cubos chicos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (34, 97, N'3/4', 29, 373, 34, N'Taza', N'Yogurt natural bajo en grasa')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (35, 97, N'2', 20, 218, 35, N'Piezas', N'Limones (el jugo)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (36, 97, N'1', 3, 4, 36, N'Cucharada', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (37, 97, N'1', 22, 321, 37, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (38, 97, N'2', 4, 16, 38, N'Cucharadas', N'Albahaca')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (39, 98, N'2', 15, 10, 39, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (40, 98, N'1/2', 3, 319, 40, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (41, 98, N'200', 8, 290, 41, N'Gr', N'Pluma la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (42, 98, N'2', 4, 4, 42, N'Cucharadas', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (43, 98, N'1', 19, 127, 43, N'Pieza', N'Echalote, finamente picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (44, 98, N'1', 19, 189, 44, N'Pieza', N'Hongo portobello grande, rebanado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (45, 98, N'1', 29, 58, 45, N'Taza', N'Champiñon(es), rebanados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (46, 98, N'1', 29, 190, 46, N'Taza', N'Hongos crimini, rebanados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (47, 98, N'1/2', 29, 330, 47, N'Taza', N'Setas rebanadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (48, 98, N'1', 22, 321, 48, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (49, 98, N'1', 3, 232, 49, N'Cucharada', N'Mejorana fresca, picada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (50, 98, N'1/4', 29, 371, 50, N'Taza', N'Vino blanco')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (51, 98, N'1', 29, 137, 51, N'Taza', N'Espinacas baby, lavadas y desinfectadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (52, 98, N'1', 3, 298, 52, N'Cucharada', N'Queso de cabra, desmoronado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (53, 99, N'2', 15, 10, 53, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (54, 99, N'1/2', 3, 319, 54, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (55, 99, N'200', 8, 360, 55, N'Gr', N'Tornillo la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (56, 99, N'1', 29, 97, 56, N'Taza', N'Crema baja en grasa')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (57, 99, N'1/2', 29, 214, 57, N'Taza', N'Leche descremada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (58, 99, N'1/4', 29, 306, 58, N'Taza', N'Queso manchego bajo en grasa, rallado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (59, 99, N'1', 22, 276, 59, N'Pizca', N'Pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (60, 99, N'1 1/2', 29, 60, 60, N'Taza', N'Chícharos, blanqueados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (61, 99, N'120', 8, 199, 61, N'Gr', N'Jamón serrano, en rollos chicos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (62, 100, N'2', 15, 10, 62, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (63, 100, N'1/2', 3, 319, 63, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (64, 100, N'200', 8, 359, 64, N'Gr', N'Tornillo con fibra la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (65, 100, N'1', 3, 4, 65, N'Cucharada', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (66, 100, N'2', 26, 342, 66, N'Rebanadas', N'Tocino de pavo, picadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (67, 100, N'2', 20, 123, 67, N'Piezas', N'Dientes de ajo, picados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (68, 100, N'2', 30, 91, 68, N'Tazas', N'Col blanca, lavada, desinfectada y fileteada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (69, 100, N'2', 30, 57, 69, N'Tazas', N'Champiñon(es), en cuartos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (70, 100, N'4', 4, 247, 70, N'Cucharadas', N'Nueces, tostadas y picadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (71, 100, N'1', 22, 321, 71, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (72, 100, N'1', 19, 62, 72, N'Pieza', N'Chile ancho, limpio y picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (73, 101, N'1/2', 19, 93, 73, N'Pieza', N'Coliflor mediana, en ramitos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (74, 101, N'1', 19, 126, 74, N'Pieza', N'Echalote, en mitades')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (75, 101, N'3', 20, 117, 75, N'Piezas', N'Dientes de ajo')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (76, 101, N'2', 4, 3, 76, N'Cucharadas', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (77, 101, N'1', 22, 321, 77, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (78, 101, N'1', 29, 214, 78, N'Taza', N'Leche descremada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (79, 101, N'1', 3, 240, 79, N'Cucharada', N'Mostaza dijón')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (80, 101, N'1', 3, 3, 80, N'Cucharada', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (81, 101, N'200', 17, 148, 81, N'Paquete', N'Fideo con fobra la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (82, 101, N'3', 30, 10, 82, N'Tazas', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (83, 101, N'1/2', 29, 185, 83, N'Taza', N'Hojas de perejil, lavadas, desinfectadas y ligeramente picadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (84, 102, N'5', 30, 43, 84, N'Tazas', N'Caldo de res sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (85, 102, N'2', 20, 381, 85, N'Piezas', N'Zanahorias, en rodajas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (86, 102, N'1/2', 29, 23, 86, N'Taza', N'Apio en cubos chicos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (87, 102, N'1', 29, 162, 87, N'Taza', N'Granos de elote blanco')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (88, 102, N'1/2', 29, 59, 88, N'Taza', N'Chícharos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (89, 102, N'1', 29, 159, 89, N'Taza', N'Garbanzos, remojados la noche anterior')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (90, 102, N'100', 17, 124, 90, N'Paquete', N'Dinosaurios la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (91, 102, N'1', 29, 152, 91, N'Taza', N'Flada o chambarete de res, cocida y deshebrada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (92, 102, N'4', 4, 307, 92, N'Cucharadas', N'Queso manchego, rallado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (93, 102, N'1', 22, 321, 93, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (94, 102, N'2', 4, 177, 94, N'Cucharadas', N'Hojas de cilantro, lavadas y desinfectadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (95, 103, N'2', 15, 10, 95, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (96, 103, N'1/2', 3, 319, 96, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (97, 103, N'200', 17, 151, 97, N'Paquete', N'Figuras del oeste la moderna kids')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (98, 103, N'2', 4, 3, 98, N'Cucharadas', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (99, 103, N'1', 3, 74, 99, N'Cucharada', N'Chile piquín molido')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (100, 103, N'1', 19, 111, 100, N'Pieza', N'Diente de ajo finamente picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (101, 103, N'1', 22, 321, 101, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (102, 103, N'1', 19, 266, 102, N'Pieza', N'Pechuga de pollo en cubos medianos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (103, 103, N'1', 19, 36, 103, N'Pieza', N'Calabacitas, en medias lunas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (104, 103, N'1', 29, 161, 104, N'Taza', N'Granos de elote amarillo')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (105, 103, N'1', 19, 282, 105, N'Pieza', N'Pimiento rojo en cubos chicos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (106, 103, N'1', 29, 136, 106, N'Taza', N'Espinacas baby')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (107, 103, N'3', 4, 3, 107, N'Cucharadas', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (108, 103, N'1', 3, 369, 108, N'Cucharada', N'Vinagre de manzana')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (109, 103, N'2', 4, 239, 109, N'Cucharadas', N'Mostaza dijon')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (110, 103, N'1', 3, 234, 110, N'Cucharada', N'Miel')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (111, 103, N'1', 22, 321, 111, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (112, 104, N'2', 15, 10, 112, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (113, 104, N'1/2', 3, 319, 113, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (114, 104, N'200', 8, 242, 114, N'Gr', N'Munición la moderna kids')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (115, 104, N'2', 13, 26, 115, N'Latas', N'Atún ')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (116, 104, N'1', 19, 377, 116, N'Pieza', N'Zanahoria rayada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (117, 104, N'2', 20, 83, 117, N'Piezas', N'Claras de huevo')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (118, 104, N'1/2', 29, 164, 118, N'Taza', N'Harina')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (119, 104, N'2', 20, 192, 119, N'Piezas', N'Huevos batidos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (120, 104, N'1 1/4', 11, 28, 120, N'Kg', N'Avena molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (121, 104, N'1', 19, 5, 121, N'Pieza', N'Aceite en aerosol')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (122, 104, N'1', 19, 225, 122, N'Pieza', N'Mango manila en cubos medianos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (123, 104, N'1', 3, 235, 123, N'Cucharada', N'Miel de abeja')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (124, 104, N'2', 4, 209, 124, N'Cucharadas', N'Jugo de limon')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (125, 104, N'1/4', 29, 10, 125, N'Taza', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (126, 104, N'5', 10, 233, 126, N'Hojas', N'Menta lavada, desinfectada y finamente picada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (127, 105, N'2', 4, 3, 127, N'Cucharadas', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (128, 105, N'200', 17, 124, 128, N'Paquete', N'Dinosaurios la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (129, 105, N'3', 20, 205, 129, N'Piezas', N'Jitomates en cuartos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (130, 105, N'4', 30, 41, 130, N'Tazas', N'Caldo de pollo sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (131, 105, N'2', 20, 379, 131, N'Piezas', N'Zanahorias en cubos chicos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (132, 105, N'1/2', 29, 161, 132, N'Taza', N'Granos de elote amarillo')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (133, 105, N'5', 10, 138, 133, N'Hojas', N'Espinacas lavadas, desinfectadas y picadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (134, 105, N'1/2', 19, 265, 134, N'Pieza', N'Pechuga de pollo cocinada y deshebrada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (135, 105, N'1', 22, 321, 135, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (136, 105, N'4', 4, 300, 136, N'Cucharadas', N'Queso fresco en cubos chicos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (137, 106, N'2', 15, 10, 137, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (138, 106, N'1/2', 3, 319, 138, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (139, 106, N'200', 17, 88, 139, N'Paquete', N'Codo no.1 la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (140, 106, N'4', 30, 41, 140, N'Tazas', N'Caldo de pollo sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (141, 106, N'1', 29, 162, 141, N'Taza', N'Granos de elote blanco')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (142, 106, N'12', 19, 57, 142, N'Pieza', N'Champiñon(es), en cuartos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (143, 106, N'2', 20, 36, 143, N'Piezas', N'Calabacitas, en medias lunas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (144, 106, N'2', 20, 179, 144, N'Piezas', N'Hojas de epazote, lavadas y desinfectadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (145, 106, N'1', 22, 321, 145, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (146, 107, N'2', 4, 3, 146, N'Cucharadas', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (147, 107, N'2', 20, 122, 147, N'Piezas', N'Dientes de ajo, finamente picados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (148, 107, N'1/2', 19, 55, 148, N'Pieza', N'Cebolla, finamente picada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (149, 107, N'1/2', 29, 160, 149, N'Taza', N'Granos de elote')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (150, 107, N'2', 20, 380, 150, N'Piezas', N'Zanahorias, en cubos chicos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (151, 107, N'1/2', 19, 260, 151, N'Pieza', N'Papa chica, en cubos chicos (con piel)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (152, 107, N'1', 19, 35, 152, N'Pieza', N'Calabacitas, en láminas delgadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (153, 107, N'1', 27, 23, 153, N'Tallo', N'Apio en cubos chicos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (154, 107, N'5 1/2', 30, 44, 154, N'Tazas', N'Caldo de vegetales, sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (155, 107, N'2', 17, 133, 155, N'Paquete', N'Engrane la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (156, 107, N'1', 22, 322, 156, N'Pizca', N'Sal y pimiental')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (157, 107, N'2', 25, 353, 157, N'Ramas', N'Tomillo')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (158, 107, N'2', 10, 212, 158, N'Hojas', N'Laurel')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (159, 107, N'200', 8, 220, 159, N'Gr', N'Lomo de huachinango, en cubos grandes')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (160, 108, N'2', 15, 10, 160, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (161, 108, N'1', 3, 319, 161, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (162, 108, N'200', 17, 88, 162, N'Paquete', N'Codo no.1 la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (163, 108, N'1/2', 29, 374, 163, N'Taza', N'Yogurt natural bajo en grasa sin azúcar')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (164, 108, N'1', 3, 234, 164, N'Cucharada', N'Miel')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (165, 108, N'2', 20, 228, 165, N'Piezas', N'Manzanas rojas en cubos chicos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (166, 108, N'1', 22, 276, 166, N'Pizca', N'Pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (167, 108, N'12', 26, 195, 167, N'Rebanadas', N'Jamón')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (168, 109, N'2', 15, 10, 168, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (169, 109, N'1', 3, 319, 169, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (170, 109, N'200', 17, 47, 170, N'Paquete', N'Caracol 1')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (171, 109, N'2', 30, 349, 171, N'Tazas', N'Tomates (jitomates) cherry, en mitades')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (172, 109, N'1', 29, 174, 172, N'Taza', N'Hojas de albahaca, lavadas y desinfectadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (173, 109, N'1', 29, 303, 173, N'Taza', N'Queso fresco, en cubos chicos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (174, 110, N'2', 15, 10, 174, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (175, 110, N'1', 3, 319, 175, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (176, 110, N'200', 17, 22, 176, N'Paquete', N'Animales de la selva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (177, 110, N'2', 30, 198, 177, N'Tazas', N'Jamón de pierna sin sal en cubos chicos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (178, 110, N'3', 4, 81, 178, N'Cucharadas', N'Cilantro lavado, desinfectado y picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (179, 110, N'1/4', 19, 53, 179, N'Pieza', N'Cebolla morada, finamente picada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (180, 110, N'1/2', 19, 285, 180, N'Pieza', N'Pimiento verde en cubos chicos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (181, 110, N'1/2', 19, 69, 181, N'Pieza', N'Chile guajillo sin semillas y picado (opcional)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (182, 110, N'4', 4, 97, 182, N'Cucharadas', N'Crema baja en grasa')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (183, 110, N'1', 3, 210, 183, N'Cucharada', N'Jugo de limón')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (184, 110, N'1', 22, 321, 184, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (185, 110, N'6', 20, 125, 185, N'Piezas', N'Duraznos grandes en mitades y sin hueso')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (186, 111, N'1', 19, 288, 186, N'Pieza', N'Plátano macho en láminas delgadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (187, 111, N'1', 19, 5, 187, N'Pieza', N'Aceite en aerosol')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (188, 111, N'1', 3, 356, 188, N'Cucharada', N'Tomillo fresco picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (189, 111, N'3', 4, 3, 189, N'Cucharadas', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (190, 111, N'1', 19, 107, 190, N'Pieza', N'Diente de ajo')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (191, 111, N'1/4', 19, 50, 191, N'Pieza', N'Cebolla')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (192, 111, N'3', 20, 206, 192, N'Piezas', N'Jitomates en cuartos y sin semillas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (193, 111, N'2', 30, 41, 193, N'Tazas', N'Caldo de pollo sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (194, 111, N'3/4', 29, 82, 194, N'Taza', N'Ciruelas pasa sin hueso, hidratadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (195, 111, N'1', 22, 321, 195, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (196, 111, N'200', 17, 150, 196, N'Paquete', N'Fideo no.2 la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (197, 111, N'4', 4, 301, 197, N'Cucharadas', N'Queso fresco rallado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (198, 111, N'4', 4, 81, 198, N'Cucharadas', N'Cilantro lavado, desinfectado y picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (199, 112, N'2', 4, 226, 199, N'Cucharadas', N'Mantequilla sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (200, 112, N'3', 20, 120, 200, N'Piezas', N'Dientes de ajo finamente picados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (201, 112, N'1/4', 19, 55, 201, N'Pieza', N'Cebolla, finamente picada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (202, 112, N'2', 20, 34, 202, N'Piezas', N'Calabacitas en rodajas delgadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (203, 112, N'200', 17, 149, 203, N'Paquete', N'Fideo no.1 la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (204, 112, N'5', 30, 40, 204, N'Tazas', N'Caldo de pollo o vegetales sin grasa ni sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (205, 112, N'2', 25, 354, 205, N'Ramas', N'Tomillo fresco')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (206, 112, N'1', 27, 270, 206, N'Tallo', N'Perejil')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (207, 112, N'1', 22, 321, 207, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (208, 112, N'4', 4, 273, 208, N'Cucharadas', N'Perejil lavado, desinfectado y picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (209, 112, N'2', 20, 361, 209, N'Piezas', N'Tortillas en julianas y horneadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (210, 113, N'1/4', 19, 50, 210, N'Pieza', N'Cebolla')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (211, 113, N'1', 19, 107, 211, N'Pieza', N'Diente de ajo')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (212, 113, N'1', 19, 345, 212, N'Pieza', N'Tomate (jitomate), asado y en cuartos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (213, 113, N'1/2', 29, 156, 213, N'Taza', N'Frijoles bayos enteros, cocinados en agua con sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (214, 113, N'1', 29, 38, 214, N'Taza', N'Caldo de frijol')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (215, 113, N'1', 19, 71, 215, N'Pieza', N'Chile morita, asado e hidratado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (216, 113, N'1', 22, 321, 216, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (217, 113, N'1', 3, 3, 217, N'Cucharada', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (218, 113, N'200', 17, 149, 218, N'Paquete', N'Fideo no.1 la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (219, 113, N'3', 30, 10, 219, N'Tazas', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (220, 113, N'4', 4, 97, 220, N'Cucharadas', N'Crema baja en grasa')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (221, 113, N'1/2', 19, 12, 221, N'Pieza', N'Aguacate, rebanado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (222, 113, N'1/2', 29, 61, 222, N'Taza', N'Chicharrón de cerdo, troceado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (223, 114, N'2', 15, 10, 223, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (224, 114, N'1', 22, 319, 224, N'Pizca', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (225, 114, N'200', 8, 237, 225, N'Gr', N'Moño la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (226, 114, N'1', 3, 3, 226, N'Cucharada', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (227, 114, N'100', 8, 238, 227, N'Gr', N'Mortadela, en cuadros chicos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (228, 114, N'1', 19, 72, 228, N'Pieza', N'Chile morita, sin semillas y finamente picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (229, 114, N'2', 4, 186, 229, N'Cucharadas', N'Hojas de perejil, lavadas, desinfectadas y picadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (230, 114, N'1', 29, 37, 230, N'Taza', N'Caldillo de tomate, frio')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (231, 114, N'1/2', 29, 378, 231, N'Taza', N'Zanahoria, en cubos chicos y blanqueados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (232, 114, N'1/2', 29, 59, 232, N'Taza', N'Chícharos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (233, 114, N'1/2', 29, 131, 233, N'Taza', N'Elotes amarillos congelados, descongelados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (234, 114, N'1/2', 29, 158, 234, N'Taza', N'Frijoles negros, cocinados en agua sin sal y escurridos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (235, 115, N'2', 15, 10, 235, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (236, 115, N'1', 22, 319, 236, N'Pizca', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (237, 115, N'200', 17, 140, 237, N'Paquete', N'Espiral la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (238, 115, N'2', 4, 226, 238, N'Cucharadas', N'Mantequilla sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (239, 115, N'2', 30, 328, 239, N'Tazas', N'Salsa de tomate para pizza')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (240, 115, N'2', 30, 309, 240, N'Tazas', N'Queso mozzarella, rallado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (241, 115, N'1', 29, 268, 241, N'Taza', N'Peperoni rebanado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (242, 116, N'2', 15, 10, 242, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (243, 116, N'1', 22, 319, 243, N'Pizca', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (244, 116, N'200', 17, 222, 244, N'Paquete', N'Macarrón corto la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (245, 116, N'1', 3, 226, 245, N'Cucharada', N'Mantequilla sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (246, 116, N'2', 19, 122, 246, N'Pieza', N'Dientes de ajo, finamente picados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (247, 116, N'1/2', 19, 55, 247, N'Pieza', N'Cebolla, finamente picada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (248, 116, N'1', 22, 276, 248, N'Pizca', N'Pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (249, 116, N'2', 19, 79, 249, N'Pieza', N'Chiles serranos, sin semillas y picados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (250, 116, N'2', 19, 350, 250, N'Pieza', N'Tomates (jitomates), en octavos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (251, 116, N'2', 13, 26, 251, N'Latas', N'Atún ')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (252, 116, N'2', 20, 193, 252, N'Piezas', N'Huevos duros, rebanados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (253, 116, N'4', 4, 169, 253, N'Cucharadas', N'Hojas chicas de cilantro, lavadas y desinfectadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (254, 116, N'4', 4, 370, 254, N'Cucharadas', N'Vinagreta de tu preferencia')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (255, 117, N'2', 15, 10, 255, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (256, 117, N'1/2', 3, 319, 256, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (257, 117, N'200', 8, 290, 257, N'Gr', N'Pluma la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (258, 117, N'1', 19, 267, 258, N'Pieza', N'Pechuga de pollo, limpia y en cubos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (259, 117, N'1', 3, 3, 259, N'Cucharada', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (260, 117, N'2', 19, 122, 260, N'Pieza', N'Dientes de ajo, finamente picados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (261, 117, N'2', 4, 274, 261, N'Cucharadas', N'Perejil, finamente picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (262, 117, N'1', 22, 321, 262, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (263, 117, N'1', 29, 352, 263, N'Taza', N'Tomates deshidratados, en mitades')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (264, 117, N'1 1/2', 29, 98, 264, N'Taza', N'Crema para batir')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (265, 117, N'2', 4, 227, 265, N'Cucharadas', N'Mantequilla sin sal, fundida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (266, 117, N'1', 3, 166, 266, N'Cucharada', N'Hierbas italianas secas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (267, 117, N'4', 4, 259, 267, N'Cucharadas', N'Pan molido integral')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (268, 117, N'1/3', 29, 311, 268, N'Taza', N'Queso parmesano')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (269, 117, N'1/3', 29, 305, 269, N'Taza', N'Queso gruyère')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (270, 118, N'1', 19, 346, 270, N'Pieza', N'Tomate (jitomate), en cuartos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (271, 118, N'1/2', 19, 50, 271, N'Pieza', N'Cebolla')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (272, 118, N'2', 20, 117, 272, N'Piezas', N'Dientes de ajo')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (273, 118, N'1/2', 29, 244, 273, N'Taza', N'Nueces')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (274, 118, N'2', 4, 97, 274, N'Cucharadas', N'Crema baja en grasa')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (275, 118, N'1', 29, 41, 275, N'Taza', N'Caldo de pollo sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (276, 118, N'2', 4, 98, 276, N'Cucharadas', N'Crema para batir')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (277, 118, N'1', 22, 320, 277, N'Pizca', N'Sal y pimienta')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (278, 118, N'2', 15, 10, 278, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (279, 118, N'1/2', 3, 319, 279, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (280, 118, N'200', 8, 335, 280, N'Gr', N'Spaghetti la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (281, 118, N'200', 8, 262, 281, N'Gr', N'Pechuga de pavo horneada\nen rebanadas delgadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (282, 118, N'1/2', 29, 24, 282, N'Taza', N'Arándanos deshidratados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (283, 118, N'2', 4, 183, 283, N'Cucharadas', N'Hojas de perejil lavadas\ndesinfectadas y picadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (284, 119, N'2', 15, 10, 284, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (285, 119, N'1/2', 3, 319, 285, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (286, 119, N'200', 8, 237, 286, N'Gr', N'Moño la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (287, 119, N'2', 4, 4, 287, N'Cucharadas', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (288, 119, N'1/4', 19, 54, 288, N'Pieza', N'Cebolla, fileteada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (289, 119, N'1', 19, 115, 289, N'Pieza', N'Diente de ajo, finamente picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (290, 119, N'2', 30, 25, 290, N'Tazas', N'Arúgula baby, lavada y desinfectada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (291, 119, N'1', 29, 157, 291, N'Taza', N'Frijoles negros, cocinados en agua sin sal (sin caldo)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (292, 119, N'1/3', 29, 247, 292, N'Taza', N'Nueces, tostadas y picadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (293, 119, N'1/2', 3, 254, 293, N'Cucharada', N'Orégano fresco, picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (294, 119, N'3', 10, 329, 294, N'Hojas', N'Salvia')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (295, 119, N'1', 22, 321, 295, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (296, 120, N'2', 15, 10, 296, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (297, 120, N'1/2', 3, 319, 297, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (298, 120, N'200', 17, 143, 298, N'Paquete', N'Fettuccine la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (299, 120, N'1', 29, 98, 299, N'Taza', N'Crema para batir')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (300, 120, N'1/3', 29, 297, 300, N'Taza', N'Queso de cabra')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (301, 120, N'1/4', 29, 311, 301, N'Taza', N'Queso parmesano')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (302, 120, N'2', 19, 122, 302, N'Pieza', N'Dientes de ajo, finamente picados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (303, 120, N'1/4', 19, 55, 303, N'Pieza', N'Cebolla, finamente picada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (304, 120, N'2', 4, 314, 304, N'Cucharadas', N'Ralladura de cáscara de limón')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (305, 120, N'1', 22, 321, 305, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (306, 120, N'2', 4, 20, 306, N'Cucharadas', N'Almendras tostadas y ligeramente picadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (307, 120, N'2', 4, 272, 307, N'Cucharadas', N'Perejil fresco, picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (308, 121, N'2', 15, 10, 308, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (309, 121, N'1/2', 3, 319, 309, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (310, 121, N'200', 8, 335, 310, N'Gr', N'Spaghetti la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (311, 121, N'2', 4, 4, 311, N'Cucharadas', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (312, 121, N'2', 20, 122, 312, N'Piezas', N'Dientes de ajo, finamente picados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (313, 121, N'16', 20, 45, 313, N'Piezas', N'Camarón(es)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (314, 121, N'1', 22, 321, 314, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (315, 121, N'2', 4, 275, 315, N'Cucharadas', N'Perejil, lavado, desinfectado y picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (316, 121, N'1', 3, 314, 316, N'Cucharada', N'Ralladura de cáscara de limón')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (317, 121, N'1/4', 29, 210, 317, N'Taza', N'Jugo de limón')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (318, 121, N'2', 4, 371, 318, N'Cucharadas', N'Vino blanco')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (319, 121, N'1', 29, 348, 319, N'Taza', N'Tomates (jitomates) cherry')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (320, 121, N'4', 4, 174, 320, N'Cucharadas', N'Hojas de albahaca, lavadas y desinfectadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (321, 121, N'1/2', 29, 308, 321, N'Taza', N'Queso mozzarella fresco y en cubos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (322, 122, N'1', 29, 98, 322, N'Taza', N'Crema para batir')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (323, 122, N'1/4', 29, 292, 323, N'Taza', N'Queso azul')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (324, 122, N'1', 22, 321, 324, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (325, 122, N'2', 15, 10, 325, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (326, 122, N'1/2', 3, 319, 326, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (327, 122, N'200', 8, 237, 327, N'Gr', N'Moño la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (328, 122, N'1', 3, 3, 328, N'Cucharada', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (329, 122, N'2', 20, 122, 329, N'Piezas', N'Dientes de ajo, finamente picados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (330, 122, N'16', 20, 45, 330, N'Piezas', N'Camarón(es)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (331, 122, N'1', 3, 261, 331, N'Cucharada', N'Páprika')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (332, 122, N'1', 22, 321, 332, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (333, 122, N'12', 20, 135, 333, N'Piezas', N'Espárragos, blanqueados y cortados en cuatro')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (334, 122, N'2', 4, 27, 334, N'Cucharadas', N'Avellanas, tostadas y en mitades')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (335, 123, N'1', 19, 278, 335, N'Pieza', N'Pimiento amarillo, limpio y escalfado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (336, 123, N'1/2', 29, 98, 336, N'Taza', N'Crema para batir')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (337, 123, N'1/2', 29, 44, 337, N'Taza', N'Caldo de vegetales, sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (338, 123, N'1', 19, 64, 338, N'Pieza', N'Chile de árbol fresco, sin semillas ni venas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (339, 123, N'1', 3, 4, 339, N'Cucharada', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (340, 123, N'1', 22, 257, 340, N'Pizca', N'Orégano, sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (341, 123, N'2', 15, 10, 341, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (342, 123, N'1/2', 3, 319, 342, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (343, 123, N'200', 17, 144, 343, N'Paquete', N'Fetuccine la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (344, 123, N'2', 30, 137, 344, N'Tazas', N'Espinacas baby, lavadas y desinfectadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (345, 123, N'2', 26, 341, 345, N'Rebanadas', N'Tocino de cerdo ahumado, en cubos chicos (40g)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (346, 123, N'4', 20, 121, 346, N'Piezas', N'Dientes de ajo, en julianas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (347, 94, N'2', 14, 10, 347, N'Lt', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (348, 94, N'1', 22, 319, 348, N'Pizca', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (349, 94, N'200', 8, 360, 349, N'Gr', N'Tornillo la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (350, 94, N'1', 3, 4, 350, N'Cucharada', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (351, 94, N'1', 19, 116, 351, N'Pieza', N'Diente de ajo, picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (352, 94, N'1/2', 19, 53, 352, N'Pieza', N'Cebolla morada, finamente picada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (353, 94, N'2', 20, 286, 353, N'Piezas', N'Pimientos rojos, en cubos chicos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (354, 94, N'1/2', 29, 184, 354, N'Taza', N'Hojas de perejil, lavadas y desinfectadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (355, 94, N'2', 20, 219, 355, N'Piezas', N'Limones amarillos (jugo y ralladura)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (356, 94, N'1/2', 29, 97, 356, N'Taza', N'Crema baja en grasa')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (357, 126, N'6', 4, 4, 357, N'Cucharadas', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (358, 126, N'2', 4, 365, 358, N'Cucharadas', N'Vinagre balsámico')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (359, 126, N'1/2', 3, 210, 359, N'Cucharada', N'Jugo de limón')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (360, 126, N'1', 22, 321, 360, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (361, 126, N'2', 14, 10, 361, N'Lt', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (362, 126, N'1/2', 3, 319, 362, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (363, 126, N'200', 17, 223, 363, N'Paquete', N'Macarrón la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (364, 126, N'1', 29, 362, 364, N'Taza', N'Uvas rojas sin semillas, en mitades')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (365, 126, N'1', 29, 363, 365, N'Taza', N'Uvas verdes sin semillas,en mitades')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (366, 126, N'1/2', 29, 6, 366, N'Taza', N'Aceitunas negras sin hueso')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (367, 126, N'1/2', 29, 7, 367, N'Taza', N'Aceitunas verdes sin hueso')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (368, 126, N'1/2', 29, 21, 368, N'Taza', N'Almendras, tostadas y en mitades')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (369, 126, N'2', 4, 304, 369, N'Cucharadas', N'Queso gorgonzola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (370, 124, N'2', 15, 10, 370, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (371, 124, N'1', 3, 319, 371, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (372, 124, N'200', 17, 87, 372, N'Paquete', N'Codo no. 4 la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (373, 124, N'2', 26, 340, 373, N'Rebanadas', N'Tocino de cerdo ahumado (60g) en cubos chicos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (374, 124, N'3', 30, 92, 374, N'Tazas', N'Coles de bruselas, en mitades y blanqueadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (375, 124, N'3', 4, 271, 375, N'Cucharadas', N'Perejil fresco, finamente picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (376, 124, N'1/2', 3, 261, 376, N'Cucharada', N'Páprika')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (377, 124, N'1', 22, 276, 377, N'Pizca', N'Pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (378, 125, N'1/4', 29, 294, 378, N'Taza', N'Queso cotija')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (379, 125, N'1 1/2', 30, 213, 379, N'Tazas', N'Leche baja en garsa')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (380, 125, N'1/2', 29, 248, 380, N'Taza', N'Nuez de castilla, limpia')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (381, 125, N'1 1/2', 4, 29, 381, N'Cucharadas', N'Azúcar')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (382, 125, N'1', 24, 46, 382, N'Raja', N'Canela (chica)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (383, 125, N'1', 3, 201, 383, N'Cucharada', N'Jerez')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (384, 125, N'2', 15, 10, 384, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (385, 125, N'1/2', 3, 319, 385, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (386, 125, N'200', 17, 144, 386, N'Paquete', N'Fetuccine la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (387, 125, N'2', 4, 3, 387, N'Cucharadas', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (388, 125, N'1', 19, 112, 388, N'Pieza', N'Diente de ajo picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (389, 125, N'1/4', 19, 55, 389, N'Pieza', N'Cebolla, finamente picada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (390, 125, N'2', 20, 77, 390, N'Piezas', N'Chiles poblanos chicos, limpios, escalfados y en rajas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (391, 125, N'1/2', 29, 163, 391, N'Taza', N'Granos de granada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (392, 125, N'3', 4, 275, 392, N'Cucharadas', N'Perejil, lavado, desinfectado y picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (393, 129, N'2', 15, 10, 393, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (394, 129, N'1/2', 3, 319, 394, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (395, 129, N'200', 8, 224, 395, N'Gr', N'Macarrón largo la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (396, 129, N'2', 4, 3, 396, N'Cucharadas', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (397, 129, N'2', 20, 13, 397, N'Piezas', N'Ajo')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (398, 129, N'1/2', 19, 55, 398, N'Pieza', N'Cebolla, finamente picada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (399, 129, N'200', 8, 49, 399, N'Gr', N'Carne molida de res')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (400, 129, N'4', 20, 202, 400, N'Piezas', N'Jitomate (asados)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (401, 129, N'2', 4, 291, 401, N'Cucharadas', N'Puré de tomate')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (402, 129, N'1', 19, 70, 402, N'Pieza', N'Chile guajillo, limpio, asado e hidratado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (403, 129, N'1', 22, 276, 403, N'Pizca', N'Pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (404, 129, N'1/4', 29, 176, 404, N'Taza', N'Hojas de cilantro (lavadas y desinfecatadas)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (405, 129, N'1/2', 29, 41, 405, N'Taza', N'Caldo de pollo sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (406, 129, N'1', 3, 253, 406, N'Cucharada', N'Orégano fresco (picado)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (407, 129, N'1/2', 29, 299, 407, N'Taza', N'Queso emmental o gouda (rallado)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (408, 129, N'1/4', 29, 312, 408, N'Taza', N'Queso parmesano (rallado)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (409, 128, N'2', 15, 10, 409, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (410, 128, N'1/2', 3, 319, 410, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (411, 128, N'200', 8, 335, 411, N'Gr', N'Spaghetti la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (412, 128, N'2', 4, 226, 412, N'Cucharadas', N'Mantequilla sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (413, 128, N'1', 3, 3, 413, N'Cucharada', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (414, 128, N'1', 19, 111, 414, N'Pieza', N'Diente de ajo finamente picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (415, 128, N'1/2', 19, 54, 415, N'Pieza', N'Cebolla, fileteada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (416, 128, N'1 1/2', 30, 153, 416, N'Tazas', N'Flores de jamainca hidratadas y ligeramente picadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (417, 128, N'1', 19, 68, 417, N'Pieza', N'Chile guajillo asado y en julianas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (418, 128, N'1', 3, 255, 418, N'Cucharada', N'Orégano seco, molido')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (419, 128, N'1', 22, 321, 419, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (420, 128, N'1/2', 29, 42, 420, N'Taza', N'Caldo de polo sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (421, 128, N'1/2', 29, 181, 421, N'Taza', N'Hojas de perejil lavadas y desinfectadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (422, 128, N'1', 3, 315, 422, N'Cucharada', N'Ralladura de cáscara de naranja')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (423, 130, N'2', 15, 10, 423, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (424, 130, N'1/2', 3, 319, 424, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (425, 130, N'200', 8, 335, 425, N'Gr', N'Spaghetti la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (426, 130, N'1', 3, 4, 426, N'Cucharada', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (427, 130, N'2', 4, 229, 427, N'Cucharadas', N'Matequilla sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (428, 130, N'2', 20, 114, 428, N'Piezas', N'Diente de ajo, fileteados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (429, 130, N'1', 19, 188, 429, N'Pieza', N'Hongo portobello grande, en láminas delgadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (430, 130, N'1 1/2', 30, 130, 430, N'Tazas', N'Ejotes, limpios, en mitades y blanqueados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (431, 130, N'1', 19, 167, 431, N'Pieza', N'Hoja de epazote, lavada, desinfectada y picada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (432, 130, N'1', 22, 321, 432, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (433, 130, N'3', 4, 210, 433, N'Cucharadas', N'Jugo de limón')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (434, 130, N'1', 3, 29, 434, N'Cucharada', N'Azúcar')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (435, 131, N'1', 3, 3, 435, N'Cucharada', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (436, 131, N'1', 19, 109, 436, N'Pieza', N'Diente de ajo (picado)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (437, 131, N'1/4', 19, 55, 437, N'Pieza', N'Cebolla, finamente picada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (438, 131, N'3', 30, 204, 438, N'Tazas', N'Jitomates cherry (en mitades)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (439, 131, N'1', 3, 291, 439, N'Cucharada', N'Puré de tomate')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (440, 131, N'2', 20, 67, 440, N'Piezas', N'Chile de árbol, finamente picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (441, 131, N'1', 22, 323, 441, N'Pizca', N'Sal, orégano seco molido y pimienta de caneya molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (442, 131, N'1/4', 29, 10, 442, N'Taza', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (443, 131, N'1/4', 29, 172, 443, N'Taza', N'Hojas de albahaca (lavadas, desinfectadas y picadas)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (444, 131, N'2', 15, 10, 444, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (445, 131, N'1/2', 3, 319, 445, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (446, 131, N'200', 8, 360, 446, N'Gr', N'Tornillo la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (447, 131, N'2', 4, 4, 447, N'Cucharadas', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (448, 131, N'1', 19, 108, 448, N'Pieza', N'Diente de ajo (finamente picado)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (449, 131, N'1', 3, 200, 449, N'Cucharada', N'Jengibre (finamente picado)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (450, 131, N'16', 20, 45, 450, N'Piezas', N'Camarón(es)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (451, 131, N'4', 4, 312, 451, N'Cucharadas', N'Queso parmesano (rallado)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (452, 132, N'2', 20, 117, 452, N'Piezas', N'Dientes de ajo')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (453, 132, N'2', 4, 249, 453, N'Cucharadas', N'Nuez en mitades')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (454, 132, N'2', 4, 313, 454, N'Cucharadas', N'Queso parmesano, rallado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (455, 132, N'1/2', 29, 175, 455, N'Taza', N'Hojas de cilantro')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (456, 132, N'2', 4, 4, 456, N'Cucharadas', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (457, 132, N'1', 22, 276, 457, N'Pizca', N'Pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (458, 132, N'1/2', 29, 98, 458, N'Taza', N'Crema para batir')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (459, 132, N'2', 15, 10, 459, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (460, 132, N'1', 3, 319, 460, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (461, 132, N'200', 8, 335, 461, N'Gr', N'Spaghetti la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (462, 132, N'1', 3, 226, 462, N'Cucharada', N'Mantequilla sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (463, 132, N'2', 20, 123, 463, N'Piezas', N'Dientes de ajo, picados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (464, 132, N'1/2', 19, 55, 464, N'Pieza', N'Cebolla, finamente picada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (465, 132, N'2', 4, 336, 465, N'Cucharadas', N'Tallo de té de limón, finamente picadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (466, 132, N'200', 8, 17, 466, N'Gr', N'Almejas blancas, muy fescas y enjuagadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (467, 132, N'1/2', 29, 39, 467, N'Taza', N'Caldo de pescado o agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (468, 132, N'2', 4, 178, 468, N'Cucharadas', N'Hojas de cilantro, lavadas, desinfectadas y picadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (469, 133, N'2', 15, 10, 469, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (470, 133, N'1', 3, 319, 470, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (471, 133, N'200', 8, 360, 471, N'Gr', N'Tornillo la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (472, 133, N'2', 30, 59, 472, N'Tazas', N'Chícharos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (473, 133, N'3', 4, 97, 473, N'Cucharadas', N'Crema baja en grasa')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (474, 133, N'1', 3, 210, 474, N'Cucharada', N'Jugo de limón')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (475, 133, N'1', 3, 314, 475, N'Cucharada', N'Ralladura de cáscara de limón')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (476, 133, N'1', 19, 66, 476, N'Pieza', N'Chile de árbol seco, limpio y picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (477, 133, N'1', 19, 276, 477, N'Pieza', N'Pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (478, 133, N'2', 13, 26, 478, N'Latas', N'Atún ')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (479, 134, N'2', 15, 10, 479, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (480, 134, N'1/2', 3, 319, 480, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (481, 134, N'200', 8, 335, 481, N'Gr', N'Spaghetti la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (482, 134, N'1', 3, 4, 482, N'Cucharada', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (483, 134, N'1', 19, 108, 483, N'Pieza', N'Diente de ajo (finamente picado)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (484, 134, N'2', 20, 76, 484, N'Piezas', N'Chiles de árbol, frescos, sin semillas y finamente picados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (485, 134, N'2', 20, 35, 485, N'Piezas', N'Calabacitas, en láminas delgadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (486, 134, N'1', 22, 321, 486, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (487, 134, N'2', 4, 226, 487, N'Cucharadas', N'Mantequilla sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (488, 134, N'1/4', 29, 98, 488, N'Taza', N'Crema para batir')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (489, 134, N'1/4', 29, 10, 489, N'Taza', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (490, 134, N'4', 4, 296, 490, N'Cucharadas', N'Queso crema')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (491, 134, N'2', 4, 253, 491, N'Cucharadas', N'Orégano fresco (picado)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (492, 135, N'2', 15, 10, 492, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (493, 135, N'1/2', 3, 319, 493, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (494, 135, N'200', 8, 335, 494, N'Gr', N'Spaghetti la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (495, 135, N'2', 4, 4, 495, N'Cucharadas', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (496, 135, N'3', 20, 108, 496, N'Piezas', N'Diente de ajo (finamente picado)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (497, 135, N'1', 29, 7, 497, N'Taza', N'Aceitunas verdes sin hueso')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (498, 135, N'1', 22, 276, 498, N'Pizca', N'Pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (499, 135, N'4', 4, 245, 499, N'Cucharadas', N'Nueces caramelizadas (lijeramente picadas)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (500, 135, N'2', 4, 312, 500, N'Cucharadas', N'Queso parmesano (rallado)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (501, 136, N'3', 30, 139, 501, N'Tazas', N'Espinacas, lavadas y desinfectadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (502, 136, N'1', 19, 107, 502, N'Pieza', N'Diente de ajo')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (503, 136, N'1', 29, 317, 503, N'Taza', N'Requezón sin sal o jocoque')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (504, 136, N'1', 19, 217, 504, N'Pieza', N'Limón sin semillas (el jugo)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (505, 136, N'2', 15, 10, 505, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (506, 136, N'1', 3, 319, 506, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (507, 136, N'200', 8, 290, 507, N'Gr', N'Pluma la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (508, 136, N'2', 30, 197, 508, N'Tazas', N'Jamon de pechuga de pavo, en cubos grandes')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (509, 136, N'1/2', 29, 19, 509, N'Taza', N'Almendras en mitades (con piel) y tostadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (510, 136, N'1', 22, 321, 510, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (511, 136, N'2', 4, 313, 511, N'Cucharadas', N'Queso parmesano, rallado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (512, 137, N'1', 3, 3, 512, N'Cucharada', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (513, 137, N'2', 20, 116, 513, N'Piezas', N'Diente de ajo, picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (514, 137, N'1/2', 19, 55, 514, N'Pieza', N'Cebolla, finamente picada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (515, 137, N'2', 4, 339, 515, N'Cucharadas', N'Tallos de cilantro, lavados, desinfectados y picados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (516, 137, N'2', 20, 63, 516, N'Piezas', N'Chile chipotles adobados, sin semillas y picados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (517, 137, N'1/2', 29, 10, 517, N'Taza', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (518, 137, N'1/2', 29, 97, 518, N'Taza', N'Crema baja en grasa')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (519, 137, N'1', 22, 321, 519, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (520, 137, N'1', 22, 251, 520, N'Pizca', N'Nuez moscada molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (521, 137, N'1', 3, 3, 521, N'Cucharada', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (522, 137, N'2', 4, 318, 522, N'Cucharadas', N'Romero fresco, picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (523, 137, N'2', 20, 231, 523, N'Piezas', N'Medias pechugas de pollo, limpias y semi aplanadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (524, 137, N'2', 15, 10, 524, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (525, 137, N'1/2', 3, 319, 525, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (526, 137, N'200', 17, 143, 526, N'Paquete', N'Fettuccine la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (527, 137, N'4', 4, 246, 527, N'Cucharadas', N'Nueces en mitades, tostadas y ligeramente picadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (528, 138, N'1', 3, 4, 528, N'Cucharada', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (529, 138, N'2', 4, 337, 529, N'Cucharadas', N'Tallo de té limón')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (530, 138, N'2', 20, 128, 530, N'Piezas', N'Echalotes (finamente picados)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (531, 138, N'1', 22, 276, 531, N'Pizca', N'Pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (532, 138, N'2', 20, 230, 532, N'Piezas', N'Medias pechugas de pollo (limpias y semiaplanadas)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (533, 138, N'2', 15, 10, 533, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (534, 138, N'200', 8, 237, 534, N'Gr', N'Moño la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (535, 138, N'2', 4, 226, 535, N'Cucharadas', N'Mantequilla sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (536, 138, N'1/4', 19, 51, 536, N'Pieza', N'Cebolla morada, en julianas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (537, 138, N'1/2', 19, 277, 537, N'Pieza', N'Pimiento amarillo (en julianas)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (538, 138, N'1/2', 19, 280, 538, N'Pieza', N'Pimiento naranja (en julianas)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (539, 138, N'1', 19, 33, 539, N'Pieza', N'Calabacita(s), en julianas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (540, 138, N'1', 19, 376, 540, N'Pieza', N'Zanahoria (en julianas)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (541, 138, N'1', 22, 321, 541, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (542, 138, N'1', 3, 253, 542, N'Cucharada', N'Orégano fresco (picado)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (543, 138, N'1', 3, 170, 543, N'Cucharada', N'Hojas de albahaca (lavadas y deinfectadas)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (544, 138, N'1/2', 29, 351, 544, N'Taza', N'Tomates cherry (en mitades)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (545, 139, N'2', 15, 10, 545, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (546, 139, N'1/2', 3, 319, 546, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (547, 139, N'200', 8, 290, 547, N'Gr', N'Pluma la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (548, 139, N'1/2', 29, 18, 548, N'Taza', N'Almendras')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (549, 139, N'1/2', 29, 269, 549, N'Taza', N'Pepitas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (550, 139, N'1', 29, 302, 550, N'Taza', N'Queso fresco, en cubos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (551, 139, N'1/2', 29, 6, 551, N'Taza', N'Aceitunas negras sin hueso')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (552, 139, N'4', 30, 136, 552, N'Tazas', N'Espinacas baby')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (553, 139, N'3', 4, 4, 553, N'Cucharadas', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (554, 139, N'1', 3, 366, 554, N'Cucharada', N'Vinagre balsámico o vinagre manzana')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (555, 139, N'1', 3, 357, 555, N'Cucharada', N'Tomillo fresco, limpio')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (556, 139, N'1', 22, 256, 556, N'Pizca', N'Orégano seco, sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (557, 139, N'2', 15, 10, 557, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (558, 139, N'1/2', 3, 319, 558, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (559, 139, N'200', 17, 143, 559, N'Paquete', N'Fettuccine la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (560, 139, N'4', 4, 246, 560, N'Cucharadas', N'Nueces en mitades, tostadas y ligeramente picadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (561, 140, N'1/4', 3, 3, 561, N'Cucharada', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (562, 140, N'1', 19, 110, 562, N'Pieza', N'Diente de ajo en láminas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (563, 140, N'1/4', 19, 52, 563, N'Pieza', N'Cebolla morada, fileteada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (564, 140, N'2', 20, 78, 564, N'Piezas', N'Chiles poblanos desvenados y pelados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (565, 140, N'2', 4, 81, 565, N'Cucharadas', N'Cilantro lavado, desinfectado y picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (566, 140, N'1', 22, 320, 566, N'Pizca', N'Sal y pimienta')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (567, 140, N'3', 4, 97, 567, N'Cucharadas', N'Crema baja en grasa')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (568, 140, N'1', 29, 44, 568, N'Taza', N'Caldo de vegetales, sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (569, 140, N'2', 15, 10, 569, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (570, 140, N'1/2', 3, 319, 570, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (571, 140, N'200', 8, 290, 571, N'Gr', N'Pluma la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (572, 140, N'2', 4, 3, 572, N'Cucharadas', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (573, 140, N'1/4', 19, 54, 573, N'Pieza', N'Cebolla, fileteada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (574, 140, N'1', 29, 162, 574, N'Taza', N'Granos de elote blanco')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (575, 140, N'1', 22, 321, 575, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (576, 141, N'1', 3, 226, 576, N'Cucharada', N'Mantequilla sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (577, 141, N'1/4', 19, 55, 577, N'Pieza', N'Cebolla, finamente picada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (578, 141, N'1', 19, 108, 578, N'Pieza', N'Diente de ajo (finamente picado)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (579, 141, N'2', 30, 57, 579, N'Tazas', N'Champiñon(es), en cuartos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (580, 141, N'1', 22, 321, 580, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (581, 141, N'1', 3, 355, 581, N'Cucharada', N'Tomillo fresco (picado)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (582, 141, N'1 1/2', 30, 98, 582, N'Tazas', N'Crema para batir')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (583, 141, N'1/2', 29, 11, 583, N'Taza', N'Agua o caldo de pollo sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (584, 141, N'2', 15, 10, 584, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (585, 141, N'1/2', 3, 319, 585, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (586, 141, N'200', 8, 360, 586, N'Gr', N'Tornillo la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (587, 141, N'1', 19, 264, 587, N'Pieza', N'Pechuga de pollo (limpia y semiaplanada)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (588, 141, N'1', 22, 321, 588, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (589, 141, N'1/3', 29, 164, 589, N'Taza', N'Harina')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (590, 141, N'1', 19, 191, 590, N'Pieza', N'Huevo (ligeramente batido)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (591, 141, N'3/4', 29, 187, 591, N'Taza', N'Hojuelas de maíz (molidas)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (592, 141, N'3', 4, 3, 592, N'Cucharadas', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (593, 141, N'2', 4, 141, 593, N'Cucharadas', N'Estragón fresco (picado)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (594, 142, N'2', 15, 10, 594, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (595, 142, N'1/2', 3, 319, 595, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (596, 142, N'200', 8, 237, 596, N'Gr', N'Moño la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (597, 142, N'2', 4, 3, 597, N'Cucharadas', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (598, 142, N'1', 19, 113, 598, N'Pieza', N'Diente de ajo, en cuartos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (599, 142, N'3/4', 19, 54, 599, N'Pieza', N'Cebolla, fileteada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (600, 142, N'1', 29, 194, 600, N'Taza', N'Huitlacoche fresco,limpio')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (601, 142, N'1', 9, 134, 601, N'Hoja', N'Epazote lavado y desinfectado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (602, 142, N'1', 19, 321, 602, N'Pieza', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (603, 142, N'1/2', 29, 10, 603, N'Taza', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (604, 142, N'1/2', 29, 98, 604, N'Taza', N'Crema para batir')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (605, 142, N'1', 3, 4, 605, N'Cucharada', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (606, 142, N'2', 30, 154, 606, N'Tazas', N'Florez de calabaza limpias, lavadas y desinfectadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (607, 143, N'2', 15, 10, 607, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (608, 143, N'1/2', 3, 319, 608, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (609, 143, N'200', 17, 143, 609, N'Paquete', N'Fettuccine la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (610, 143, N'2', 4, 2, 610, N'Cucharadas', N'Aceite de ajonjolí')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (611, 143, N'1', 19, 52, 611, N'Pieza', N'Cebolla morada, fileteada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (612, 143, N'1', 29, 32, 612, N'Taza', N'Brócoli en ramitos y blanqueado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (613, 143, N'1/4', 29, 211, 613, N'Taza', N'Jugo de naranja')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (614, 143, N'2', 30, 161, 614, N'Tazas', N'Granos de elote amarillo')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (615, 143, N'1', 3, 14, 615, N'Cucharada', N'Ajonjolí blanco, tostado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (616, 143, N'1', 3, 15, 616, N'Cucharada', N'Ajonjolí negro, tostado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (617, 144, N'3', 4, 226, 617, N'Cucharadas', N'Mantequilla sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (618, 144, N'1/4', 19, 55, 618, N'Pieza', N'Cebolla, finamente picada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (619, 144, N'3', 4, 165, 619, N'Cucharadas', N'Harina de trigo')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (620, 144, N'2', 30, 214, 620, N'Tazas', N'Leche descremada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (621, 144, N'1', 22, 250, 621, N'Pizca', N'Nuez moscada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (622, 144, N'2', 20, 84, 622, N'Piezas', N'Clavos de olor')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (623, 144, N'1', 19, 168, 623, N'Pieza', N'Hoja de laurel')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (624, 144, N'1', 22, 321, 624, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (625, 144, N'2', 15, 10, 625, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (626, 144, N'1/2', 3, 319, 626, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (627, 144, N'200', 17, 143, 627, N'Paquete', N'Fettuccine la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (628, 144, N'4', 20, 221, 628, N'Piezas', N'Lonjas de salmón (140g c/u)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (629, 144, N'2', 4, 4, 629, N'Cucharadas', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (630, 144, N'2', 4, 132, 630, N'Cucharadas', N'Eneldo fresco (picado)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (631, 144, N'1', 22, 276, 631, N'Pizca', N'Pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (632, 144, N'1', 3, 226, 632, N'Cucharada', N'Mantequilla sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (633, 144, N'2', 20, 118, 633, N'Piezas', N'Dientes de ajo (finamente picados)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (634, 144, N'1', 19, 284, 634, N'Pieza', N'Pimiento verde (en julianas)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (635, 144, N'1', 19, 33, 635, N'Pieza', N'Calabacita(s), en julianas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (636, 144, N'1', 29, 32, 636, N'Taza', N'Brócoli en ramitos y blanqueado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (637, 144, N'1', 29, 129, 637, N'Taza', N'Ejotes (blanqueados y en mitades)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (638, 144, N'1', 22, 319, 638, N'Pizca', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (639, 144, N'2', 4, 312, 639, N'Cucharadas', N'Queso parmesano (rallado)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (640, 145, N'1', 29, 295, 640, N'Taza', N'Queso cottage')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (641, 145, N'1', 29, 306, 641, N'Taza', N'Queso manchego bajo en grasa, rallado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (642, 145, N'1', 29, 214, 642, N'Taza', N'Leche descremada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (643, 145, N'1', 22, 276, 643, N'Pizca', N'Pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (644, 145, N'1', 3, 357, 644, N'Cucharada', N'Tomillo fresco, limpio')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (645, 145, N'2', 15, 10, 645, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (646, 145, N'1/2', 3, 319, 646, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (647, 145, N'200', 17, 90, 647, N'Paquete', N'Codo no.4 la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (648, 145, N'1', 3, 226, 648, N'Cucharada', N'Mantequilla sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (649, 145, N'2', 30, 32, 649, N'Tazas', N'Brócoli en ramitos y blanqueado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (650, 145, N'2', 19, 325, 650, N'Pieza', N'Salchichas de pavo en rodajas y asadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (651, 145, N'4', 4, 182, 651, N'Cucharadas', N'Hojas de perejil lavadas, desinfectadas y finamente picadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (652, 146, N'2', 15, 10, 652, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (653, 146, N'1/2', 3, 319, 653, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (654, 146, N'200', 17, 143, 654, N'Paquete', N'Fettuccine la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (655, 146, N'1', 3, 3, 655, N'Cucharada', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (656, 146, N'1/2', 19, 55, 656, N'Pieza', N'Cebolla, finamente picada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (657, 146, N'2', 19, 75, 657, N'Pieza', N'Chiles de árbol, frescos en julianas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (658, 146, N'200', 8, 80, 658, N'Gr', N'Chorizo español (en rodajas gruesas)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (659, 146, N'2', 4, 372, 659, N'Cucharadas', N'Vino tinto')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (660, 146, N'2', 30, 136, 660, N'Tazas', N'Espinacas baby')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (661, 146, N'1', 22, 276, 661, N'Pizca', N'Pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (662, 147, N'2', 15, 10, 662, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (663, 147, N'1/2', 3, 319, 663, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (664, 147, N'200', 8, 290, 664, N'Gr', N'Pluma la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (665, 147, N'1', 12, 94, 665, N'Lata', N'Corazones de alcachofa (drenada)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (666, 147, N'2', 30, 351, 666, N'Tazas', N'Tomates cherry (en mitades)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (667, 147, N'1', 19, 284, 667, N'Pieza', N'Pimiento verde (en julianas)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (668, 147, N'1', 29, 310, 668, N'Taza', N'Queso panela (en bastones)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (669, 147, N'6', 4, 4, 669, N'Cucharadas', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (670, 147, N'2', 4, 368, 670, N'Cucharadas', N'Vinagre de jerez')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (671, 147, N'1', 3, 240, 671, N'Cucharada', N'Mostaza dijón')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (672, 147, N'1', 3, 211, 672, N'Cucharada', N'Jugo de naranja')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (673, 147, N'1', 3, 173, 673, N'Cucharada', N'Hojas de albahaca lavadas y desinfectadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (674, 147, N'1', 22, 321, 674, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (675, 127, N'2', 15, 10, 675, N'Lts', N'Agua')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (676, 127, N'1/2', 3, 319, 676, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (677, 127, N'200', 8, 335, 677, N'Gr', N'Spaghetti la moderna')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (678, 127, N'1', 3, 3, 678, N'Cucharada', N'Aceite de canola')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (679, 127, N'2', 20, 35, 679, N'Piezas', N'Calabacitas, en láminas delgadas')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (680, 127, N'1', 22, 321, 680, N'Pizca', N'Sal y pimienta negra molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (681, 127, N'2', 4, 226, 681, N'Cucharadas', N'Mantequilla sin sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (682, 127, N'2', 20, 119, 682, N'Piezas', N'Dientes de ajo (picados)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (683, 127, N'2', 20, 207, 683, N'Piezas', N'Jitomates sin semillas (en cubos medianos)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (684, 127, N'3', 4, 171, 684, N'Cucharadas', N'Hojas de albahaca (lavadas y desinfectadas)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (685, 127, N'4', 4, 312, 685, N'Cucharadas', N'Queso parmesano (rallado)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (686, 148, N'1', 17, 358, 686, N'Paquete', N'Tornillo')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (687, 148, N'1', 12, 26, 687, N'Lata', N'Atún ')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (688, 148, N'200', 16, 96, 688, N'Ml', N'Crema ')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (689, 148, N'1/2', 19, 50, 689, N'Pieza', N'Cebolla')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (690, 148, N'1', 3, 319, 690, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (691, 148, N'1', 3, 4, 691, N'Cucharada', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (692, 149, N'1', 17, 332, 692, N'Paquete', N'Spaghetti 450')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (693, 149, N'2', 4, 4, 693, N'Cucharadas', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (694, 149, N'1/4', 19, 55, 694, N'Pieza', N'Cebolla, finamente picada')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (695, 149, N'3', 6, 99, 695, N'Dientes', N'De ajo finalmente picado')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (696, 149, N'2/3', 29, 103, 696, N'Taza', N'De crema agria ')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (697, 149, N'1 o 2 ', 20, 102, 697, N'Piezas', N'De chiles chipotles adobados de lata o al gusto')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (698, 149, N'1/4 ', 29, 104, 698, N'Taza', N'De queso parmesano')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (699, 149, N'1/2', 29, 101, 699, N'Taza', N'De champiñones rebanados')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (700, 149, N'1', 29, 106, 700, N'Taza', N'De trozos de piña en cubitos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (701, 150, N'2', 17, 146, 701, N'Paquete', N'Fideo 2')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (702, 150, N'1', 11, 203, 702, N'Kg', N'Jitomate guaje')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (703, 150, N'500', 8, 155, 703, N'Gr', N'Frijol negro cocido y molido')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (704, 150, N'1/4', 19, 50, 704, N'Pieza', N'Cebolla')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (705, 150, N'1', 19, 107, 705, N'Pieza', N'Diente de ajo')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (706, 150, N'2', 3, 1, 706, N'Cucharada', N'Aceite')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (707, 151, N'1', 17, 358, 707, N'Paquete', N'Tornillo')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (708, 151, N'1', 17, 56, 708, N'Paquete', N'Champiñon(es)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (709, 151, N'100', 16, 327, 709, N'Ml', N'Salsa de tomate')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (710, 151, N'1', 3, 319, 710, N'Cucharada', N'Sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (711, 151, N'1', 19, 31, 711, N'Pieza', N'Brocoli')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (712, 151, N'1', 19, 8, 712, N'Pieza', N'Acelgas en trozos')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (713, 151, N'1', 19, 344, 713, N'Pieza', N'Tomate')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (714, 151, N'100', 8, 236, 714, N'Gr', N'Milanesa de pollo')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (715, 151, N'1', 3, 4, 715, N'Cucharada', N'Aceite de oliva')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (716, 151, N'1', 23, 180, 716, N'Puñito', N'Hojas de laurel')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (717, 152, N'1', 17, 332, 717, N'Paquete', N'Spaghetti 450')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (718, 152, N'1', 1, 327, 718, N'Bote', N'Salsa de tomate')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (719, 152, N'1', 3, 100, 719, N'Cucharada', N'De albaca fresca')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (720, 152, N'500', 8, 48, 720, N'Gr', N'Carne molida')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (721, 152, N'1', 21, 105, 721, N'Pisca', N'De sal')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (722, 153, N'500', 8, 331, 722, N'Gr', N'Spaghetti')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (723, 153, N'4', 30, 31, 723, N'Tazas', N'Brocoli')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (724, 153, N'2', 6, 13, 724, N'Dientes', N'Ajo')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (725, 153, N'1/2', 29, 9, 725, N'Taza', N'Aderezo italiano')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (726, 153, N'250', 8, 45, 726, N'Gr', N'Camarón(es)')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (727, 153, N'1/4', 29, 311, 727, N'Taza', N'Queso parmesano')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (728, 154, N'1', 17, 216, 728, N'Paquete', N'Letra')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (729, 154, N'1', 19, 344, 729, N'Pieza', N'Tomate')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (730, 154, N'3', 20, 13, 730, N'Piezas', N'Ajo')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (731, 155, N'1', 17, 95, 731, N'Paquete', N'Corona')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (732, 155, N'2', 20, 344, 732, N'Piezas', N'Tomate')
GO
INSERT [dbo].[RecipeIngredient] ([Id], [RecipeId], [Quantity], [UnitId], [IngredientId], [Order], [Unit], [Description]) VALUES (733, 94, N'1/4', 12, 26, 733, N'Lata', N'Atún ')
GO
SET IDENTITY_INSERT [dbo].[RecipeIngredient] OFF
GO
SET IDENTITY_INSERT [dbo].[AppUser] ON 
GO
INSERT [dbo].[AppUser] ([Id], [FirstName], [LastName], [Email], [Ocupation], [EstateId], [BirthDate], [Gender], [OauthToken], [AuthenticationType], [Password], [AvatarUrl], [FacebookId]) VALUES (1, N'Miguel', N'Miguel', N'miguelcenteno@hotmail.com', N'Humilde Carpintero', 7, CAST(N'1982-10-10' AS Date), 1, N'EAADgj8oazg8BAJmcuH82WiJqrzZAIZC1HTAR1rZCXRyaJRcYbLewKsAVRgx0jyqWbGew4COSVT4mgZB5llp4ZBcp9mmhPI4lzLzu0MJrZBTKUbaEOz8ugSWjNZCQSp5syEibDsw1ndocZB2k6l9Pkf3RdISIZBHjHfhw8WC5bODshSAZDZD', 2, NULL, N'https://graph.facebook.com/10154890765365752/picture?type=large', N'10154890765365752')
GO
SET IDENTITY_INSERT [dbo].[AppUser] OFF
GO
SET IDENTITY_INSERT [dbo].[Comment] ON 
GO
INSERT [dbo].[Comment] ([Id], [RecipeId], [AppUserId], [Message], [Date], [CommenParentId]) VALUES (1, 99, 1, N'cggdsadasdasdasd', CAST(N'2018-10-11T00:41:34.333' AS DateTime), 0)
GO
INSERT [dbo].[Comment] ([Id], [RecipeId], [AppUserId], [Message], [Date], [CommenParentId]) VALUES (2, 99, 1, N'werewrewrewrewrwer', CAST(N'2018-10-11T00:41:44.623' AS DateTime), 0)
GO
INSERT [dbo].[Comment] ([Id], [RecipeId], [AppUserId], [Message], [Date], [CommenParentId]) VALUES (3, 99, 1, N'fffertwewrwerwerwrew', CAST(N'2018-10-11T00:47:44.960' AS DateTime), 2)
GO
SET IDENTITY_INSERT [dbo].[Comment] OFF
GO
INSERT [dbo].[CommentLike] ([CommentId], [AppUserId], [IsLike]) VALUES (3, 1, 1)
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (1, N'Aguascalientes')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (2, N'Baja California')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (3, N'Baja California Sur')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (4, N'Campeche')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (5, N'Chiapas')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (6, N'Chihuahua')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (7, N'Ciudad de México')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (8, N'Coahuila de Zaragoza')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (9, N'Colima')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (10, N'Durango')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (11, N'Estado de México')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (12, N'Guanajuato')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (13, N'Guerrero')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (14, N'Hidalgo')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (15, N'Jalisco')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (16, N'Michoacán de Ocampo')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (17, N'Morelos')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (18, N'Nayarit')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (19, N'Nuevo León')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (20, N'Oaxaca')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (21, N'Puebla')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (22, N'Querétaro')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (23, N'Quintana Roo')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (24, N'San Luis Potosí')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (25, N'Sin Localidad')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (26, N'Sinaloa')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (27, N'Sonora')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (28, N'Tabasco')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (29, N'Tamaulipas')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (30, N'Tlaxcala')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (31, N'Veracruz de Ignacio de la Llave')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (32, N'Yucatán')
GO
INSERT [dbo].[Estates] ([Id], [Name]) VALUES (33, N'Zacatecas')
GO
SET IDENTITY_INSERT [dbo].[Level] ON 
GO
INSERT [dbo].[Level] ([Id], [Level]) VALUES (1, N'Fácil')
GO
INSERT [dbo].[Level] ([Id], [Level]) VALUES (2, N'Medio')
GO
INSERT [dbo].[Level] ([Id], [Level]) VALUES (3, N'Dificil')
GO
SET IDENTITY_INSERT [dbo].[Level] OFF
GO
SET IDENTITY_INSERT [dbo].[RecipeCookStep] ON 
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (1, 94, 1, N'Calienta el agua junto con la sal en una olla alta hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (2, 94, 2, N'Inmediatamente agrega el Tornillo La Moderna y cocínalo hasta que esté en su punto (suave por fuera y firme por dentro); revisa los tiempos de cocción marcados en el empaque')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (3, 94, 3, N'Escurre y r')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (4, 95, 1, N'Para la vin')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (5, 96, 1, N'Para la salsa Calienta el aceite en una olla a fuego medio y agrega ajo y chile de árbol')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (6, 96, 2, N'Mueve constantemente y permite que el ajo dore ligeramente')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (7, 96, 3, N'Incorpora el orégano junto con la cebolla y mueve regularmente hasta que la cebolla se torne traslúcida')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (8, 97, 1, N'Para La Sopa Calienta en una olla el agua con la sal hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (9, 97, 2, N'Inmediatamente agrega la pasta Codo No')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (10, 97, 3, N'3 La Moderna® y cocina hasta que esté en su punto; revisa los tiempos de cocción marcados en el empaque')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (11, 97, 4, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (12, 97, 5, N'Combina en un tazón a')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (13, 98, 1, N'Pon a hervir el agua con sal en una olla alta')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (14, 98, 2, N'Inmediatamente agrega la Pluma La Moderna® y cocina hasta que llegue casi a su punto')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (15, 98, 3, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (16, 98, 4, N'Vierte el aceite en un sartén caliente y dora ligeramente el echalote')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (17, 98, 5, N'Añade los hongos, aumenta el sab')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (18, 99, 1, N'Calienta en el agua junto con la sal en una olla alta hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (19, 99, 2, N'Inmediatamente agrega la pasta Tornillo La Moderna® y cocina hasta que esté en su punto')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (20, 99, 3, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (21, 99, 4, N'Calienta la crema junto con la leche en un sartén semiprofundo hasta que hi')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (22, 100, 1, N'Calienta el agua junto con la sal en una olla alta hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (23, 100, 2, N'Inmediatamente agrega la pasta Tornillo con Fibra La Moderna® y cocina hasta que esté en su punto (suave por fuera y firme por dentro)')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (24, 100, 3, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (25, 100, 4, N'Calienta el aceite en un sartén')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (26, 101, 1, N'Para La Salsa Precalienta el horno a 180ºC')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (27, 101, 2, N'Coloca la coliflor, los echalotes y el ajo en un tazón')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (28, 101, 3, N'Agrega el aceite de canola, sal y pimienta, y revuelve bien para combinar todos los sabores')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (29, 101, 4, N'Pasa los vegetales a una charola para horno, dejándolos en una')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (30, 102, 1, N'Calienta en una olla el caldo, agrega los vegetales y los garbanzos y hierve por 15 minutos a fuego bajo, para que los garbanzos suavicen')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (31, 102, 2, N'Añade los Dinosaurios La Moderna a la sopa y cocina por 8 minutos a fuego medio')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (32, 102, 3, N'Incorpora la carne junto con el que')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (33, 103, 1, N'Para la salsa Calienta en una olla el agua con la sal hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (34, 103, 2, N'Inmediatamente agrega la pasta de Inmediatamente agrega la pasta de revisa los tiempos de cocción marcados en el empaque')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (35, 103, 3, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (36, 103, 4, N'Combina en un tazón aceite de canola, chil')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (37, 104, 1, N'Para las croquetas')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (38, 104, 2, N'Precalienta tu horno a 180ºC')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (39, 104, 3, N'Calienta en una olla el agua con la sal hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (40, 104, 4, N'Inmediatamente agrega la pasta Munición La Moderna® revisa los tiempos de cocción marcados en el empaque')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (41, 104, 5, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (42, 104, 6, N'Mezcla en un tazón la')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (43, 105, 1, N'Calienta una olla, vierte el aceite y cocina la pasta de Dinosaurios La Moderna® Kids!, moviendo constantemente, hasta que esté ligeramente dorada')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (44, 105, 2, N'Licúa los jitomates con un poco del caldo de pollo, cuela, vierte en la olla y deja hervir')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (45, 105, 3, N'Agrega el caldo')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (46, 106, 1, N'Calienta el agua junto con la sal en una olla alta hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (47, 106, 2, N'Inmediatamente agrega el Codo No')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (48, 106, 3, N'1 La Moderna® y cocina hasta que esté en su punto (suave por fuera y firme por dentro); revisa los tiempos de cocción marcados en el empaque')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (49, 106, 4, N'Escurre y r')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (50, 107, 1, N'Calienta el aceite por un minuto en una olla a fuego alto')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (51, 107, 2, N'Agrega el ajo y la cebolla y mueve constantemente hasta que la cebolla tome un tono traslúcido')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (52, 107, 3, N'Incorpora los granos de elote y permite que se tornen de color dorado')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (53, 107, 4, N'Añade la zanahoria y la papa,')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (54, 108, 1, N'Calienta en una olla el agua con la sal hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (55, 108, 2, N'Inmediatamente agrega el Codo No')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (56, 108, 3, N'1 La Moderna® y cocina hasta que esté en su punto; revisa los tiempos de cocción marcados en el empaque')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (57, 108, 4, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (58, 108, 5, N'Integra en un tazón la mitad de la pasta')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (59, 109, 1, N'Calienta el agua junto con la sal en una olla alta hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (60, 109, 2, N'Inmediatamente agrega la Pasta')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (61, 109, 3, N'Caracol La Moderna® y cocínala hasta que quede en su punto (suave por fuera y firme por dentro)')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (62, 109, 4, N'Revisa los tiempos de cocción marcados en el empaque; esc')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (63, 110, 1, N'Calienta en una olla el agua con la sal hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (64, 110, 2, N'Inmediatamente agrega la pasta Animales de la Selva La Moderna® Kids! y cocina hasta que estén en su punto')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (65, 110, 3, N'Escurre la pasta, pásala a un tazón y combínala junto con el jamón, el cilantro, la cebol')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (66, 111, 1, N'Para los chips')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (67, 111, 2, N'Precalienta tu horno a 160°C')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (68, 111, 3, N'Coloca las rebanadas de plátano sobre una charola, rocía con el aceite en aerosol y espolvorea con tomillo')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (69, 111, 4, N'Hornea por 15 minutos a 160°C o hasta que estén ligeramente dorados y crujientes')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (70, 111, 5, N'Para el fideo seco')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (71, 112, 1, N'Para la sopa')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (72, 112, 2, N'Funde la mantequilla en una olla y dora y acitrona ligeramente el ajo y la cebolla')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (73, 112, 3, N'Incorpora las calabazas y cocínalas hasta que tomen un ligero tono dorado')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (74, 112, 4, N'Retira de la olla y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (75, 112, 5, N'Agrega el Fideo No')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (76, 112, 6, N'1 La Moderna® a la olla y tuesta l')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (77, 113, 1, N'Para La Salsa Coloca todos los ingredientes dentro del vaso de tu licuadora y procesa hasta lograr una salsa homogénea y suave; reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (78, 113, 2, N'Para El Fideo Calienta el aceite en una olla a fuego medio, agrega el Fideo No')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (79, 113, 3, N'1 La Moderna® y cocina moviendo ocasion')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (80, 114, 1, N'Calienta el agua junto con la sal en una olla alta hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (81, 114, 2, N'Inmediatamente agrega el Moño La Moderna y cocínalo hasta que esté en su punto (suave por fuera y firme por dentro); revisa los tiempos de cocción marcados en el empaque')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (82, 114, 3, N'Escurre, reserv')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (83, 115, 1, N'Precalienta tu horno a 180 °C')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (84, 115, 2, N'Calienta el agua junto con la sal en una olla alta hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (85, 115, 3, N'Inmediatamente agrega el Espiral La Moderna® y cocinalo hasta que esté en su punto (suave por fuera y firme por dentro); revisa los tiempos de cocción marc')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (86, 116, 1, N'Para La Pasta Calienta el agua junto con la sal en una olla alta hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (87, 116, 2, N'Inmediatamente agrega el Macarrón Corto La Moderna® y cocínalo hasta que esté en su punto (suave por fuera y firme por dentro); revisa los tiempos de cocción marcados en el')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (88, 117, 1, N'Calienta el agua con la sal hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (89, 117, 2, N'Inmediatamente agrega la pasta Moño La Moderna®, revisa los tiempos de cocción marcados en el empaque')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (90, 117, 3, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (91, 117, 4, N'Calienta el aceite en un sartén y saltea la cebolla y el ajo hasta que suavicen')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (92, 117, 5, N'Integr')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (93, 118, 1, N'Para la salsaAsa los vegetales y tuesta las nueces en un comal')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (94, 118, 2, N'Pasa al vaso de tu licuadora e integra junto con el resto de los ingredientes')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (95, 118, 3, N'Vacía la salsa en el sartén en donde sellaste las pechugas y cocina por 5 minutos a fuego bajo')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (96, 118, 4, N'Reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (97, 118, 5, N'Pa')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (98, 119, 1, N'Calienta el agua con la sal hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (99, 119, 2, N'Inmediatamente agrega la pasta Moño La Moderna® suave por fuera y firme por dentro')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (100, 119, 3, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (101, 119, 4, N'Calienta el aceite en un sartén y saltea la cebolla y el ajo hasta que suavicen')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (102, 119, 5, N'Integra la arúgula y')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (103, 120, 1, N'Calienta en una olla el agua con la sal hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (104, 120, 2, N'Agrega el Fettuccine La Moderna® y cocina hasta que esté en su punto')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (105, 120, 3, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (106, 120, 4, N'Licúa la crema con los quesos y lleva a un sartén a fuego medio')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (107, 120, 5, N'Agrega los ajos, la cebolla y la ralladu')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (108, 121, 1, N'Calienta en una olla el agua con la sal hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (109, 121, 2, N'Inmediatamente agrega el Spaghetti La Moderna® y cocina hasta que esté en su punto')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (110, 121, 3, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (111, 121, 4, N'Calienta el aceite en un sartén y suaviza los ajos sin dejar que cambien de color')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (112, 121, 5, N'Incorpor')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (113, 122, 1, N'Para la salsaLleva al fuego todos los ingredientes, cocina por 5 minutos sin dejar de mover o hasta obtener una salsa tersa; reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (114, 122, 2, N'Para la pastaCalienta en una olla el agua con la sal hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (115, 122, 3, N'Inmediatamente agrega los Moños La Moderna®')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (116, 123, 1, N'Para la salsaLicúa todos los ingredientes de la salsa hasta integrar y obtener una salsa lisa')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (117, 123, 2, N'Vacía la salsa en un sartén y calienta a fuego lento hasta espesar ligeramente')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (118, 123, 3, N'Para la pastaPrecalienta tu horno a 180ºC')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (119, 123, 4, N'Calienta el agua junto con')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (120, 124, 1, N'Calienta en una olla el agua con la sal hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (121, 124, 2, N'Inmediatamente agrega el Codo No')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (122, 124, 3, N'4 La Moderna® y cocina hasta que esté en su punto')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (123, 124, 4, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (124, 124, 5, N'Fríe el tocino en un sartén caliente, añade las coles y saltea por un par de minutos o hasta q')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (125, 125, 1, N'Para La Salsa Coloca todos los ingredientes en tu licuadora o procesador de alimentos y mezcla hasta lograr una salsa de textura suave y homogénea; reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (126, 125, 2, N'Para La Pasta Calienta el agua junto con la sal en una olla alta hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (127, 125, 3, N'Inmediatamente')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (128, 126, 1, N'Para la vinagreta Mezcla todos los ingredientes dentro de un frasco de vidrio con tapa y agita vigorosamente hasta formar una emulsión')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (129, 126, 2, N'Reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (130, 126, 3, N'Para la pasta Calienta en una olla el agua con la sal hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (131, 126, 4, N'Inmediatamente agrega los Macarrones L')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (132, 127, 1, N'Calienta en una olla el agua con la sal hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (133, 127, 2, N'Inmediatamente agrega el Spaghetti La Moderna® y cocina hasta que quede suave por fuera y firme por dentro')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (134, 127, 3, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (135, 127, 4, N'Calienta el aceite en un sartén')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (136, 127, 5, N'Coloca algunas láminas de calabaza de')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (137, 128, 1, N'Calienta el agua junto con la sal en una olla alta hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (138, 128, 2, N'Inmediatamente agrega Spaghetti La Moderna® y cocina hasta que esté en su punto (suave por fuera y firme por dentro); revisa los tiempos de cocción marcados en el empaque')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (139, 128, 3, N'Escurre y rese')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (140, 129, 1, N'Precalienta tu horno a 200°C')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (141, 129, 2, N'Hierve el agua con la sal en una olla alta, agrega el Macarrón Largo La Moderna® y cocina un minuto antes de lo que marca el empaque')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (142, 129, 3, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (143, 129, 4, N'Calienta el aceite en un sartén grande para horno y suda el ajo junto co')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (144, 130, 1, N'Calienta el agua junto con la sal en una olla alta hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (145, 130, 2, N'Inmediatamente agrega el Spaghetti La Moderna® y revisa los tiempos de cocción marcados en el empaque')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (146, 130, 3, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (147, 130, 4, N'Calienta el aceite en un sartén y funde la mantequilla, agrega l')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (148, 131, 1, N'Para la salsa Calienta el aceite en un sartén y suda el ajo con la cebolla hasta que empiecen a cambiar de color')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (149, 131, 2, N'Agrega el jitomate y cocina a fuego alto hasta suavizar; mueve constantemente para que no se peguen')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (150, 131, 3, N'Integra puré de tomate, chile, sal, orég')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (151, 132, 1, N'Para la salsa Licúa todos los ingredientes hasta incorporar y obtener una salsa suave y homogénea; reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (152, 132, 2, N'Para la pasta Calienta el agua junto con la sal en una olla alta hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (153, 132, 3, N'Inmediatamente agrega el Spaghetti La Moderna® y cocínalo hasta')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (154, 133, 1, N'Calienta el agua junto con la sal en una olla alta hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (155, 133, 2, N'Inmediatamente agrega Tornillos La Moderna® y cocínalos hasta que queden en su punto (suaves por fuera y firmes por dentro)')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (156, 133, 3, N'Revisa los tiempos de cocción marcados en el empaque')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (157, 133, 4, N'Agrega')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (158, 134, 1, N'Pon a hervir el agua con sal y agrega el Spaghetti La Moderna® cocina casi hasta que esté en su punto según el tiempo de cocción que diga el empaque')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (159, 134, 2, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (160, 134, 3, N'Calienta el aceite en un sartén y dora ligeramente el ajo')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (161, 134, 4, N'Incorpora los chiles y la c')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (162, 135, 1, N'Hierve el agua con sal en una olla alta')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (163, 135, 2, N'Inmediatamente agrega el Spaghetti La Moderna® y cocina hasta que esté suave por fuera y firme por dentro')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (164, 135, 3, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (165, 135, 4, N'Vierte el aceite en un sartén caliente y dora el ajo')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (166, 135, 5, N'Agrega la pasta y las aceitunas; m')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (167, 136, 1, N'Para la salsa Coloca la mitad de las espinacas y el resto de los ingredientes dentro de tu procesador de alimentos y procesa hasta lograr una salsa suave y homogénea')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (168, 136, 2, N'De ser necesario agrega un par de cucharadas de agua')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (169, 136, 3, N'Para la pasta Calienta el agua jun')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (170, 137, 1, N'Para la salsa Calienta el aceite de canola en un sartén y suda el ajo y la cebolla hasta que esta última tome un tono traslúcido')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (171, 137, 2, N'Incorpora los tallos de cilantro junto con los chiles y saltea por un minuto')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (172, 137, 3, N'Añade el agua y la crema al sartén, revuelve bi')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (173, 138, 1, N'Para el pollo Combina en un tazón el aceite junto con té limón, los echalotes y pimienta; barniza las pechugas de pollo con esta preparación y cocínalo en un sartén bien caliente hasta que tome un tono dorado y quede bien cocinado al centro; unos por cinc')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (174, 139, 1, N'Para la ensalada Calienta el agua junto con la sal en una olla alta hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (175, 139, 2, N'Inmediatamente agrega la Pluma La Moderna® y cocina hasta que esté en su punto (suave por fuera y firme por dentro)')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (176, 139, 3, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (177, 139, 4, N'Tuesta en un sartén las semillas')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (178, 140, 1, N'Para la salsa Calienta el aceite de canola en un sartén, dora ligeramente las láminas de ajo y acitrona la cebolla')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (179, 140, 2, N'Añade el chile y mezcla con todos los ingredientes; deja por 2 minutos y aumenta el sabor con sal y pimienta')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (180, 140, 3, N'Pasa a tu licuadora e integra')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (181, 141, 1, N'Para la salsa Funde la mantequilla en un sartén y cocina cebolla, ajo y chile de árbol, hasta que la cebolla tome un tono traslúcido')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (182, 141, 2, N'Agrega los champiñones y el tomillo; aumenta el sabor con sal y pimienta y cocina hasta evaporar el líquido de los hongos')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (183, 142, 1, N'Calienta en una olla el agua con la sal hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (184, 142, 2, N'Inmediatamente agrega el Moño La Moderna® y cocina hasta que esté en su punto (suave por fuera y firme por dentro); revisa los tiempos de cocción marcados en el empaque')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (185, 142, 3, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (186, 142, 4, N'Calienta')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (187, 143, 1, N'Calienta en una olla el agua con la sal hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (188, 143, 2, N'Inmediatamente agrega el Fettuccine La Moderna® revisa los tiempos de cocción marcados en el empaque')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (189, 143, 3, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (190, 143, 4, N'Calienta un sartén, vierte el aceite de ajonjolí y saltea los vegetales por')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (191, 144, 1, N'Para la salsa Funde la mantequilla en una olla, agrega y cocina la cebolla hasta que esté traslúcida')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (192, 144, 2, N'Añade la harina y mueve constantemente hasta que se dore ligeramente; incorpora la leche y mezcla con un batidor de globo para evitar grumos')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (193, 144, 3, N'Por último,')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (194, 145, 1, N'Para las croquetas Incorpora y licúa todos los ingredientes hasta obtener una salsa tersa y homogénea; reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (195, 145, 2, N'Para el dip de mango Precalienta tu horno a 180°C')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (196, 145, 3, N'Calienta en una olla el agua con sal hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (197, 145, 4, N'Inmediatamente agrega la pasta Codo N')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (198, 146, 1, N'Calienta en una olla el agua con la sal hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (199, 146, 2, N'Inmediatamente agrega el Fettuccine La Moderna® y cocina según el tiempo de cocción marcado en el empaque')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (200, 146, 3, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (201, 146, 4, N'Calienta el aceite en un sartén y saltea la cebolla y el chile')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (202, 146, 5, N'Incorpo')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (203, 147, 1, N'Para la ensalada Calienta agua junto con la sal en una olla hasta que hierva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (204, 147, 2, N'Agrega la pasta Pluma La Moderna® y cocina hasta que esté suave por fuera y firme por dentro')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (205, 147, 3, N'Escurre y reserva')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (206, 147, 4, N'Corta los corazones de alcachofa en cuartos y colócalos en un ta')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (207, 148, 1, N'1')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (208, 148, 2, N'- Hervir el Rotini DeLuigi según instrucciones del empaque, con 1 cucharada de sal')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (209, 148, 3, N'2')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (210, 148, 4, N'- Ya hervida la pasta, poner a escurrir 3')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (211, 148, 5, N'- En una cacerola poner 1 cucharada de aceite de olivo y acitronar 1/2 cebolla finamente picada')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (212, 148, 6, N'4')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (213, 148, 7, N'- Agregar 1 Lat')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (214, 149, 1, N'1')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (215, 149, 2, N'Cuece la pasta en abundante agua hirviendo con sal y un trocito de cebolla hasta que esté al dente')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (216, 149, 3, N'2')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (217, 149, 4, N'En una olla calienta el aceite y fríe la cebolla hasta que esté transparente')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (218, 149, 5, N'3')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (219, 149, 6, N'Agrega el ajo y fríe por unos minutos, añade los champiñones y coc')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (220, 150, 1, N'Se viven los jitomates en agua hirviendo ya que reventaron se apagan y se muelen con la cebolla y el ajo')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (221, 150, 2, N'Se muelen los frijoles En un sartén se pone el aceite se fríe el fideo ya que está transparente se le vacía el jitomate molido y se sazona con sal')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (222, 151, 1, N'primero se pone a hervir el agua con las hojas de laurel y una cuch de aceite de oliva en otro sarten se pone los champiñones con las acelgas el broccoli despues el pollo se mezclan todos los ingredientes y se vierte la salsa de tomate')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (223, 151, 2, N'se sirve')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (224, 152, 1, N'Poner tu pasta a cocer en lo que esta coce tu carne y poner sal, vaciarle la salsa de tomate la albacar cocinar')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (225, 152, 2, N'poner en un plato la pasta cocida y encima poner la carne')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (226, 152, 3, N'acompañar con pan 30')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (227, 153, 1, N'Cuece el espagueti en una cacerola grande según se indica en el paquete, pero sin sal')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (228, 153, 2, N'Agrega el brócoli al agua 2 min')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (229, 153, 3, N'antes de terminada la cocción')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (230, 153, 4, N'Entretanto, calienta el aderezo en una sartén grande a fuego medio-alto')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (231, 153, 5, N'Agrega los camarones con el')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (232, 154, 1, N'holaz')
GO
INSERT [dbo].[RecipeCookStep] ([Id], [RecipeId], [Order], [Description]) VALUES (233, 155, 1, N'se prepara muy fácil')
GO
SET IDENTITY_INSERT [dbo].[RecipeCookStep] OFF
GO
INSERT [dbo].[RecipeFavourite] ([RecipeId], [AppUserId]) VALUES (99, 1)
GO
INSERT [dbo].[RecipeLike] ([RecipeId], [AppUserId], [IsLike]) VALUES (99, 1, 1)
GO
INSERT [dbo].[RecipeScore] ([RecipeId], [AppUserId], [Score]) VALUES (99, 1, 5.0000)
GO
