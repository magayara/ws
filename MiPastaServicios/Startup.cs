﻿namespace MiPastaServicios
{
    using System.Linq;
    using System.Net.Http.Formatting;
    using System.Net.Http.Headers;
    using System.Web.Http;
    using System.Web.Mvc;
    using Owin;
    using System.Web.Http.Cors;
    using System.Web.Routing;
    using Newtonsoft.Json.Serialization;
    using MiPastaServicios.Data.Converters;
    using MiPastaServicios.Util;
    using System.Text;

    /// <summary>
    ///     Permite iniciar la aplicación Web.
    /// </summary>
    public class Startup
    {

        /// <summary>
        /// Identificador público de cliente.
        /// </summary>
        public static string PublicClientId { get; private set; }

        /// <summary>
        /// Permite configurar el Api.
        /// </summary>
        /// <param name="app">Intanciá de consttructor de aplicación.</param>
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(config);
            RegisterWebApi(config);

            Encoding oldDefault = GlobalConfiguration.Configuration.Formatters.JsonFormatter.SupportedEncodings[0];
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SupportedEncodings.Add(oldDefault);
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SupportedEncodings.RemoveAt(0);

            RegisterFormatters(config);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);
        }

        /// <summary>
        /// Registra las configuraciones generales de la aplicación.
        /// </summary>
        /// <param name="config">Configuración de la aplicación.</param>
        public static void RegisterWebApi(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();
            config.EnableCors(new EnableCorsAttribute("*", "*", "*"));
            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });
            RouteTable.Routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            RouteTable.Routes.MapRoute("Default", "{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional });
            config.Filters.Add(new GlobalExceptionFilter());
            GlobalFilters.Filters.Add(new HandleErrorAttribute());
            config.SuppressDefaultHostAuthentication();
            config.EnableCors(new EnableCorsAttribute("*", "*", "GET, POST, OPTIONS, PUT, DELETE"));
        }

        /// <summary>
        /// Regostra los serializadores y deserializadores de la aplicación.
        /// </summary>
        /// <param name="config">Configuración de la aplicación.</param>
        public static void RegisterFormatters(HttpConfiguration config)
        {
            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            config.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
            config.Formatters.Remove(jsonFormatter);
            config.Formatters.Insert(0, jsonFormatter);
            config.Formatters[0].SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

            var settings = (Newtonsoft.Json.JsonSerializerSettings)jsonFormatter.SerializerSettings;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            settings.Converters.Add(new CustomDateTimeConverter());
        }

    }
}