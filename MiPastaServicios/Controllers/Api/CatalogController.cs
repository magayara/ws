﻿using MiPastaServicios.Models.Profile;
using MiPastaServicios.Models.Recipes;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace MiPastaServicios.Controllers.Api
{
    [RoutePrefix("api/catalog")]
    public class CatalogController : ApiBase
    {
        [HttpGet, Route("estates")]
        [ResponseType(typeof(List<Estate>))]
        public HttpResponseMessage GetEstates()
        {
            return Request.CreateResponse(HttpStatusCode.OK, Estate.GetEstates());
        }

        [HttpGet, Route("units")]
        [ResponseType(typeof(List<IngredientUnit>))]
        public HttpResponseMessage GetIngredientUnits([FromUri] string query)
        {
            return Request.CreateResponse(HttpStatusCode.OK, IngredientUnit.GetIngredientUnits(query));
        }

        [HttpGet, Route("pastas")]
        [ResponseType(typeof(List<IngredientUnit>))]
        public HttpResponseMessage GetIngredientPastas()
        {
            return Request.CreateResponse(HttpStatusCode.OK, IngredientPasta.GetIngredientPastas());
        }

        [HttpGet, Route("ingredients")]
        [ResponseType(typeof(List<Ingredient>))]
        public HttpResponseMessage GetIngredients([FromUri] string query)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Ingredient.GetIngredients(query));
        }

        [HttpGet, Route("product")]
        [ResponseType(typeof(List<Ingredient>))]
        public HttpResponseMessage GetProduct([FromUri] string barcode)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Product.GetProduct(barcode));
        }
    }
}
