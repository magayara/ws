﻿using MiPastaServicios.Models.Profile;
using MiPastaServicios.Util;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace MiPastaServicios.Controllers.Api
{
    [RoutePrefix("api/user")]
    public class UserController : ApiBase
    {
        [HttpGet, Route("get/by-email")]
        [ResponseType(typeof(User))]
        public HttpResponseMessage GetByemail([FromUri] string email)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Models.Profile.User.Get(email));
        }

        [HttpGet, Route("get/by-credential")]
        [ResponseType(typeof(User))]
        public HttpResponseMessage GetByCredential([FromUri] string email, [FromUri] string password)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Models.Profile.User.GetByCredential(email, password));
        }

        [HttpGet, Route("get/{userId:long}")]
        [ResponseType(typeof(User))]
        public HttpResponseMessage Get(long userId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Models.Profile.User.Get(userId));
        }

        [HttpGet, Route("get/statistics/{userId:long}")]
        [ResponseType(typeof(UserStatistic))]
        public HttpResponseMessage GetStatistic(long userId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, UserStatistic.Get(userId));
        }

        [HttpPost, Route("follow/{followerId:long}/{userId:long}")]
        [ResponseType(typeof(UserStatistic))]
        public HttpResponseMessage Follow(long followerId, long userId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Models.Profile.User.Follow(followerId, userId));
        }

        [HttpPost, Route("stop-follow/{followerId:long}/{userId:long}")]
        [ResponseType(typeof(UserStatistic))]
        public HttpResponseMessage StopFollow(long followerId, long userId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Models.Profile.User.StopFollow(followerId, userId));
        }

        [HttpPost, Route("save")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage Save()
        {
            var model = DeserializaPeticion<User>();

            if (model.AvatarBase64 != null) {
                model.Avatar = new Models.FileModel {
                   FileContent = ImageUtil.ImageFromBase64(model.AvatarBase64)
                };

                if (model.Avatar.FileContent == null) {
                    model.Avatar = null;
                }
            }
            
            return Request.CreateResponse(HttpStatusCode.OK, Models.Profile.User.Save(model));
        }
    }
}
