﻿using MiPastaServicios.Models.Recipes;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace MiPastaServicios.Controllers.Api
{
    [RoutePrefix("api/comment")]
    public class CommentController : ApiBase
    {
        [HttpGet, Route("get/by-user/{userId:long}/{pageNumber:int}/{itemsPerPage:int}")]
        [ResponseType(typeof(List<Comment>))]
        public HttpResponseMessage GetByUser(long userId, int pageNumber, int itemsPerPage)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Comment.GetByUser(userId, pageNumber, itemsPerPage));
        }

        [HttpGet, Route("get/by-recipe/{recipeId:long}/{userId:long}/{pageNumber:int}/{itemsPerPage:int}")]
        [ResponseType(typeof(List<Comment>))]
        public HttpResponseMessage GetByRecipe(long recipeId, long userId, int pageNumber, int itemsPerPage)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Comment.GetByRecipe(recipeId, userId, pageNumber, itemsPerPage));
        }

        [HttpPost, Route("set-like/{userid:long}/{commentId:long}/{isLike:bool}")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage SetLike(long userId, long commentId, bool isLike)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Comment.SetLike(userId, commentId, isLike));
        }

        [HttpPost, Route("set-as-viewed/{commentId:long}")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage SetAsViewed(long commentId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Comment.SetAsViewed(commentId));
        }

        [HttpPost, Route("delete/{commentId:long}")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage Delete(long commentId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Comment.Delete(commentId));
        }

        [HttpGet, Route("get/new-comment-count/{userId:long}")]
        [ResponseType(typeof(Comment))]
        public HttpResponseMessage GetNewCommentCount(long userId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Comment.GetNewCommentCount(userId));
        }
        

        [HttpPost, Route("save")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage Save()
        {
            var comment = DeserializaPeticion<Comment>();

            return Request.CreateResponse(HttpStatusCode.OK, Comment.Save(comment));
        }

        [HttpPost, Route("save-contact")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage SaveContactComment()
        {
            var comment = DeserializaPeticion<Comment>();

            return Request.CreateResponse(HttpStatusCode.OK, Comment.SaveContactComment(comment));
        }
        [HttpPost, Route("send-message")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage SendMessage()
        {
            var model = DeserializaPeticion<Comment>();
            return Request.CreateResponse(HttpStatusCode.OK, Comment.SendMessage(model.Message));
        }


        [HttpPost, Route("send-message-users")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage SendMessageUsers()
        {
            var model = DeserializaPeticion<Comment>();
            return Request.CreateResponse(HttpStatusCode.OK, Comment.SendMessageUsers(model.Message, model.IdUsers));
        }

    }
}
