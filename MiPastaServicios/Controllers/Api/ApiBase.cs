﻿using MiPastaServicios.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace MiPastaServicios.Controllers.Api
{
    /// <summary>
    ///     Controlador base de API. Define métodos genéricos y utilitarios para los controladores API
    ///     de la aplicación
    /// </summary>
    public class ApiBase : ApiController
    {
        /// <summary>
        ///  Define pararámetro de serialización de forma.
        /// </summary>
        private const string JsonSerializedForm = "JSON";

        private static JsonSerializerSettings _jsonSettings;

        /// <summary>
        ///     Ruta de almacenamiento temporal de archivos adjuntos.
        /// </summary>
        public const string UploadDirectory = "~/App_Data/uploads/";

        /// <summary>
        /// Cantidad de Bytes de un KiloByte.
        /// </summary>
        private const int BytesOfKilobyte = 1024;

        private static JsonSerializerSettings JsonSettings
        {
            get
            {
                if (_jsonSettings == null)
                {
                    _jsonSettings = new JsonSerializerSettings
                    {
                        DefaultValueHandling = DefaultValueHandling.Ignore,
                        NullValueHandling = NullValueHandling.Ignore,
                        MissingMemberHandling = MissingMemberHandling.Ignore,
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    };
                    _jsonSettings.Converters.Add(new IsoDateTimeConverter());
                }

                return _jsonSettings;
            }
        }

        /// <summary>
        ///     Permite deserializar peticiones, serializadas en <see cref="string" />.
        /// </summary>
        /// <typeparam name="T">Tipo de objeto a deserializar.</typeparam>
        /// <returns>Instancia deserializada.</returns>
        protected T DeserializaPeticion<T>() where T : class, new()
        {
            if (ActionContext.Request.Content.IsMimeMultipartContent())
            {
                return MapRequestFile<T>();
            }

            var data = ActionContext.Request.Content.ReadAsStringAsync().Result;

            return !string.IsNullOrEmpty(data) ? JsonConvert.DeserializeObject<T>(data, JsonSettings) : new T();
        }

        protected Image Base64ToImage(string base64String)
        {
            byte[] imageBytes = Convert.FromBase64String(base64String);

            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                Image image = Image.FromStream(ms, true);
                return image;
            }
        }
        private T MapRequestFile<T>() where T : class, new()
        {
            var uploadedFiles = new List<FileModel>();
            var entidad = new T();
            var root = GetRequestRoot();
            var provider = new MultipartFormDataStreamProvider(root);

            Task.Factory.StartNew(() => provider = ActionContext.Request.Content.ReadAsMultipartAsync(
                new MultipartFormDataStreamProvider(root)).Result,
                new CancellationToken(),
                TaskCreationOptions.LongRunning,
                TaskScheduler.Default).Wait();

            var values = provider.FormData.GetValues(JsonSerializedForm);
            var formValue = values != null && values.Length > 0 ? values[0] : null;

            if (!string.IsNullOrEmpty(formValue))
            {
                entidad = JsonConvert.DeserializeObject<T>(formValue);
            }


            foreach (var file in provider.FileData)
            {
                try
                {
                    var fileInfo = new FileModel
                    {
                        FileName = file.Headers.ContentDisposition.FileName.Replace("\"", ""),
                        OriginalFileName = file.Headers.ContentDisposition.Name.Replace("\"", ""),
                        ContentType = file.Headers.ContentType.MediaType,
                        ContentLength = new FileInfo(file.LocalFileName).Length
                    };

                    if (fileInfo.ContentLength > 0)
                    {
                        fileInfo.ContentLength = fileInfo.ContentLength / BytesOfKilobyte;
                    }

                    fileInfo.FileContent = File.ReadAllBytes(file.LocalFileName);

                    uploadedFiles.Add(fileInfo);

                }
                finally
                {
                    File.Delete(file.LocalFileName);

                    foreach (var oldFile in
                            from oldFile in Directory.GetFiles(root)
                            let info = new FileInfo(oldFile)
                            where
                                info.CreationTime.CompareTo(
                                    DateTime.Now.AddMinutes(-15)) != 1
                            select oldFile)
                    {
                        File.Delete(oldFile);
                    }

                }
            }

            AsignarAdjuntos(entidad, uploadedFiles);

            return entidad;
        }

        private static void AsignarAdjuntos<T>(T model, List<FileModel> attachments) where T : class, new()
        {
            if (!typeof(T).IsSubclassOf(typeof(FileAttachModel)))
            {
                return;
            }

            var modeloBase = model as FileAttachModel;
            if (modeloBase != null)
            {
                modeloBase.FileAttachments = attachments;
            }
        }

        private static string GetRequestRoot()
        {
            try
            {
                return HttpContext.Current.Server.MapPath(UploadDirectory);
            }
            catch
            {
                return $"{AppDomain.CurrentDomain.BaseDirectory}{UploadDirectory.Replace("~", "").Replace("/", "\\")}".Replace("\\\\", "\\");
            }
        }

    }
}