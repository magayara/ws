﻿using MiPastaServicios.Data.Dao;
using MiPastaServicios.Models.Recipes;
using MiPastaServicios.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace MiPastaServicios.Controllers.Api
{
    [RoutePrefix("api/recipe")]
    public class RecipeController : ApiBase
    {
        
        [HttpGet, Route("get/for-guest/{pageNumber:int}/{itemsPerPage:int}")]
        [ResponseType(typeof(List<Recipe>))]
        public HttpResponseMessage GetForGuest(int pageNumber, int itemsPerPage)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Recipe.GetForGuest(pageNumber, itemsPerPage));
        }
        [HttpGet, Route("get/filter/{pageNumber:int}/{itemsPerPage:int}")]
        [ResponseType(typeof(List<Recipe>))]
        public HttpResponseMessage GetByFilters(int pageNumber, int itemsPerPage, [FromUri] string barcode, 
            [FromUri] bool latest, [FromUri] bool popular, [FromUri] string title)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Recipe.GetByFilters(pageNumber, itemsPerPage, barcode, latest, popular, title));
        }

        [HttpGet, Route("get/by-user/{userid:long}/{pageNumber:int}/{itemsPerPage:int}")]
        [ResponseType(typeof(List<Recipe>))]
        public HttpResponseMessage GetRecipesByUserId(long userid, int pageNumber, int itemsPerPage)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Recipe.GetRecipesByUserId(userid, pageNumber, itemsPerPage));
        }

        [HttpGet, Route("get/favourites/{userId:long}/{pageNumber:int}/{itemsPerPage:int}")]
        [ResponseType(typeof(List<Recipe>))]
        public HttpResponseMessage GetRecipesFavouritesByUserId(long userId, int pageNumber, int itemsPerPage)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Recipe.GetRecipesFavouritesByUserId(userId, pageNumber, itemsPerPage));
        }

        [HttpGet, Route("get/{userId:long}/{recipeId:long}/{loadImages:bool}")]
        [ResponseType(typeof(Recipe))]
        public HttpResponseMessage Get(long userId, long recipeId, bool loadImages)
        {
            var recipe = Recipe.Get(userId, recipeId);

            if (recipe == null) {
                return Request.CreateResponse(HttpStatusCode.OK, recipe);
            }

            if (loadImages) {
                if (!string.IsNullOrEmpty(recipe.Image1Url)) {
                    recipe.Image1Base64 = "data:image/jpg;base64," + Convert.ToBase64String(Recipe.GetRecipeImage(recipeId, Recipe.Image1FileName));
                }
                if (!string.IsNullOrEmpty(recipe.Image2Url))
                {
                    recipe.Image2Base64 = "data:image/jpg;base64," + Convert.ToBase64String(Recipe.GetRecipeImage(recipeId, Recipe.Image2FileName));
                }
                if (!string.IsNullOrEmpty(recipe.Image3Url))
                {
                    recipe.Image3Base64 = "data:image/jpg;base64," + Convert.ToBase64String(Recipe.GetRecipeImage(recipeId, Recipe.Image3FileName));
                }
            }

            return Request.CreateResponse(HttpStatusCode.OK, recipe);
        }

        [HttpPost, Route("save")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage Save()
        {
            var model = DeserializaPeticion<Recipe>();

            if (model.Image1Base64 != null)
            {
                model.Image1 = new Models.FileModel
                {
                    FileContent = ImageUtil.ImageFromBase64(model.Image1Base64)
                };
                model.DbImage1 = Recipe.Image1FileName;
            }

            if (model.Image2Base64 != null)
            {
                model.Image2 = new Models.FileModel
                {
                    FileContent = ImageUtil.ImageFromBase64(model.Image2Base64)
                };
                model.DbImage2 = Recipe.Image2FileName;
            }

            if (model.Image3Base64 != null)
            {
                model.Image3 = new Models.FileModel
                {
                    FileContent = ImageUtil.ImageFromBase64(model.Image3Base64)
                };
                model.DbImage3 = Recipe.Image3FileName;
            }

            return Request.CreateResponse(HttpStatusCode.OK, Recipe.Save(model));
        }

        [HttpPost, Route("delete")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage Delete()
        {
            var model = DeserializaPeticion<Recipe>();
            return Request.CreateResponse(HttpStatusCode.OK, Recipe.Delete(model.RecipeId));
        }

        [HttpPost, Route("set-as-favourite/{userId:long}/{recipeId:long}/{isFavourite:bool}")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage SetAsFavourite(long userId, long recipeId, bool isFavourite)
        {
            Recipe.SetAsFavourite(userId, recipeId, isFavourite);
            Recipe.SetLike(userId, recipeId, isFavourite);

            return Request.CreateResponse(HttpStatusCode.OK, true);
        }

        [HttpPost, Route("set-rating/{userid:long}/{recipeId:long}")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage SetRating(long userId, long recipeId, [FromUri] decimal rating)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Recipe.SetRating(userId, recipeId, rating));
        }

        [HttpPost, Route("set-like/{userid:long}/{recipeId:long}/{isLike:bool}")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage SetLike(long userId, long recipeId, bool isLike)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Recipe.SetLike(userId, recipeId, isLike));
        }

        [HttpGet, Route("recipe-post-image/{recipeId:long}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage GetBuildRecipePostImage(long recipeId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Recipe.BuildRecipePostImage(recipeId));
        }


        [HttpGet, Route("SendPush/")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage SendPush()
        {
            var model = new Recipe {
                RecipeId = 1188,
                Title = "FIDEOS  CON SALSA DE COLIFLOR  Y PEREJIL",
                User = new Models.Profile.User {
                    Id = 20,
                    FirstName = "La",
                    LastName = "Moderna ®"
                }
            };
            var tokens = new UserProfileDao().GetFollowers(model.User.Id);

            if (tokens != null && tokens.Any())
            {
                var pushSent = FcmUtil.SendPushNotification(tokens,
                    ConfigurationManager.AppSettings["PushNotification_Title"],
                    string.Format("{0} ha {1} la receta \"{2}\"",
                                model.User.NombreCompleto,
                                (false ? "agregado" : "actualizado"),
                                model.Title), null);
            }

            //var recipe = new RecipeDao().GetRecipeBasicInfo(model.RecipeId);
            //model.User.FirstName = "Miguel";
            //model.User.LastName = "Ceteno";
            //recipe.User.PushRegistrationId = "c6aDBD_GMcg:APA91bF0Kkh34WJgBGzh6GnhG_XAF1YlfmuMf7GqZKMu6wsWSL7Jpv6FNjloFEPW21fnTreE_9qExAXqx3GsaMO2ltL7GBH9EBE8vtQVMBUSiMG4VBL1laqVvbx4Qud3AT-fdK_6OyWN";

            //var pushSent1 = FcmUtil.SendPushNotification(new string[] { recipe.User.PushRegistrationId },
            //    ConfigurationManager.AppSettings["PushNotification_Title"],
            //    string.Format("{0} ha realizado un comentario de tu receta \"{1}\"",
            //                model.User.NombreCompleto, recipe.Title), null);

            return Request.CreateResponse(HttpStatusCode.OK, true);
        }
    }
}
