﻿using MiPastaServicios.Exceptions;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace MiPastaServicios
{
    public class GlobalExceptionFilter : ExceptionFilterAttribute
    {
        /// <summary>
        ///     Arroja un evento tipo Excepción.
        /// </summary>
        /// <param name="actionExecutedContext">Contexto de la excepción.</param>
        public override void OnException(
            HttpActionExecutedContext actionExecutedContext)
        {
            var errorMessage = ConstuyeMensajeError(actionExecutedContext.Exception).Replace(Environment.NewLine, " ");
            var response = new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                Content = new StringContent(errorMessage),
                ReasonPhrase = errorMessage
            };
            response.Headers.Add("X-Error", Locale.es.ServerError500);
            actionExecutedContext.Response = response;
        }

        /// <summary>
        ///     Construye el mensaje de error.
        /// </summary>
        /// <param name="exception">Excepción generada.</param>
        /// <returns>Mensaje de error.</returns>
        public static string ConstuyeMensajeError(Exception exception)
        {
            if (exception is UserNotFoundException)
            {
                return Locale.es.UserNotFoundException;
            }
            if (exception is WrongUserPasswordException)
            {
                return Locale.es.WrongUserPasswordException;
            }

            return string.Format(Locale.es.ServerError, exception.Message);

        }
    }
}