﻿using Newtonsoft.Json.Converters;
using System.Globalization;
using System.Web.Http;

namespace MiPastaServicios
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Filters.Add(new GlobalExceptionFilter());
            IsoDateTimeConverter converter = new IsoDateTimeConverter
            {
                DateTimeStyles = DateTimeStyles.AdjustToUniversal,
                DateTimeFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ssK"
            };

            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(converter);
        }
    }
}
