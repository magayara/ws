﻿namespace MiPastaServicios.Data
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    /// <summary>
    ///     Clase conexión a base de datos. Se encarga de gestionar 
    ///     las conexiones y consumo de la base de datos.
    /// </summary>

    public class DbConnection
    {
        /// <summary>
        /// Instancia de conector SQL.
        /// </summary>
        public SqlConnection SqlConnection { get; }

        public DbConnection()
        {
            SqlConnection = new SqlConnection(
                ConfigurationManager.ConnectionStrings["Database"].ConnectionString);
        }

        /// <summary>
        /// Cierra conexión con base de datos.
        /// </summary>
        internal void Close()
        {
            if (SqlConnection != null && SqlConnection.State != ConnectionState.Closed)
            {
                SqlConnection.Close();
            }
        }

        /// <summary>
        /// Lanza conexión a base de datos y valida estatus de conexión.
        /// </summary>
        internal void DbConnect()
        {
            try
            {
                SqlConnection.Open();
            }
            catch (SqlException ex)
            {
                throw new Exception("Conexión rechazada");
            }

        }
    }
}