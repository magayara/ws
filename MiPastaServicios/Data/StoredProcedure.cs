﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace MiPastaServicios.Data
{
    /// <summary>
    ///     Clase para la ejecución de procedimientos almacenados.
    /// </summary>
    public class StoredProcedure : IDisposable
    {

        /// <summary>
        ///     Libera los recursos.
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }


        public SqlConnection GetSqlConnection() {
            var con = new DbConnection().SqlConnection;
            con.Open();

            return con;

        }

        public SqlTransaction StartTransaction(SqlConnection con)
        {
            return con.BeginTransaction();
        }

        public bool ExecuteNonQuery(SqlConnection con, SqlTransaction transaction, string storeProcedure, List<SqlParameter> parameters = null) {
            using (var command = BuildCommand(storeProcedure, parameters))
            {
                command.Connection = con;
                command.Transaction = transaction;
                return command.ExecuteNonQuery() > 0;
            }
        }

        /// <summary>
        ///     Ejecuta procedimiento almacenado sin devolver ningun resultado.
        /// </summary>
        public bool ExecuteNonQuery(string storeProcedure, List<SqlParameter> parameters = null)
        {
            int result;

            using (var con = new DbConnection().SqlConnection)
            {
                con.Open();

                try
                {
                    using (var command = BuildCommand(storeProcedure, parameters))
                    {
                        command.Connection = con;
                        result = command.ExecuteNonQuery();
                    }
                }
                finally
                {
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                }
            }

            return result> 0;
        }

        public long ExecuteParameterReturn(string storeProcedure, string parameterReturn, List<SqlParameter> parameters = null)
        {
            long result;

            using (var con = new DbConnection().SqlConnection)
            {
                con.Open();

                try
                {
                    using (var command = BuildCommand(storeProcedure, parameters))
                    {
                        command.Connection = con;
                        command.ExecuteNonQuery();
                    
                        result = long.Parse(command.Parameters[parameterReturn].Value.ToString());
                    }
                }
                finally
                {
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                }
            }

            return result;
        }

        /// <summary>
        ///     Ejecuta procedimiento almacenado devolviendo múltiples resultados.
        /// </summary>
        /// <typeparam name="T">Tipo de modelo resultado.</typeparam>
        /// <returns>
        ///     Colección de ejemplares resultado de ejecución.
        /// </returns>
        /// <param name="storeProcedure"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public List<T> GetMultiple<T>(string storeProcedure, List<SqlParameter> parameters = null) where T : class, new()
        {
            List<T> result;

            using (var con = new DbConnection().SqlConnection)
            {
                try
                {
                    con.Open();

                    using (var command = BuildCommand(storeProcedure, parameters))
                    {
                        command.Connection = con;
                        using (var dr = command.ExecuteReader())
                        {
                            result = DataReaderMapToList<T>(dr);
                        }
                    }
                }
                finally
                {
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                }
            }

            return result;
        }

        /// <summary>
        ///     Ejecuta procedimiento almacenado devolviendo un atributo.
        /// </summary>
        /// <returns>
        ///     Atributo resultado de ejecución.
        /// </returns>
        public T GetSingle<T>(string storeProcedure, List<SqlParameter> parameters = null)
        {
            using (var con = new DbConnection().SqlConnection)
            {
                try
                {
                    con.Open();

                    using (var command = BuildCommand(storeProcedure, parameters))
                    {
                        command.Connection = con;
            
                        using (var dr = command.ExecuteReader())
                        {
                            if (!dr.HasRows) {
                                return default(T);
                            }

                             return DataReaderMapToObject<T>(dr);
                        }
                    }
                }
                finally
                {
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Permite mapear una colección de tipo de clase definida, como resultado la ejecución de un
        /// procedimiento almacenado.
        /// </summary>
        /// <typeparam name="T">Tipo de clase definida</typeparam>
        /// <param name="dr">Lector de datos resultado de ejecución de procedimiento almacenado.</param>
        /// <returns>Colección de tipo de clase definida.</returns>
        private static List<T> DataReaderMapToList<T>(IDataReader dr)
        {
            var list = new List<T>();

            try
            {
                while (dr.Read())
                {
                    var obj = Activator.CreateInstance<T>();
                    MapToObject(obj, dr);
                    list.Add(obj);
                }

                dr.Close();
            }
            catch (Exception ex)
            {
                if (dr.IsClosed)
                {
                    throw ex;
                }

                dr.Close();

                throw ex;
            }

            return list;
        }

        /// <summary>
        /// Permite mapear un ejemplar de tipo de clase definida, como resultado la ejecución de un
        /// procedimiento almacenado.
        /// </summary>
        /// <typeparam name="T">Tipo de clase definida</typeparam>
        /// <param name="dr">Lector de datos resultado de ejecución de procedimiento almacenado.</param>
        /// <returns>Ejemplar de tipo de clase definida.</returns>
        private static T DataReaderMapToObject<T>(IDataReader dr)
        {
            T obj = Activator.CreateInstance<T>();

            try
            {
                while (dr.Read())
                {
                    MapToObject(obj, dr);
                }

                dr.Close();
            }
            catch (Exception ex)
            {
                if (dr.IsClosed)
                {
                    throw ex;
                }

                dr.Close();

                throw ex;
            }

            return obj;
        }

        /// <summary>
        /// Permite realizar el mapeo de un <see cref="IDataReader"/> a un objeto tipo <see cref="T"/>.
        /// </summary>
        /// <typeparam name="T">Tipo de ejemplar a mapear.</typeparam>
        /// <param name="obj">Objeto <see cref="T"/> destino.</param>
        /// <param name="dr">Lector de datos.</param>
        private static void MapToObject<T>(T obj, IDataReader dr)
        {
            foreach (var prop in obj.GetType().GetProperties().Where(prop =>
                Enumerable.Range(0, dr.FieldCount).Any(i => dr.GetName(i) == prop.Name)
                    && !Equals(dr[prop.Name], DBNull.Value)))
            {
                prop.SetValue(obj, dr[prop.Name], null);
            }
        }

        /// <summary>
        /// Permite construir el comando a ejecutar en la base de datos.
        /// </summary>
        /// <param name="stringCommand">Procedimiento almacenado a ejecutar.</param>
        /// <param name="parameters">Parámetros de Procedimiento almacenado.</param>
        /// <returns>Ejemplar <see cref="SqlCommand"/>.</returns>
        private static SqlCommand BuildCommand(string stringCommand, List<SqlParameter> parameters)
        {
            var command = new SqlCommand(stringCommand)
            {
                CommandType = CommandType.StoredProcedure
            };

            if (parameters == null || !parameters.Any())
            {
                return command;
            }

            foreach (var parameter in parameters)
            {
                command.Parameters.Add(parameter);
            }

            return command;
        }

    }
}