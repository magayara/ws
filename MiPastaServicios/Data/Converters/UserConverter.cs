﻿using MiPastaServicios.Data.Entities;
using MiPastaServicios.Models.Profile;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace MiPastaServicios.Data.Converters
{
    public static class UserConverter
    {

        public static User ToModel(UserTe from)
        {

            if (from == null)
            {
                return null;
            }

            return new User
            {
                Id = from.Id,
                FirstName = from.FirstName,
                LastName = from.LastName,
                Email = from.Email,
                Ocupation = from.Ocupation,
                Estate = new Estate
                {
                    Id = from.EstateId
                },
                BirthDate = from.BirthDate,
                Gender = (Gender)from.Gender,
                OauthToken = from.OauthToken,
                AuthenticationType = (AuthenticationType)from.AuthenticationType,
                Password = from.Password,
                _avatarUrl = (AuthenticationType)from.AuthenticationType == AuthenticationType.Custom
                    ? string.Format("{0}{1}", ConfigurationManager.AppSettings["UrlAvataresUsuario"], from.AvatarUrl)
                    : from.AvatarUrl,
                FacebookId = from.FacebookId,
                PushRegistrationId = from.PushRegistrationId,
                Enabled = from.Enabled
            };
        }


        public static List<User> ToModel(List<UserTe> from)
        {
            return from.Select(ToModel).ToList();
        }
    }
}