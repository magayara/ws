﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace MiPastaServicios.Data.Converters
{
    /// <summary>
    ///     Convertidor de Fecha personalizado.
    /// </summary>
    public class CustomDateTimeConverter : DateTimeConverterBase
    {
        /// <summary>
        /// Permite realizar la lectura de la representación JSON del objeto proporcionado.
        /// </summary>
        /// <param name="reader">Ejemplar <see cref="JsonReader"/> de donde se realizará la lectura.</param>
        /// <param name="objectType">Tipo de objeto proporcionado.</param>
        /// <param name="existingValue">Valor del objeto proporcionado.</param>
        /// <param name="serializer">Ejemplar <see cref="JsonSerializer"/> del serializador JSON.</param>
        /// <returns>El valor del objeto.</returns>
        public override object ReadJson(JsonReader reader, Type objectType,
            object existingValue, JsonSerializer serializer)
        {
            return DateTime.Parse(reader.Value.ToString());
        }

        /// <summary>
        /// Permite escribi la representación  JSON del objeto proporcionado.
        /// </summary>
        /// <param name="writer">Ejemplar <see cref="JsonWriter"/> de donde se realizará la escritura.</param>
        /// <param name="value">Valor del objeto proporcionado.</param>
        /// <param name="serializer">Ejemplar <see cref="JsonSerializer"/> del serializador JSON.</param>
        public override void WriteJson(JsonWriter writer, object value,
            JsonSerializer serializer)
        {
            writer.WriteValue(((DateTime)value).ToString("dd/MM/yyyy HH:mm:ss"));
        }
    }
}