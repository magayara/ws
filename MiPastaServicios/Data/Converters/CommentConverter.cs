﻿using MiPastaServicios.Data.Entities;
using MiPastaServicios.Models.Profile;
using MiPastaServicios.Models.Recipes;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace MiPastaServicios.Data.Converters
{
    public static class CommentConverter
    {

        public static Comment ToModel(CommentTe from)
        {
            return new Comment
            {
                CommentId = from.CommentId,
                RecipeId = from.RecipeId,
                Message = from.Message,
                IdUsers = from.IdUsers,
                Date = from.Date,
                User = new User
                {
                    Id = from.UserId,
                    FirstName = from.FirstName,
                    LastName = from.LastName,
                    AvatarUrl = (AuthenticationType)from.AuthenticationType == AuthenticationType.Custom
                    ? string.Format("{0}{1}", ConfigurationManager.AppSettings["UrlAvataresUsuario"], from.AvatarUrl)
                    : from.AvatarUrl
                },
                Ilike = from.Ilike,
                CommenParentId = from.CommenParentId,
                isContactComment = from.isContactComment,
                Counter = from.Counter
            };
        }

        public static List<Comment> ToModel(List<CommentTe> from)
        {
            return from.Select(ToModel).ToList();
        }
    }
}