﻿using MiPastaServicios.Data.Entities;
using MiPastaServicios.Models.Profile;
using MiPastaServicios.Models.Recipes;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace MiPastaServicios.Data.Converters
{
    public static class RecipeConverter
    {
        public static Recipe ToModel(RecipeTe from)
        {
            if (from == null)
            {
                return null;
            }

            return new Recipe
            {
                RecipeId = from.RecipeId,
                Title = from.Title,
                Rating = from.Rating,
                MyRating = (float) from.MyRating,
                IsFavourite = from.IsFavourite,
                IsLike = from.IsLike,
                User = from.UserId > 0 ? new User
                {
                    Id = from.UserId,
                    FirstName = from.FirstName,
                    LastName = from.LastName,
                    AvatarUrl =  (AuthenticationType)from.AuthenticationType == AuthenticationType.Custom
                    ? string.Format("{0}{1}", ConfigurationManager.AppSettings["UrlAvataresUsuario"], from.AvatarUrl)
                    : from.AvatarUrl,
                    PushRegistrationId = from.PushRegistrationId
                } : null,
                Difficulty = from.Difficulty > 0 ? (Difficulty)from.Difficulty : Difficulty.Easy,
                CookTime = from.CookTime,
                Likes = from.Likes,
                VideoUrl = from.VideoUrl,
                DbImage1 = from.DbImage1,
                DbImage2 = from.DbImage2,
                DbImage3 = from.DbImage3,
                Date = from.Date,
                BarCode = from.Barcode,
                IsFollowed = from.IsFollowed
            };
        }

        public static List<Recipe> ToModel(List<RecipeTe> from)
        {
            return from.Select(ToModel).ToList();
        }
    }
}