﻿using MiPastaServicios.Data.Converters;
using MiPastaServicios.Data.Entities;
using MiPastaServicios.Models.Recipes;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace MiPastaServicios.Data.Dao
{
    /// <summary>
    /// Componente que suministra la interfaz necesaria para realizar transacciones 
    /// de datos de las recetas.
    /// </summary>
    public class RecipeDao
    {

        internal List<Recipe> GetRecipesByUserId(long userId, int pageNumber, int itemsPerPage)
        {
            using (var sp = new StoredProcedure())
            {
                return RecipeConverter.ToModel(sp.GetMultiple<RecipeTe>("GetRecipesByUserId_sp", new List<SqlParameter> {
                    new SqlParameter("UserId", userId),
                    new SqlParameter("PageNumber", pageNumber),
                    new SqlParameter("ItemsPerPage", itemsPerPage)
                }));
            }
        }

        internal List<Recipe> GetRecipesFavouritesByUserId(long userId, int pageNumber, int itemsPerPage)
        {
            using (var sp = new StoredProcedure())
            {
                return RecipeConverter.ToModel(sp.GetMultiple<RecipeTe>("GetRecipesFavouritesByUserId_sp", new List<SqlParameter> {
                    new SqlParameter("UserId", userId),
                    new SqlParameter("PageNumber", pageNumber),
                    new SqlParameter("ItemsPerPage", itemsPerPage)
                }));
            }
        }

        internal List<Recipe> GetForGuest(int pageNumber, int itemsPerPage)
        {
            using (var sp = new StoredProcedure())
            {
                return RecipeConverter.ToModel(sp.GetMultiple<RecipeTe>("GetRecipesForGuest_sp", new List<SqlParameter> {

                    new SqlParameter("PageNumber", pageNumber),
                    new SqlParameter("ItemsPerPage", itemsPerPage)
                }));
            }
        }

        internal List<Recipe> GetByFilters(int pageNumber, int itemsPerPage, string barcode, bool latest, bool popular, string title)
        {
            using (var sp = new StoredProcedure())
            {
                return RecipeConverter.ToModel(sp.GetMultiple<RecipeTe>("GetRecipesByFilters_sp", new List<SqlParameter> {

                    new SqlParameter("PageNumber", pageNumber),
                    new SqlParameter("ItemsPerPage", itemsPerPage),
                    new SqlParameter("Barcode", barcode),
                    new SqlParameter("Latest", latest),
                    new SqlParameter("Popular", popular),
                    new SqlParameter("Title", title)
                }));
            }
        }

        internal Recipe GetRecipe(long userId, long recipeId)
        {
            using (var sp = new StoredProcedure())
            {
                var model = RecipeConverter.ToModel(sp.GetSingle<RecipeTe>("GetRecipe_sp", new List<SqlParameter> {
                    new SqlParameter("UserId", userId),
                    new SqlParameter("RecipeId", recipeId)
                }));

                if (model == null)
                {
                    return null;
                }

                model.Ingredients = sp.GetMultiple<Ingredient>("GetRecipeIngredientsByRecipeId_sp", new List<SqlParameter> {
                       new SqlParameter("RecipeId", recipeId)
                });

                model.CookSteps = sp.GetMultiple<CookStep>("GetRecipeCookStepsByRecipeId_sp", new List<SqlParameter> {
                       new SqlParameter("RecipeId", recipeId)
                });

                return model;
            }
        }

        internal Recipe GetRecipeBasicInfo(long recipeId)
        {
            using (var sp = new StoredProcedure())
            {
                return RecipeConverter.ToModel(sp.GetSingle<RecipeTe>("GetRecipeBasicInfo_sp", new List<SqlParameter> {
                    new SqlParameter("RecipeId", recipeId)
                }));
            }
        }

        public bool SetRecipeAsFavourite(long userId, long recipeId, bool isFavourite)
        {
            using (var sp = new StoredProcedure())
            {
                return sp.ExecuteNonQuery("SetRecipeAsFavourite_sp", new List<SqlParameter> {
                    new SqlParameter("UserId", userId),
                    new SqlParameter("RecipeId", recipeId),
                    new SqlParameter("IsFavourite", isFavourite)

                });
            }
        }

        public bool SaveRecipe(Recipe model)
        {
            using (var sp = new StoredProcedure())
            {
                var con = sp.GetSqlConnection();
                var transaction = sp.StartTransaction(con);

                try
                {
                    SqlParameter pvNewId = new SqlParameter
                    {
                        ParameterName = "@NewRecipeId",
                        DbType = DbType.Int64,
                        Direction = ParameterDirection.Output
                    };

                    sp.ExecuteNonQuery(con, transaction, "SaveRecipe_sp", new List<SqlParameter> {
                        new SqlParameter("RecipeId", model.RecipeId),
                        new SqlParameter("Title", model.Title),
                        new SqlParameter("UserId", model.User.Id),
                        new SqlParameter("Difficulty", (int) model.Difficulty),
                        new SqlParameter("CookTime", model.CookTime),
                        new SqlParameter("DbImage1", model.DbImage1),
                        new SqlParameter("DbImage2", model.DbImage2),
                        new SqlParameter("DbImage3", model.DbImage3),
                        new SqlParameter("Barcode", model.BarCode),
                        pvNewId
                    });

                    model.RecipeId = long.Parse(pvNewId.Value.ToString());

                    foreach (var item in model.Ingredients)
                    {
                        sp.ExecuteNonQuery(con, transaction, "SaveRecipeIngredient_sp", new List<SqlParameter> {
                            new SqlParameter("RecipeId", model.RecipeId),
                            new SqlParameter("Order", item.Order),
                            new SqlParameter("Quantity", string.IsNullOrEmpty(item.Quantity) ?  null : item.Quantity),
                            new SqlParameter("Unit", item.Unit),
                            new SqlParameter("Description", item.Description)
                        });
                    }

                    foreach (var item in model.CookSteps)
                    {
                        sp.ExecuteNonQuery(con, transaction, "SaveRecipeCookStep_sp", new List<SqlParameter> {
                            new SqlParameter("RecipeId", model.RecipeId),
                            new SqlParameter("Order", item.Order),
                            new SqlParameter("Description", item.Description)
                        });
                    }

                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    if (con.State == ConnectionState.Open) con.Close();
                }

                return true;
            }
        }

        public bool DeleteRecipe(long recipeId)
        {
            using (var sp = new StoredProcedure())
            {
                return sp.ExecuteNonQuery("DeleteRecipe_sp", new List<SqlParameter> {
                    new SqlParameter("RecipeId", recipeId)
                });
            }
        }

        public bool SetRecipeRating(long userId, long recipeId, decimal rating)
        {
            using (var sp = new StoredProcedure())
            {
                return sp.ExecuteNonQuery("SetRecipeRating_sp", new List<SqlParameter> {
                    new SqlParameter("UserId", userId),
                    new SqlParameter("RecipeId", recipeId),
                    new SqlParameter("Rating", rating)
                });
            }
        }

        public bool SetRecipeLike(long userId, long recipeId, bool isLike)
        {
            using (var sp = new StoredProcedure())
            {
                return sp.ExecuteNonQuery("SetRecipeLike_sp", new List<SqlParameter> {
                    new SqlParameter("UserId", userId),
                    new SqlParameter("RecipeId", recipeId),
                    new SqlParameter("IsLike", isLike)
                });
            }
        }



    }
}