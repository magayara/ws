﻿using MiPastaServicios.Data.Converters;
using MiPastaServicios.Data.Entities;
using MiPastaServicios.Models.Profile;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace MiPastaServicios.Data.Dao
{
    /// <summary>
    /// Componente que suministra la interfaz necesaria para realizar transacciones 
    /// de datos del perfil de usuario.
    /// </summary>
    public class UserProfileDao
    {
        internal bool Follow(long followerId, long userId)
        {
            using (var sp = new StoredProcedure())
            {
                return sp.ExecuteNonQuery("FollowUser_sp", new List<SqlParameter> {
                    new SqlParameter("FollowerId", followerId),
                    new SqlParameter("FollowId", userId)
                });
            }
        }

        internal bool StopFollow(long followerId, long userId)
        {
            using (var sp = new StoredProcedure())
            {
                return sp.ExecuteNonQuery("StopFollowUser_sp", new List<SqlParameter> {
                    new SqlParameter("FollowerId", followerId),
                    new SqlParameter("FollowId", userId)
                });
            }
        }

        public string[] GetFollowers(long userId)
        {
            using (var sp = new StoredProcedure())
            {
                return UserConverter.ToModel(sp.GetMultiple<UserTe>("GetUserFollowers_sp",
                    new List<SqlParameter> { new SqlParameter("UserId", userId) }))?.Select(p => p.PushRegistrationId).Where(p=> !string.IsNullOrEmpty(p)).ToArray();
            }
        }

        public string[] GetAllUsers()
        {
            using (var sp = new StoredProcedure())
            {
                return UserConverter.ToModel(sp.GetMultiple<UserTe>("GetAllUsers_sp"))?.Select(p => p.PushRegistrationId).Where(p => !string.IsNullOrEmpty(p)).ToArray();
            }
        }
        public string[] GetSpecificUsers(string ids)
        {
            using (var sp = new StoredProcedure())
            {
                var model = UserConverter.ToModel(sp.GetMultiple<UserTe>("GetTokens", new List<SqlParameter> {
                        new SqlParameter("UsersId", ids)
                    }));
                return model.Select(p => p.PushRegistrationId).Where(p => !string.IsNullOrEmpty(p)).ToArray();
            }
        }

        internal UserStatistic GetUserStatistic(long userId)
        {
            using (var sp = new StoredProcedure())
            {
                var model =  sp.GetSingle<UserStatistic>("GetUserStatistic_sp", new List<SqlParameter> {
                    new SqlParameter("UserId", userId)
                });

                if (model == null){
                    model = new UserStatistic
                    {
                        UserId = userId
                    };
                }

                model.Seguidores = UserConverter.ToModel(sp.GetMultiple<UserTe>("GetUserFollowers_sp", new List<SqlParameter> {
                    new SqlParameter("UserId", userId)
                }));

                model.Seguidos = UserConverter.ToModel(sp.GetMultiple<UserTe>("GetUserFollowings_sp", new List<SqlParameter> {
                    new SqlParameter("UserId", userId)
                })); ;

                return model;
            }
        }

        internal bool SaveUser(User model)
        {
            using (var sp = new StoredProcedure())
            {
                return sp.ExecuteNonQuery("SaveUser_sp", new List<SqlParameter> {
                    new SqlParameter("FirstName", model.FirstName),
                    new SqlParameter("LastName", model.LastName),
                    new SqlParameter("Email", model.Email.Trim()),
                    new SqlParameter("Ocupation", model.Ocupation),
                    new SqlParameter("EstateId", model.Estate != null ? model.Estate.Id : (int?) null),
                    new SqlParameter("BirthDate", model.BirthDate),
                    new SqlParameter("Gender", model.Gender != null ? (int) model.Gender : (int?) null),
                    new SqlParameter("OauthToken", model.OauthToken),
                    new SqlParameter("AuthenticationType", (int)  model.AuthenticationType),
                    new SqlParameter("Password", model.Password),
                    new SqlParameter("AvatarUrl", model.AvatarUrl),
                    new SqlParameter("FacebookId", model.FacebookId),
                    new SqlParameter("PushRegistrationId", model.PushRegistrationId),
                });
            }
        }

        internal User GetUser(string email)
        {
            using (var sp = new StoredProcedure())
            {
                return UserConverter.ToModel(sp.GetSingle<UserTe>("GetUserByEmail_sp", new List<SqlParameter> {
                    new SqlParameter("Email", email)
                }));
            }
        }

        internal User GetUser(long userId)
        {
            using (var sp = new StoredProcedure())
            {
                return UserConverter.ToModel(sp.GetSingle<UserTe>("GetUserById_sp", new List<SqlParameter> {
                    new SqlParameter("UserId", userId)
                }));
            }
        }

    }
}