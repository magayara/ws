﻿using MiPastaServicios.Data.Converters;
using MiPastaServicios.Data.Entities;
using MiPastaServicios.Models.Profile;
using MiPastaServicios.Models.Recipes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace MiPastaServicios.Data.Dao
{
    public class CommentDao
    {

        internal string[] GetregistrationIdsCommenters(long recipeId)
        {
            using (var sp = new StoredProcedure())
            {
                return sp.GetMultiple<User>("GetregistrationIdsCommenters_sp", new List<SqlParameter> {
                    new SqlParameter("RecipeId", recipeId),
                }).Select(p=>p.PushRegistrationId).ToArray();
            }
        }

        internal List<Comment> GetByRecipe(long recipeId, long userId, int pageNumber, int itemsPerPage) {
            using (var sp = new StoredProcedure())
            {
                return CommentConverter.ToModel(sp.GetMultiple<CommentTe>("GetComments_sp", new List<SqlParameter> {
                    new SqlParameter("RecipeId", recipeId),
                    new SqlParameter("UserId", userId),
                    new SqlParameter("PageNumber", pageNumber),
                    new SqlParameter("ItemsPerPage", itemsPerPage)
                }));
            }
        }

        internal List<Comment> GetByUser(long userId, int pageNumber, int itemsPerPage)
        {
            var list = new List<Comment>();
            using (var sp = new StoredProcedure())
            {
                list.AddRange(CommentConverter.ToModel(sp.GetMultiple<CommentTe>("GetComments_sp", new List<SqlParameter> {
                    new SqlParameter("UserId", userId),
                    new SqlParameter("PageNumber", pageNumber),
                    new SqlParameter("ItemsPerPage", itemsPerPage)
                })));
            }

            using (var sp = new StoredProcedure())
            {
                list.AddRange(CommentConverter.ToModel(sp.GetMultiple<CommentTe>("GetContactComments_sp", new List<SqlParameter> {
                    new SqlParameter("UserId", userId),
                    new SqlParameter("PageNumber", pageNumber),
                    new SqlParameter("ItemsPerPage", itemsPerPage)
                })));
            }

            return list;
        }

        public bool SetCommentLike(long userId, long commentId, bool isLike)
        {
            using (var sp = new StoredProcedure())
            {
                return sp.ExecuteNonQuery("SetCommentLike_sp", new List<SqlParameter> {
                    new SqlParameter("UserId", userId),
                    new SqlParameter("CommentId", commentId),
                    new SqlParameter("IsLike", isLike)
                });
            }
        }

        internal Comment GetNewCommentCount(long userId)
        {
            using (var sp = new StoredProcedure())
            {
                var res= CommentConverter.ToModel(sp.GetMultiple<CommentTe>("GetNewCommentCount_sp", new List<SqlParameter> {

                    new SqlParameter("UserId", userId)
                }));



                //var result= sp.GetSingle<int>("GetNewCommentCount_sp", new List<SqlParameter> {
                //    new SqlParameter("UserId", userId)
                //});
                return res.FirstOrDefault();
                
            }
        }
        

        public bool SetAsViewed(long commentId)
        {
            using (var sp = new StoredProcedure())
            {
                return sp.ExecuteNonQuery("SetAsViewed_sp", new List<SqlParameter> {
                    new SqlParameter("CommentId", commentId)
                });
            }
        }

        public bool Delete(long commentId)
        {
            using (var sp = new StoredProcedure())
            {
                return sp.ExecuteNonQuery("DeleteComment_sp", new List<SqlParameter> {
                    new SqlParameter("CommentId", commentId)
                });
            }
        }
        

        public bool Save(Comment model)
        {
            using (var sp = new StoredProcedure())
            {
                return sp.ExecuteNonQuery("SaveComment_sp", new List<SqlParameter> {
                    new SqlParameter("RecipeId", model.RecipeId),
                    new SqlParameter("UserId", model.User.Id),
                    new SqlParameter("Message", model.Message),
                    new SqlParameter("Date", DateTime.Now),
                    new SqlParameter("CommenParentId", model.CommenParentId),
                    
                });
            }
        }

        public bool SaveContactComment(Comment model)
        {
            using (var sp = new StoredProcedure())
            {
                return sp.ExecuteNonQuery("SaveContactComment_sp", new List<SqlParameter> {
                    new SqlParameter("UserId", model.User.Id),
                    new SqlParameter("Message", model.Message),
                    new SqlParameter("Date", DateTime.Now),
                    new SqlParameter("CommenParentId", model.CommenParentId),

                });
            }
        }
        
    }
}