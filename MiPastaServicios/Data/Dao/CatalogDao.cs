﻿using MiPastaServicios.Models.Profile;
using MiPastaServicios.Models.Recipes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace MiPastaServicios.Data.Dao
{
    /// <summary>
    /// Componente que suministra la interfaz necesaria para consultar catálogos de la app.
    /// </summary>
    public class CatalogDao
    {
        /// <summary>
        /// Permite obtener estados de la república.
        /// </summary>
        /// <returns>Colección de tipo <see ref="Estate"></see></returns>
        public List<Estate> GetEstates()
        {
            using (var sp = new StoredProcedure()) {
                return sp.GetMultiple<Estate>("GetEstates_sp");
            }
        }

        public List<IngredientUnit> GetIngredientUnits(string query)
        {
            using (var sp = new StoredProcedure())
            {
                return sp.GetMultiple<IngredientUnit>("GetIngredientUnits_sp", new List<SqlParameter> {
                    new SqlParameter("Query", string.IsNullOrEmpty(query) ? null :query.Trim())
                });
            }
        }

        public List<IngredientPasta> GetIngredientPastas()
        {
            using (var sp = new StoredProcedure())
            {
                return sp.GetMultiple<IngredientPasta>("GetIngredientPastas_sp");
            }
        }
        
        public List<Ingredient> GetIngredients(string query)
        {
            using (var sp = new StoredProcedure())
            {
                return sp.GetMultiple<Ingredient>("GetIngredients_sp", new List<SqlParameter> {
                    new SqlParameter("Query", string.IsNullOrEmpty(query) ? null :query.Trim())
                }).OrderBy(p => p.Description).ToList();
            }
        }

        public Product GetProduct(string barCode)
        {
            using (var sp = new StoredProcedure())
            {
                return sp.GetSingle<Product>("GetProduct_sp", new List<SqlParameter> {
                    new SqlParameter("BarCode", barCode.Trim())
                });
            }
        }
    }
}