﻿using System;

namespace MiPastaServicios.Data.Entities
{
    public class UserTe
    {
        /// <summary>
        /// Id de usuario.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Nombres del usuario.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Apellidos del usuario.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Email del usuario.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Ocupación del usuario.
        /// </summary>
        public string Ocupation { get; set; }

        /// <summary>
        /// Identificador de estado de la república en el que habita el usuario.
        /// </summary>
        public int EstateId { get; set; }

        /// <summary>
        /// Fecha de nacimiento del usuario.
        /// </summary>
        public DateTime BirthDate { get; set; }

        /// <summary>
        /// Género del usuario.
        /// </summary>
        public int Gender { get; set; }

        /// <summary>
        /// Token de autenticación externa con redes sociales.
        /// </summary>
        public string OauthToken { get; set; }

        /// <summary>
        /// Tipo de autenticación usada por el usuario.
        /// </summary>
        public int AuthenticationType { get; set; }

        /// <summary>
        /// Constraseña encriptada del usuario.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// URL del avatar del usuario.
        /// </summary>
        public string AvatarUrl { get; set; }

        public string FacebookId { get; set; }

        public string PushRegistrationId { get; set; }

        public bool Enabled { get; set; }

    }
}