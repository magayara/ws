﻿using System;

namespace MiPastaServicios.Data.Entities
{
    public class CommentTe
    {
        public long CommentId { get; set; }

        public long RecipeId { get; set; }

        public string Message { get; set; }
        public string IdUsers { get; set; }

        public DateTime Date { get; set; }

        /// <summary>
        /// Id de usuario.
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// Nombres del usuario.
        /// </summary>
        public string FirstName { get; set; }
        public int Counter { get; set; }
        /// <summary>
        /// Apellidos del usuario.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Tipo de autenticación usada por el usuario.
        /// </summary>
        public int AuthenticationType { get; set; }

        /// <summary>
        /// URL del avatar del usuario.
        /// </summary>
        public string AvatarUrl { get; set; }

        public bool Ilike { get; set; }

        public long? CommenParentId { get; set; }

        public bool isContactComment { get; set; }

    }
}