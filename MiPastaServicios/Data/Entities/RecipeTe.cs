﻿using System;

namespace MiPastaServicios.Data.Entities
{
    public class RecipeTe
    {
        public long RecipeId { get; set; }

        public string Title { get; set; }

        public decimal Rating { get; set; }

        /// <summary>
        /// Id de usuario.
        /// </summary>
        public long UserId { get; set; }

        public int Difficulty { get; set; }

        public decimal CookTime { get; set; }

        public long Likes { get; set; }

        public string VideoUrl { get; set; }

        public string DbImage1 { get; set; }

        public string DbImage2 { get; set; }

        public string DbImage3 { get; set; }

        public DateTime Date { get; set; }

        public string Barcode { get; set; }

        /// <summary>
        /// Nombres del usuario.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Apellidos del usuario.
        /// </summary>
        public string LastName { get; set; }

        public int AuthenticationType { get; set; }

        /// <summary>
        /// URL del avatar del usuario.
        /// </summary>
        public string AvatarUrl { get; set; }

        public decimal MyRating { get; set; }

        public bool IsFavourite { get; set; }

        public bool IsLike { get; set; }

        public bool IsFollowed { get; set; }

        public string PushRegistrationId { get; set; }

    }
}