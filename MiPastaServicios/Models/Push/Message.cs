﻿namespace MiPastaServicios.Model.Push
{
    public class Message
    {
        public string sound { get { return "default"; } }
        public string[] registration_ids { get; set; }
        public string[] to { get; set; }
        public Notification data { get; set; }
        public Notification notification { get; set; }
        //public object data { get; set; }
    }

}