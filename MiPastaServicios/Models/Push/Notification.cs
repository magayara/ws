﻿using Newtonsoft.Json;

namespace MiPastaServicios.Model.Push
{

    public class Notification
    {
        public string title { get; set; }
        public string body { get; set; }
        public int notId { get; set; }

        [JsonProperty(PropertyName = "content-available")]
        public bool content { get { return true;  } }
        
    }
}