﻿namespace MiPastaServicios.Models
{
    /// <summary>
    ///     Parser de validación de token de autenticación con redes sociales.
    /// </summary>
    public class ParsedExternalAccessToken
    {
        /// <summary>
        /// Identificador de usuario.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Identificador de aplicación.
        /// </summary>
        public string AppId { get; set; }
    }

}