﻿namespace MiPastaServicios.Models
{
    /// <summary>
    ///     Modela la información de un archivo cargado a la aplicación.
    /// </summary>
    public class FileModel
    {
        /// <summary>
        ///     Nombre de archivo.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        ///     Nombre original de archivo.
        /// </summary>
        public string OriginalFileName { get; set; }

        /// <summary>
        ///     Tipo Mime.
        /// </summary>
        public string ContentType { get; set; }

        /// <summary>
        ///     Peso del archivo.
        /// </summary>
        public long ContentLength { get; set; }

        /// <summary>
        ///     Contenido del archivo.
        /// </summary>
        public byte[] FileContent { get; set; }
    }
}