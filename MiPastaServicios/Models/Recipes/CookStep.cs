﻿using System;

namespace MiPastaServicios.Models.Recipes
{
    public class CookStep
    {

        public long CookStepId { get; set; }

        public int Order { get; set; }

        public string Description { get; set; }
    }
}