﻿using MiPastaServicios.Data.Dao;
using MiPastaServicios.Models.Profile;
using MiPastaServicios.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web.Hosting;

namespace MiPastaServicios.Models.Recipes
{
    public class Recipe : FileAttachModel
    {
        public const string Image1FileName = "Image1.jpeg";

        public const string Image2FileName = "Image2.jpeg";

        public const string Image3FileName = "Image3.jpeg";

        public long RecipeId { get; set; }

        public string Title { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal Rating { get; set; }

        public User User { get; set; }

        public Difficulty Difficulty { get; set; }

        public decimal CookTime { get; set; }

        public long Likes { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string VideoUrl { get; set; }

        [JsonIgnore]
        public string DbImage1 { get; set; }

        [JsonIgnore]
        public string DbImage2 { get; set; }

        [JsonIgnore]
        public string DbImage3 { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Image1Url
        {
            get
            {
                if (string.IsNullOrEmpty(DbImage1) || !IsExistImage(DbImage1))
                {
                    return null;
                }

                return string.Format("{0}{1}/{2}?time={3}",
                        ConfigurationManager.AppSettings["UrlImagenReceta"], RecipeId, DbImage1, DateTime.Now.Ticks);
            }
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Image2Url {
            get
            {
                if (string.IsNullOrEmpty(DbImage2) || !IsExistImage(DbImage2))
                {
                    return null;
                }

                return string.Format("{0}{1}/{2}?time={3}",
                        ConfigurationManager.AppSettings["UrlImagenReceta"], RecipeId, DbImage2, DateTime.Now.Ticks);
            }
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Image3Url {
            get
            {
                if (string.IsNullOrEmpty(DbImage3) || !IsExistImage(DbImage3))
                {
                    return null;
                }

                return string.Format("{0}{1}/{2}?time={3}",
                        ConfigurationManager.AppSettings["UrlImagenReceta"], RecipeId, DbImage3, DateTime.Now.Ticks);
            }
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Ingredient> Ingredients { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<CookStep> CookSteps { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Comment> Comments { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public float MyRating { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool IsFavourite { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool IsLike { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool IsFollowed { get; set; }
        
        [JsonIgnore]
        public FileModel Image1 { get; set; }

        [JsonIgnore]
        public FileModel Image2 { get; set; }

        [JsonIgnore]
        public FileModel Image3 { get; set; }

        public string Image1Base64 { get; set; }

        public string Image2Base64 { get; set; }

        public string Image3Base64 { get; set; }

        public string BarCode { get; set; }

        public DateTime Date { get; set; }

        internal static List<Recipe> GetForGuest(int pageNumber, int itemsPerPage)
        {
            return new RecipeDao().GetForGuest(pageNumber, itemsPerPage);
        }

        internal static List<Recipe> GetByFilters(int pageNumber, int itemsPerPage, string barcode, bool latest, bool popular, string title)
        {
            return new RecipeDao().GetByFilters(pageNumber, itemsPerPage, barcode, latest, popular, title);
        }

        internal static List<Recipe> GetRecipesFavouritesByUserId(long userId, int pageNumber, int itemsPerPage)
        {
            return new RecipeDao().GetRecipesFavouritesByUserId(userId, pageNumber, itemsPerPage);
        }

        internal static List<Recipe> GetRecipesByUserId(long userId, int pageNumber, int itemsPerPage)
        {
            return new RecipeDao().GetRecipesByUserId(userId, pageNumber, itemsPerPage);
        }

        internal static Recipe Get(long userId, long recipeId)
        {
            return new RecipeDao().GetRecipe(userId, recipeId);
        }

        internal static bool Save(Recipe model)
        {
            var newRecipe = model.RecipeId == 0;
            new RecipeDao().SaveRecipe(model);

            IoUtil.CreateDirectoryIfNotExists(string.Format("{0}{1}", System.Web.HttpContext.Current.Server.MapPath(
                    ConfigurationManager.AppSettings["RutaRepositorioImagenReceta"]), model.RecipeId.ToString()));

            var imageRepository = System.Web.HttpContext.Current.Server.MapPath(
                    ConfigurationManager.AppSettings["RutaRepositorioImagenReceta"]);

			var pageRepository = System.Web.HttpContext.Current.Server.MapPath(
					ConfigurationManager.AppSettings["RutaRepositorioPaginas"]);

			var image1Name = string.Format("{0}/{1}", model.RecipeId, Image1FileName);
            var image2Name = string.Format("{0}/{1}", model.RecipeId, Image2FileName);
            var image3Name = string.Format("{0}/{1}", model.RecipeId, Image3FileName);

            IoUtil.DeleteFile(imageRepository, image1Name);
            IoUtil.DeleteFile(imageRepository, image2Name);
            IoUtil.DeleteFile(imageRepository, image3Name);

			if (model.Image1 != null && model.Image1.FileContent.Length > 0)
			{
				IoUtil.CreateReplaceFile(imageRepository, image1Name, ImageUtil.ScaleImage(model.Image1.FileContent, 581, 436));
				model.DbImage1 = Image1FileName;

				// se agrega el html para el link fb - app
				var page = Locale.htmlPage.PageText.Replace("{0}", model.Title).Replace("{1}", model.RecipeId.ToString()).Replace("{2}", Image1FileName);
	
				IoUtil.CreateReplaceFile(pageRepository, string.Format("{0}.html", model.RecipeId), page);

			}
			else
			{

				// se agrega el html para el link fb - app en caso de no haber imagen se agrega una imagen default
				var page = Locale.htmlPage.PageText.Replace("{0}", model.Title).Replace("{1}", "0").Replace("{2}", "default.jpeg");

				IoUtil.CreateReplaceFile(pageRepository, string.Format("{0}.html", model.RecipeId), page);
			}

            if (model.Image2 != null && model.Image2.FileContent.Length > 0)
            {
                IoUtil.CreateReplaceFile(imageRepository, image2Name, ImageUtil.ScaleImage(model.Image2.FileContent, 581, 436));
                model.DbImage2 = Image2FileName;
            }

            if (model.Image3 != null && model.Image3.FileContent.Length > 0 )
            {
                IoUtil.CreateReplaceFile(imageRepository, image3Name, ImageUtil.ScaleImage(model.Image3.FileContent, 581, 436));
                model.DbImage3 = Image3FileName;
            }

            var tokens = new UserProfileDao().GetFollowers(model.User.Id);

            if (tokens != null && tokens.Any()) {
                var pushSent = FcmUtil.SendPushNotification(tokens.Distinct().ToArray(), 
                    ConfigurationManager.AppSettings["PushNotification_Title"], 
                    string.Format("{0} ha {1} la receta \"{2}\"", 
                                model.User.NombreCompleto, 
                                (newRecipe ? "agregado" : "actualizado"), 
                                model.Title), null);
            }

            return true;
        }

        internal static bool Delete(long recipeId)
        {
            return new RecipeDao().DeleteRecipe(recipeId);
        }

        public static bool SetAsFavourite(long userId, long recipeId, bool isFavourite)
        {
            return new RecipeDao().SetRecipeAsFavourite(userId, recipeId, isFavourite);
        }

        public static bool SetRating(long userId, long recipeId, decimal rating)
        {
            return new RecipeDao().SetRecipeRating(userId, recipeId, rating);
        }

        public static bool SetLike(long userId, long recipeId, bool isLike)
        {
            return new RecipeDao().SetRecipeLike(userId, recipeId, isLike);
        }

        public static byte[] GetRecipeImage(long recipeId, string imagePath)
        {
            return IoUtil.GetFile(string.Format("{0}{1}", System.Web.HttpContext.Current.Server.MapPath(
                    ConfigurationManager.AppSettings["RutaRepositorioImagenReceta"]), recipeId.ToString()), imagePath);
        }

        internal static string BuildRecipePostImage(long recipeId)
        {
            var model =  new RecipeDao().GetRecipeBasicInfo(recipeId);

            if (model == null || model.RecipeId != recipeId) {
                return string.Empty;
            }

            Bitmap bitmap = (Bitmap)Image.FromFile(string.Format("{0}Public/Recipe/{1}/{2}", HostingEnvironment.MapPath("~"), model.RecipeId, Image1FileName));
            SolidBrush semiTransBrush = new SolidBrush(Color.FromArgb(160, 245, 184, 8));

            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                graphics.FillRectangle(semiTransBrush, new Rectangle(0, 0, bitmap.Width, 100));

                graphics.DrawImage(Image.FromFile(string.Format("{0}mipasta.png", HostingEnvironment.MapPath("~"))), 10,10, 30, 30);

                using (Font arialFont = new Font("Arial", 7, FontStyle.Bold))
                {
                    graphics.DrawString("Mi pasta mi mundo", arialFont, Brushes.Black, new PointF(45, 13f));
                }

                using (Font arialFont = new Font("Arial", 8))
                {
                    graphics.DrawString("Compartimos nuestra receta:", arialFont, Brushes.Black, new PointF(45f, 28f));
                }

                using (Font arialFont = new Font("Arial", 9, FontStyle.Bold))
                {
                    graphics.DrawString(model.Title, arialFont, Brushes.Black, new RectangleF(10, 50f, bitmap.Width-10, 70));
                }
            }

            using (var ms = new MemoryStream()) {
      
                bitmap.Save(ms, ImageFormat.Jpeg);
                return Convert.ToBase64String(ms.ToArray());
            }
        }

        internal static bool PostOnFacebookAsync(long userId, long recipeId)
        {
            var user = new UserProfileDao().GetUser(userId);

            if (user == null)
            {
                return false;
            }

            new Facebook(user.OauthToken, user.FacebookId).PublishSimplePost("hola mundo");

            return true;
        }

        private bool IsExistImage(string dbImage2)
        {
            return
                IoUtil.IsExistFile(string.Format("{0}{1}", System.Web.HttpContext.Current.Server.MapPath(
                    ConfigurationManager.AppSettings["RutaRepositorioImagenReceta"]), RecipeId.ToString()), dbImage2);
        }
    }
}