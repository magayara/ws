﻿using MiPastaServicios.Data.Dao;
using System.Collections.Generic;

namespace MiPastaServicios.Models.Recipes
{
    public class IngredientUnit
    {
        public string Unit { get; set; }

        public static List<IngredientUnit> GetIngredientUnits(string query)
        {
            return new CatalogDao().GetIngredientUnits(query);
        }        
    }
}