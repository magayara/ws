﻿using MiPastaServicios.Data.Dao;
using System.Collections.Generic;

namespace MiPastaServicios.Models.Recipes
{
    public class IngredientPasta
    {
        public long IngredientId { get; set; }

        public long RecipeId { get; set; }

        public int Order { get; set; }

        public string Clave { get; set; }

        public static List<IngredientPasta> GetIngredientPastas()
        {
            return new CatalogDao().GetIngredientPastas();
        }
    }
}