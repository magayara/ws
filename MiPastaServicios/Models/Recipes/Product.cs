﻿using MiPastaServicios.Data.Dao;
using System.Configuration;

namespace MiPastaServicios.Models.Recipes
{
    public class Product
    {
        public string BarCode { get; set; }

        public string Nombre { get; set; }

        public string DbImage { get; set; }

        public string ImageUrl
        {
            get
            {
                if (string.IsNullOrEmpty(DbImage))
                {
                    return null;
                }

                return string.Format("{0}/{1}",
                        ConfigurationManager.AppSettings["UrlImagenProducto"],  DbImage);
            }
        }

        public static Product GetProduct(string barCode)
        {
            return new CatalogDao().GetProduct(barCode);
        }
    }
}