﻿using MiPastaServicios.Data.Dao;
using System;
using System.Collections.Generic;

namespace MiPastaServicios.Models.Recipes
{
    public class Ingredient
    {
        public long IngredientId { get; set; }

        public int Order { get; set; }

        public string Quantity { get; set; }

        public string Unit { get; set; }

        public string Description { get; set; }

        public static List<Ingredient> GetIngredients(string query)
        {
            return new CatalogDao().GetIngredients(query);
        }
    }
}