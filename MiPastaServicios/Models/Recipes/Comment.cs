﻿using MiPastaServicios.Data.Dao;
using MiPastaServicios.Models.Profile;
using MiPastaServicios.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace MiPastaServicios.Models.Recipes
{
    public class Comment
    {
        public long CommentId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public long RecipeId { get; set; }

        public string Message { get; set; }

        public DateTime Date { get; set; }

        public User User { get; set; }

        public bool Ilike { get; set; }

        public long? CommenParentId { get; set; }

        public int Counter { get; set; }

        public bool FromCheff { get; set; }

        public bool isContactComment { get; set; }
        public string IdUsers { get; set; }

        internal static List<Comment> GetByRecipe(long recipeId, long userId, int pageNumber, int itemsPerPage)
        {
            return new CommentDao().GetByRecipe(recipeId, userId, pageNumber, itemsPerPage).OrderByDescending(p=>p.Date).ToList();
        }

        internal static List<Comment> GetByUser(long userId, int pageNumber, int itemsPerPage)
        {
            return new CommentDao().GetByUser(userId, pageNumber, itemsPerPage).OrderByDescending(p => p.Date).ToList();
        }

        internal static bool SetLike(long userId, long commentId, bool isLike) {
            return new CommentDao().SetCommentLike(userId, commentId, isLike);
        }

        internal static bool SetAsViewed( long commentId)
        {
            return new CommentDao().SetAsViewed(commentId);
        }

        internal static bool Delete(long commentId)
        {
            return new CommentDao().Delete(commentId);
        }

        internal static Comment GetNewCommentCount(long userId)
        {
            return new CommentDao().GetNewCommentCount(userId);
        }
        
        internal static bool Save(Comment model)
        {
            var result =  new CommentDao().Save(model);

            if (!result) {
                return result;
            }

            var recipe = new RecipeDao().GetRecipeBasicInfo(model.RecipeId);

            if (model.FromCheff) {
                
               var regs = new CommentDao().GetregistrationIdsCommenters(model.RecipeId);

                if (regs == null || !regs.Any())
                {
                    return result;
                }

                var pushSent1 = FcmUtil.SendPushNotification(regs,
                    ConfigurationManager.AppSettings["PushNotification_Title"],
                    string.Format("{0} ha realizado un comentario de la receta \"{1}\"",
                                model.User.NombreCompleto, recipe.Title), null);

                return result;
            }



            if (string.IsNullOrEmpty(recipe.User.PushRegistrationId)) {
                return result;
            }

            var pushSent = FcmUtil.SendPushNotification(new string[] { recipe.User.PushRegistrationId },
                ConfigurationManager.AppSettings["PushNotification_Title"],
                string.Format("{0} ha realizado un comentario de tu receta \"{1}\"",
                            model.User.NombreCompleto, recipe.Title), null);

            return result;

        }

        internal static bool SaveContactComment(Comment model)
        {
            return new CommentDao().SaveContactComment(model);
        }

        internal static string SendMessage(string message)
        {
            try
            {

            var tokens = new UserProfileDao().GetAllUsers();
            var pushSent = FcmUtil.SendPushNotification(tokens.Distinct().ToArray(),
                ConfigurationManager.AppSettings["PushNotification_Title"],
                message, null);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "correcto";
        }
        internal static string SendMessageUsers(string message, string usersIds)
        {
            try
            {

            var tokens = new UserProfileDao().GetSpecificUsers(usersIds);
            var pushSent = FcmUtil.SendPushNotification(tokens.Distinct().ToArray(),
                ConfigurationManager.AppSettings["PushNotification_Title"],
                message, null);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "correcto";
        }

    }
}