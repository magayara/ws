﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace MiPastaServicios.Models
{
    public class FileAttachModel
    {
        [JsonIgnore]
        public List<FileModel> FileAttachments { get; set; }
    }
}