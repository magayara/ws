﻿using MiPastaServicios.Data.Dao;
using System;
using System.Collections.Generic;

namespace MiPastaServicios.Models.Profile
{
    /// <summary>
    /// Clase que modela los atributos del estadisticas de usuario.
    /// </summary>
    public class UserStatistic
    {
        /// <summary>
        /// Id de usuario.
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// Total de usuarios seguidores.
        /// </summary>
        public long Followers { get; set; }

        /// <summary>
        /// Totales de usuarios siguiendo.
        /// </summary>
        public long Following { get; set; }

        /// <summary>
        /// Totales de puntos.
        /// </summary>
        public long Points { get; set; }

        public List<User> Seguidores { get; set; }

        public List<User> Seguidos { get; set; }

        internal static UserStatistic Get(long userId)
        {
            return new UserProfileDao().GetUserStatistic(userId);
        }
    }
}