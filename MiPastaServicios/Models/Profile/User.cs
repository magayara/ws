﻿using MiPastaServicios.Data.Dao;
using MiPastaServicios.Exceptions;
using MiPastaServicios.Util;
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace MiPastaServicios.Models.Profile
{
    /// <summary>
    /// Clase que modela los atributos del perfil de usuario, así como la lógica de negocio para 
    /// realizar transacciones referentes al perfil de usuario.
    /// </summary>
    public class User : FileAttachModel
    {
        public const string AvatarFilename = "Avatar";

        public string _avatarUrl;

        /// <summary>
        /// Id de usuario.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Nombres del usuario.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Apellidos del usuario.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Email del usuario.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Email { get; set; }

        /// <summary>
        /// Ocupación del usuario.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Ocupation { get; set; }

        /// <summary>
        /// Estado de la república en el que habita el usuario.
        /// </summary>

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Estate Estate { get; set; }

        /// <summary>
        /// Fecha de nacimiento del usuario.
        /// </summary>
        public DateTime? BirthDate { get; set; }

        /// <summary>
        /// Género del usuario.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Gender? Gender { get; set; }

        /// <summary>
        /// Token de autenticación externa con redes sociales.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string OauthToken { get; set; }

        /// <summary>
        /// Tipo de autenticación usada por el usuario.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public AuthenticationType? AuthenticationType { get; set; }

        /// <summary>
        /// Constraseña encriptada del usuario.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Password { get; set; }

        /// <summary>
        /// Arreglo de bytes del avatar cargado del usuario.
        /// </summary>
        [JsonIgnore]
        public FileModel Avatar { get; set; }

        public string AvatarBase64 { get; set; }

        public string FacebookId { get; set; }

        public string NombreCompleto
        {
            get
            {
                return string.Format("{0} {1}", FirstName, LastName).Trim();
            }
        }

        public string PushRegistrationId { get; set; }

        public bool Enabled { get; set; }

        /// <summary>
        /// URL del avatar del usuario.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AvatarUrl
        {
            get
            {
                return _avatarUrl;
            }
            set
            {
                _avatarUrl = AuthenticationType == Profile.AuthenticationType.Custom ? string.Format("{0}?time={1}", value, DateTime.Now.Ticks) : value;
            }
        }

        internal static User Get(string email)
        {
            var user = new UserProfileDao().GetUser(email);

            if (user == null)
            {
                throw new UserNotFoundException();
            }

            return user;
        }

        internal static User GetByCredential(string email, string password)
        {
            var user = new UserProfileDao().GetUser(email);

            if (user == null)
            {
                throw new UserNotFoundException();
            }

            if (!user.Password.Equals(HashPassword(password.Trim()), StringComparison.InvariantCultureIgnoreCase))
            {
                throw new WrongUserPasswordException();
            }

            return user;
        }

        internal static User Get(long userId)
        {
            return new UserProfileDao().GetUser(userId);
        }

        internal static bool Save(User model)
        {
            if (model.AuthenticationType.Value == Profile.AuthenticationType.Custom
                && !string.IsNullOrEmpty(model.Password))
            {
                model.Password = HashPassword(model.Password.Trim());
            }

            if (model.AuthenticationType.Value == Profile.AuthenticationType.Custom && model.Avatar != null)
            {
                IoUtil.CreateReplaceFile(System.Web.HttpContext.Current.Server.MapPath(
                    ConfigurationManager.AppSettings["RutaRepositorioAvatarUsuario"]),
                        string.Format("{0}.png", model.Id), ImageUtil.RedimensionaAvatarUsuario(model.Avatar.FileContent));

                model.AvatarUrl = string.Format("{0}.png", model.Id);
            }

            return new UserProfileDao().SaveUser(model);
        }

        internal static bool Follow(long followerId, long userId)
        {
            return new UserProfileDao().Follow(followerId, userId);
        }

        internal static bool StopFollow(long followerId, long userId)
        {
            return new UserProfileDao().StopFollow(followerId, userId);
        }

        private static string HashPassword(string password)
        {
            return Convert.ToBase64String(
                new SHA256CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(password)));
        }
    }
}