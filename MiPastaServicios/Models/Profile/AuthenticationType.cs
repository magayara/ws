﻿namespace MiPastaServicios.Models.Profile
{
    /// <summary>
    /// Enumeración que define los tipos de autenticación de usuario,
    /// </summary>
    public enum AuthenticationType
    {
        /// <summary>
        /// Autenticación con Google.
        /// </summary>
        Google = 1,

        /// <summary>
        /// Autenticación con Facebook.
        /// </summary>
        Facebook = 2,

        /// <summary>
        /// Autenticación personalizada en base a e-mail y contraseña.
        /// </summary>
        Custom = 3
    }
}