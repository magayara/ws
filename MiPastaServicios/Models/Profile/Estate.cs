﻿using MiPastaServicios.Data.Dao;
using System.Collections.Generic;

namespace MiPastaServicios.Models.Profile
{
    /// <summary>
    /// Clase que modela los atributos del catálogo de estados de la república.
    /// </summary>
    public class Estate
    {
        /// <summary>
        /// Identificador del estado.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del estado.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Permite obtener lista de estados de la república.
        /// </summary>
        /// <returns>Colección de tipo <see ref="Estate"></see></returns>
        internal static List<Estate> GetEstates()
        {
            return new CatalogDao().GetEstates();
        }

    }
}