﻿namespace MiPastaServicios.Models.Profile
{
    /// <summary>
    /// Enumeración que define los géneros de una persona.
    /// </summary>
    public enum Gender
    {
        /// <summary>
        /// Género masculino.
        /// </summary>
        Male = 1,

        /// <summary>
        /// Género femenino.
        /// </summary>
        Female = 2
    }
}