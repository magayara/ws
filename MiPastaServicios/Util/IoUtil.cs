﻿using System;

namespace MiPastaServicios.Util
{
    using System.IO;

    /// <summary>
    ///     Contiene utilerias de manipulación  de IO
    /// </summary>
    public class IoUtil
    {

        /// <summary>
        /// Elimina archivo.
        /// </summary>
        /// <param name="directoryPath">Ruta de directorio.</param>
        /// <param name="fileName">Nombe del archivo.</param>
        public static void DeleteFile(string directoryPath, string fileName)
        {
            var directoryInfo = new DirectoryInfo(directoryPath);
            var archivo = Path.Combine(directoryInfo.FullName, fileName);

            if (File.Exists(archivo))
            {
                File.Delete(archivo);
            }
        }


        public static void DeleteFile(string filePath)
        {
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }

        /// <summary>
        /// Crea o remplaza archivo,
        /// </summary>
        /// <param name="directoryPath">Ruta de directorio.</param>
        /// <param name="fileName">Nombe del archivo.</param>
        /// <param name="content">Contenido del archivo.</param>
        public static void CreateReplaceFile(string directoryPath, string fileName, byte[] content)
        {
            var directoryInfo = new DirectoryInfo(directoryPath);
            var archivo = Path.Combine(directoryInfo.FullName, fileName);

            if (!directoryInfo.Exists)
            {
                directoryInfo.Create();
            }
            else
            {
                DeleteFile(archivo);
            }

            File.WriteAllBytes(archivo, content);
        }

        internal static byte[] GetFile(string directorio, string fileName)
        {
            var fileInfo = new FileInfo(Path.Combine(directorio, fileName));
            return !fileInfo.Exists ? null : File.ReadAllBytes(fileInfo.FullName);
        }

        internal static bool IsExistFile(string directorio, string fileName) {
            if (string.IsNullOrEmpty(fileName)) {
                return false;
            }
            return new FileInfo(Path.Combine(directorio, fileName)).Exists;
        }
   

        public static void CreateDirectoryIfNotExists(string mapPath)
        {
            if (!Directory.Exists(mapPath)) {
                Directory.CreateDirectory(mapPath);
            }
        }

		public static void CreateReplaceFile(string directoryPath, string fileName, string content)
		{
			var directoryInfo = new DirectoryInfo(directoryPath);
			var archivo = Path.Combine(directoryInfo.FullName, fileName);

			if (!directoryInfo.Exists)
			{
				directoryInfo.Create();
			}
			else
			{
				DeleteFile(archivo);
			}

			using (StreamWriter sw = File.CreateText(archivo))
			{
				sw.WriteLine(content);
			}
		}

	}
}