﻿using nQuant;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;


namespace MiPastaServicios.Util
{
    /// <summary>
    ///     Utilería para manipulación de imágenes.
    /// </summary>
    public class ImageUtil
    {

        /// <summary>
        ///     Alto predeterminado de imagen avatar de usuario.
        /// </summary>
        public const int AltoPredeterminadoAvatarUsuario = 100;

        /// <summary>
        ///     Ancho predeterminado de imagen avatar de usuario.
        /// </summary>
        public const int AnchoPredeterminadoAvatarUsuario = 100;

        /// <summary>
        ///     Alto predeterminado de imagen logotipo.
        /// </summary>
        public const int AltoPredeterminadoLogoTipo = 150;

        /// <summary>
        ///     Ancho predeterminado de imagen logotipo.
        /// </summary>
        public const int AnchoPredeterminadoLogoTipo = 100;

        /// <summary>
        ///     Convierte un Raw de imagen en <see cref="Bitmap" />.
        /// </summary>
        /// <param name="rawImagen"> Raw de imagen a convertir. </param>
        /// <returns>
        ///     Representacion en <see cref="Bitmap" /> de raw de ManipulacionImagenes.
        /// </returns>
        public static Bitmap BytesToBitmap(byte[] rawImagen)
        {
            using (var ms = new MemoryStream(rawImagen))
            {
                var img = (Bitmap)Image.FromStream(ms);
                return img;
            }
        }

        /// <summary>
        ///     Redimensiona una imagen al tamaño predeterminado para un avatar de
        ///     usuario.
        /// </summary>
        /// <param name="rawImagen"> Raw de la imagen a redimensionar. </param>
        /// <returns>
        ///     Raw de imagen redimensionada al alto y ancho especificado.
        /// </returns>
        public static byte[] RedimensionaAvatarUsuario(byte[] rawImagen)
        {
            return CropImageFromCenter(rawImagen,
                AnchoPredeterminadoAvatarUsuario,
                AltoPredeterminadoAvatarUsuario);
        }

        public static byte[] ImageFromBase64(string imageBase64) {
            if (imageBase64.ToLower().Contains("data:image/png")) {
                imageBase64 = imageBase64.Replace("data:image/png;base64,", "");
            }
    
            else if (imageBase64.ToLower().Contains("data:image/jpeg")) {
                imageBase64 = imageBase64.Replace("data:image/jpeg;base64,", "");
            }
            else if (imageBase64.ToLower().Contains("data:image/jpg"))
            {
                imageBase64 = imageBase64.Replace("data:image/jpg;base64,", "");
            }
            else if (imageBase64.ToLower().Contains("data:image/gif"))
            {
                imageBase64 = imageBase64.Replace("data:image/gif;base64,", "");
            }

            try
            {
                return Convert.FromBase64String(imageBase64);
            }
            catch {
                return null;
            }
        }

        public static byte[] CropImageFromCenter(byte[] rawImagen, int maxWidth, int maxHeight)
        {
            var image = Image.FromStream(new MemoryStream(rawImagen));
            var left = 0;
            var top = 0;
            int srcWidth;
            int srcHeight;
            var bitmap = new Bitmap(maxWidth, maxHeight);
            var croppedHeightToWidth = (double)maxHeight / maxWidth;
            var croppedWidthToHeight = (double)maxWidth / maxHeight;

            if (image.Width > image.Height)
            {
                srcWidth = (int)(Math.Round(image.Height * croppedWidthToHeight));
                if (srcWidth < image.Width)
                {
                    srcHeight = image.Height;
                    left = (image.Width - srcWidth) / 2;
                }
                else
                {
                    srcHeight = (int)Math.Round(image.Height * ((double)image.Width / srcWidth));
                    srcWidth = image.Width;
                    top = (image.Height - srcHeight) / 2;
                }
            }
            else
            {
                srcHeight = (int)(Math.Round(image.Width * croppedHeightToWidth));
                if (srcHeight < image.Height)
                {
                    srcWidth = image.Width;
                    top = (image.Height - srcHeight) / 2;
                }
                else
                {
                    srcWidth = (int)Math.Round(image.Width * ((double)image.Height / srcHeight));
                    srcHeight = image.Height;
                    left = (image.Width - srcWidth) / 2;
                }
            }
            using (var g = Graphics.FromImage(bitmap))
            {
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(image, new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                new Rectangle(left, top, srcWidth, srcHeight), GraphicsUnit.Pixel);
            }

            var finalImage = bitmap;

            using (var st = new MemoryStream())
            {
                finalImage.Save(st, System.Drawing.Imaging.ImageFormat.Png);
                return st.ToArray();
            }
        }


        /// <summary>
        ///     Redimensiona imagen a ancho y alto especificado.
        /// </summary>
        /// <param name="rawImagen"> Raw de la imagen a redimensionar. </param>
        /// <param name="ancho">Ancho requerido en pixeles. </param>
        /// <param name="alto">Alto requerido en pixeles. </param>
        /// <returns>
        ///     Raw de imagen redimensionada al alto y ancho especificado.
        /// </returns>
        public static byte[] RedimensionaImagen(byte[] rawImagen, int ancho,
            int alto)
        {
            var imgPhoto = Image.FromStream(new MemoryStream(rawImagen));
            var sourceWidth = imgPhoto.Width;
            var sourceHeight = imgPhoto.Height;
            const int SourceX = 0;
            const int SourceY = 0;
            var destX = 0;
            var destY = 0;

            if (sourceWidth <= ancho  && sourceHeight <= alto)
            {
                using (var st = new MemoryStream())
                {
                    imgPhoto.Save(st, System.Drawing.Imaging.ImageFormat.Jpeg);
                    return st.ToArray();
                }
            }

            float nPercent;
            var nPercentW = (ancho / (float)sourceWidth);
            var nPercentH = (alto / (float)sourceHeight);
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = Convert.ToInt16((ancho - (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = Convert.ToInt16((alto - (sourceHeight * nPercent)) / 2);
            }

            var destWidth = (int)(sourceWidth * nPercent);
            var destHeight = (int)(sourceHeight * nPercent);
            var bmPhoto = new Bitmap(ancho, alto);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                imgPhoto.VerticalResolution);

            using (var grPhoto = Graphics.FromImage(bmPhoto))
            {
                grPhoto.Clear(Color.Transparent);
                grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                grPhoto.DrawImage(imgPhoto,
                    new Rectangle(destX, destY, destWidth, destHeight),
                    new Rectangle(SourceX, SourceY, sourceWidth, sourceHeight),
                    GraphicsUnit.Pixel);
            }

            using (var st = new MemoryStream())
            {
                using (var quantized = new WuQuantizer().QuantizeImage(bmPhoto))
                {
                    quantized.Save(st, ImageFormat.Png);
                }
                return st.ToArray();
            }
        }

        public static byte[] ScaleImage( byte[] rawImagen, int maxWidth, int maxHeight)
        {
            Image image = Image.FromStream(new MemoryStream(rawImagen));
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(newWidth, newHeight);

            using (var graphics = Graphics.FromImage(newImage))
            {
                graphics.Clear(Color.White);
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);
            }

            using (var st = new MemoryStream())
            {
                using (var quantized = new WuQuantizer().QuantizeImage(newImage))
                {
                    quantized.Save(st, ImageFormat.Jpeg);
                }

                return st.ToArray();
            }
        }
    }
}