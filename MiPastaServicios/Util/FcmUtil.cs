﻿using MiPastaServicios.Model.Push;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MiPastaServicios.Util
{
    public class FcmUtil
    {
        private static Uri FireBasePushNotificationsURL = new Uri("https://fcm.googleapis.com/fcm/send");

        public static bool SendPushNotification(string[] deviceTokens, string title, string body, object data)
        {
            //HttpResponseMessage result;
            bool sent = true;
            var request = new HttpRequestMessage(HttpMethod.Post, FireBasePushNotificationsURL);

            request.Headers.TryAddWithoutValidation("Authorization", "key=" + ConfigurationManager.AppSettings["ServerKey"]);
            request.Content = new StringContent(JsonConvert.SerializeObject(new Message
            {
                data = new Notification
                {
                    notId = new Random().Next(1, 99999),
                    title = title,
                    body = body
                },
                notification = new Notification
                {
                    notId = new Random().Next(1, 99999),
                    title = title,
                    body = body
                },
                registration_ids = deviceTokens.Distinct().ToArray()
            }), Encoding.UTF8, "application/json");

            using (var client = new HttpClient())
            {

                client.SendAsync(request).Wait();
               // sent = sent && result.IsSuccessStatusCode;
            }

            return sent;
        }
    }
}