﻿using MiPastaServicios.Exceptions;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace MiPastaServicios.Util
{
    /// <summary>
    ///     Manejador de excepciones global para la aplicacion Web.
    /// </summary>
    public class GlobalExceptionFilter : ExceptionFilterAttribute
    {

        /// <summary>
        ///     Arroja un evento tipo Excepción.
        /// </summary>
        /// <param name="actionExecutedContext">Contexto de la excepción.</param>
        public override void OnException(
            HttpActionExecutedContext actionExecutedContext)
        {
            var errorMessage = BuildErrorMessage(actionExecutedContext.Exception).Replace(Environment.NewLine, " ");
            var response = new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                Content = new StringContent(errorMessage),
                ReasonPhrase = errorMessage
            };

            response.Headers.Add("X-Error", "Ocurrió un error");
            actionExecutedContext.Response = response;
        }

        /// <summary>
        ///     Construye el mensaje de error.
        /// </summary>
        /// <param name="exception">Excepción generada.</param>
        /// <returns>Mensaje de error.</returns>
        private static string BuildErrorMessage(Exception exception)
        {
            if (exception is UserDisabledException) {
                return "El usuario ha sido desactivado";
            }

            if (exception is WrongUserPasswordException)
            {
                return "La contreseña no es válida";
            }

            if (exception is UserNotFoundException)
            {
                return "Su usuario no esta registrado";
            }

            return string.Format("Se ha generado la siguiente excepción: {0}", exception.Message);
        }
    }
}